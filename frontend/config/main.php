<?php
$params = array_merge(

    require __DIR__ . '/../../common/config/params.php',

    require __DIR__ . '/../../common/config/params-local.php',

    require __DIR__ . '/params.php',

    require __DIR__ . '/params-local.php'

);



return [

    'id' => 'app-frontend',

    'basePath' => dirname(__DIR__),

    'bootstrap' => ['log'],

    'controllerNamespace' => 'frontend\controllers',

    'components' => [

        'general' => [ 

            'class' => 'common\components\General', 

        ],

        'cart' => [ 

            'class' => 'common\components\MyCart', 

        ],

        'user' => [

            'identityClass' => 'common\models\User',

            'enableAutoLogin' => true,

            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],

        ],

        'session' => [

            'name' => 'PHPFRONTSESSID',

            'savePath' => sys_get_temp_dir(),

        ],

        'log' => [

            'traceLevel' => YII_DEBUG ? 3 : 0,

            'targets' => [

                [

                    'class' => 'yii\log\FileTarget',

                    'levels' => ['error', 'warning'],

                ],

            ],

        ],

        'errorHandler' => [

            'errorAction' => 'site/error',

        ],

        'request'=>[

			'class' => 'common\components\Request',

			'web'=> '/frontend/web',

			'cookieValidationKey' => 'Vfkp8kCUNg',           

			'csrfParam' => '_csrf-frontend',
			'baseUrl' => '/vitadaddy',

		],
		'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'product/<id:\d+>-<title:\w+>' => 'product/view',	
                '/<slug>-purchase-online-in-india.html' => 'product/category',
                '/<slug>-purchase-online-in-india-lowest-price.html' => 'product/view',
            ]
       ],
        'assetManager' => [  

            'bundles' => [

             'yii\bootstrap\BootstrapPluginAsset' => ['js'=>[]], 

             'yii\bootstrap\BootstrapAsset' => ['css' => []]   

            ],

        ],

        

    ],

    'params' => $params,

];

