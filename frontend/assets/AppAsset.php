<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        "css/easyauto.css",
        'css/site.css',
        'css/bootstrap.css',
        'css/fonts/fonts.css',
        'css/layout.css',
        'css/responsive.css',
        'css/bootstrap.min.css',
        'css/jquery.mmenu.css',
		'css/slick.css',
		'css/slick-theme.css',

    ];
	
    public $js = [
        "js/easyauto.js",
        "js/bootstrap.js",
        "js/popper.min.js",
        "js/ie10-viewport-bug-workaround.js",
        "js/jquery.mmenu.js",
        "js/custom.js",
		"js/slick.min.js",
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
    