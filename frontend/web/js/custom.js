$(function () {

    $('nav#menu').mmenu();

});

function toggleDiv(divId) {

    $("#" + divId).toggle();

}

var call = function(data, callback) {

    var callTry = function(data, callback) {

        data.params._csrf =  $('meta[name=csrf-token]').attr("content");

        var DATA = data.params;

        var ajxOpts = {

            url: baseurl + data.url,

            data: DATA,

            dataType: 'json',

            crossDomain: true,

            cache: false,

            type: (typeof data.type != 'undefined' ? data.type : 'Post'),

        };

        $.ajax(ajxOpts).done(function(res) {

            callback(res);

        }).fail(function(r) {

            callback('fail');

        });

    }

    callTry(data, callback);

}
window.setTimeout(function() {
    $(".alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 4000);
//################## Ajax Call #####################

$(document).ready(function () {
		
	
    $(document).on('click',".btn-number",function (e) {
        e.preventDefault();

        var fieldName = $(this).attr('data-field');

        var type = $(this).attr('data-type');

        var input = $("input[name='" + fieldName + "']");

        var currentVal = parseInt(input.val());

        if (!isNaN(currentVal)) {

            if (type == 'minus') {

                var minValue = parseInt(input.attr('min'));

                if (!minValue) minValue = 1;

                if (currentVal > minValue) {

                    input.val(currentVal - 1).change();

                }

                if (parseInt(input.val()) == minValue) {

                    $(this).attr('disabled', true);

                }



            } else if (type == 'plus') {

                var maxValue = parseInt(input.attr('max'));

                if (!maxValue) maxValue = 9999999999999;

                if (currentVal < maxValue) {

                    input.val(currentVal + 1).change();

                }

                if (parseInt(input.val()) == maxValue) {

                    $(this).attr('disabled', true);

                }



            }

        } else {

            input.val(0);

        }

    });
    $(document).on('focusin',".input-number",function (e) {
        $(this).data('oldValue', $(this).val());

    });
    $(document).on('change',".input-number",function (e) {

        var minValue = parseInt($(this).attr('min'));

        var maxValue = parseInt($(this).attr('max'));

        if (!minValue) minValue = 1;

        if (!maxValue) maxValue = 9999999999999;

        var valueCurrent = parseInt($(this).val());



        var name = $(this).attr('name');

        if (valueCurrent >= minValue) {

            $(".btn-number[data-type='minus'][data-field='" + name + "']").removeAttr(

                'disabled')

        } else {

            alert('Sorry, the minimum value was reached');

            $(this).val($(this).data('oldValue'));

        }

        if (valueCurrent <= maxValue) {

            $(".btn-number[data-type='plus'][data-field='" + name + "']").removeAttr('disabled')

        } else {

            alert('Sorry, the maximum value was reached');

            $(this).val($(this).data('oldValue'));

        }





    });

    $(document).on('keydown',".input-number",function (e) {

        // Allow: backspace, delete, tab, escape, enter and .

        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||

            // Allow: Ctrl+A

            (e.keyCode == 65 && e.ctrlKey === true) ||

            // Allow: home, end, left, right

            (e.keyCode >= 35 && e.keyCode <= 39)) {

            // let it happen, don't do anything

            return;

        }

        // Ensure that it is a number and stop the keypress

        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode >

                105)) {

            e.preventDefault();

        }

    });

    $('.hover_show_list > li').hover(function(){

        var id = $(this).attr('data-index');

        $('.hover_show_list li').removeClass('active');

        $(this).addClass('active');

        $('.tab_hover_main > .hover_show_div').hide();

        $('.tab_hover_main > .hover_show_div[data-id='+id+']').show();

    });

    $('.nav_menu_list li').hover(function(){

        var id = $(this).attr('data-id');

        $('.nav_menu_list li').removeClass('active');

        $(this).addClass('active');

    });

    $('.nav_menu_list > li').click(function(){

        // var index = $(this).index();

        var index = $(this).attr('data-id');

        $('.nav_menu_list > li').removeClass('opened');

        $(this).toggleClass('opened');

        $('.header-menu-top').toggleClass('opened');

        $('.header-menu-top').find('.tog-menu-header').addClass('hide');

        // $('.header-menu-top').find('.tog-menu-header:eq('+ index +')').removeClass('hide');

        $('.header-menu-top').find('.tog-menu-header[id='+ index +']').removeClass('hide');

    });     
    $(document).on('click','.remove-from-cart',function(e){	
        e.preventDefault();	
        var sku    = $(this).attr('sku');
       
        call({ url: '/cart/remove-cart', params: { 'sku':sku}, type: 'GET' }, function(resp) {
                  if(resp.status == true){                              
                       $('#header-cart').html(resp.header_cart_html);
                       $('#view-cart-section').html(resp.view_cart_holder_html);
                  }else{
                  
                  }

       });
   });
	$(document).on('click','.addtocart',function(e){	
         e.preventDefault();		 
		 var th =$(this);
		 $(".click-productVBox").remove();
		 th.html('<i class="fa fa-spin fa-circle-o-notch fa-2x"></i>');
		 var sku    = $(this).attr('data-sku');
		 call({ url: '/cart/add-to-cart', params: { 'sku':sku,'qty':1}, type: 'GET' }, function(resp) {		
		 		   th.html('Add to cart'); 			
                   if(resp.status == true){
				   		$('#'+sku).prepend(resp.html);   
                        $('#'+sku).find('.click-productVBox').show();                         
                        $('#header-cart').html(resp.header_cart_html);
				   }else{
				   
				   }

        });
    });
    
    $(document).on('submit','.add-to-cart-form',function(e){	
        e.preventDefault();	
        var th = $(this);	 
        var formData =$(this).serialize();
        $(".click-productVBox").remove();
        th.find('input[type="submit"]').html('<i class="fa fa-spin fa-circle-o-notch fa-2x"></i>');
        var sku    = $(this).attr('data-sku');
        call({ url: '/cart/add-to-cart', params: formData, type: 'GET' }, function(resp) {		
                  th.find('input[type="submit"]').html('Add to cart'); 			
                  if(resp.status == true){                    
                          $('#'+sku).prepend(resp.html);   
                          $('#'+sku).find('.click-productVBox').show(); 
                          $('#header-cart').html(resp.header_cart_html);
                  }else{
                  
                  }

       });
   });
   $(document).on('submit','#search-form',function(e){	
        if($('#basics').val()=="") return false;
        $(this).submit();
   });   
   $(document).on('click','.close-cart',function(e){
            $(this).parent(".click-productVBox").remove();
   });  
   var options = {
        url: function(phrase) { 

            if(parseInt(phrase.length)>2){
                return baseurl + "/search/index?str=" + phrase;
            }           
        },
        getValue: "name",
        list: {
            onLoadEvent: function() {
               
            },
            match: {
                enabled: true
            },
            showAnimation: {
                type: "fade", //normal|slide|fade
                time: 200,
                callback: function() {
                   
                }
            },
    
            hideAnimation: {
                type: "slide", //normal|slide|fade
                time: 200,
                callback: function() {
                   
                }

            },
            sort: {
                enabled: true
            },
            maxNumberOfElements: 200,
        },        
        template: {
            type: "links",
            fields: {
                link: "url"
            }
        }
    };
    $("#basics").easyAutocomplete(options);

});