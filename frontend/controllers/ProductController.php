<?php

namespace frontend\controllers;
use \common\models\Product;
use \common\models\Category;
use \common\models\ProductCombination;

use \common\models\ProductCategory;
use \common\models\ProductRelated;
use yii\helpers\ArrayHelper;
use Yii;

class ProductController extends \yii\web\Controller
{
	public $enableCsrfValidation = false;
    public function actionCategory($slug){
        $Category             =  Category::find()->where(['slug'=>$slug])->asArray()->one();
        return $this->render('categoryProduct',[
            'Category'   =>  $Category,
        ]);
    }
	public function actionLoadProducts(){
        $limit        = !empty($_POST['limit'])?$_POST['limit']:0;
        $category_id  = !empty($_POST['category_id'])?$_POST['category_id']:0;

        $CategoryIds   = ArrayHelper::map(Yii::$app->general->recursiveCategory($category_id),'id','id');
        $CategoryIds   =  array($category_id)+$CategoryIds;
        $CategoryIds   = !empty($CategoryIds)?implode(",",$CategoryIds) :array();
        $TotalResult   = ProductCategory::find()->joinWith('product')
                        ->where('map_product_category.category_id IN ('.$CategoryIds.')')->count();
						
		$Products      = ProductCategory::find()->joinWith('product')
                        ->where('map_product_category.category_id IN ('.$CategoryIds.')')
                        ->orderBy(' position asc')->limit(30)->offset($limit)->all();
 	
		$start 				= $limit+1;
		$end 				= $limit+30 >= $TotalResult?$TotalResult: $limit+30;
		$res['TotalResult'] = !empty($TotalResult)?$TotalResult:0;
		$res['summary']     = 'Showing '.($start).' - '.($end).' Of '.$TotalResult;
		$res['html']        = "";
		$res['html']        =   $this->renderAjax('_products',[
							 		'Products'   =>  $Products,
								]);
		if($limit+30 >= $TotalResult){
			$res['status']      = false;
		}else{
			$res['status']      = true;
		}		
		echo json_encode($res);die;
	}
    public function actionView($slug)
    {
        $Product             =  Product::find()->where(['slug'=>$slug])->asArray()->one();
        $ProductCombination  =  ProductCombination::find()->select(['product.slug as slug','product.name as name'])
                                ->leftJoin('product', 'product_combination.child_product_id=product.id')
                                ->where(['product_combination.parent_product_id'=>$Product['id']])
                                ->asArray()->all();
        $ProductRelated      =  ProductRelated::find()->select(['product.*'])
                                ->leftJoin('product', 'product_related.child_product_id=product.id')
                                ->where(['product_related.parent_product_id'=>$Product['id']])
                                ->asArray()->all();
        return $this->render('productDetails',[
            'Product'           =>  $Product,
            'ProductCombination'=>  $ProductCombination,
            'ProductRelated'    =>  $ProductRelated
        ]);
    }


}
