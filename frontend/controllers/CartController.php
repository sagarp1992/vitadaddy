<?php
namespace frontend\controllers;
use \common\models\Product;
use \common\models\Cart;
use \common\models\Customer;
use \common\models\MapCardCustomerCart;
use \common\models\PaymentCard;
use \common\models\Order;
use \common\models\OrderDetaills;
use \yii\helpers\Html;
use yii\widgets\ActiveForm;

use Yii;

class CartController extends \yii\web\Controller

{
     public $enableCsrfValidation = false;
	 private function prepare(){
		$action   =  Yii::$app->controller->action->id;
        if(Yii::$app->cart->cartId() == 0){			
            Yii::$app->session->setFlash('info','Please add item in cart.');
            return $this->redirect(['/site/index']);
        }else{
			switch($action){
				case 'shipping':
					$Customer = Yii::$app->cart->CustomerInCart();
					if($Customer['status'] == false || $Customer['customer_id'] == 0){
						 return $this->redirect(['/customer/create']);
					}
					break;
				case 'payment':
					$Customer = Yii::$app->cart->CustomerInCart();					
					if($Customer['status'] == false || $Customer['shipping_id'] == 0){
						 return $this->redirect(['/cart/shipping']);
					}					
					break;
			}
		}
        return ;
    }

    public function actionAddToCart($sku,$qty=1,$total_qty=0){  
       //
        $CartData = array('sku'=>$sku,'qty'=>$qty,'total_qty'=>$total_qty);
        $res =  Yii::$app->cart->AddToCart($CartData);
        
        $res['view_cart_holder_html']     = "";
        if($res['status']== true){
            $Product 		             =   Product::find()->where(['sku'=>$sku])->one();            
            $res['html']                 =   $this->renderAjax('_ajax_cart',[
							 	        	    'Product'   =>  $Product,
                                            ]);
            if($total_qty > 0){
                $res['view_cart_holder_html']     =    $this->renderAjax('//cart/_cart-items');
            }
            $res['header_cart_html']     =   $this->renderAjax('//cart/_header_cart');
        }else{

              Yii::$app->session->setFlash('cart_failed', $res['message']);

        }
		echo json_encode($res);die;
    }

    public function actionRemoveCart($sku){
        $CartData = array('sku'=>$sku);
        $res =  Yii::$app->cart->RemoveItemFromCart($CartData);  
        if($res['status']== true){
            $res['view_cart_holder_html']     =    $this->renderAjax('//cart/_cart-items');
            $res['header_cart_html']          =    $this->renderAjax('//cart/_header_cart');
        }else{
            Yii::$app->session->setFlash('view_cart_failed', $res['message']);
        }
        echo json_encode($res);die;
    }
    public function actionViewCart()

    {

        $this->prepare();

        return $this->render('cart');

    }

	 public function actionPayment(){
		$this->prepare();
        $model = new PaymentCard();         
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format =  \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()) && $model->save()) {

            $res = Yii::$app->cart->placeOrder($model->id);    

            return $this->redirect(['confirmation','order_id'=>$res['order_id']]);  

        }

        return $this->render('payment', [

            'model' => $model,

        ]);



        return $this->render('payment');

    }

	 public function actionConfirmation($order_id)

    {
		
        $model = Order::find()->select(['order.*','payment_card.type as type'])
                ->leftJoin('payment_card','order.paymen_card_id = payment_card.id')
                ->where(['order.order_no'=>$order_id])->asArray()
                ->one();
        $OrderDetaills  =   OrderDetaills::find()->select(['product.*','order_detaills.*'])->where(['order_id'=>$model['id']])
                             ->leftJoin('product', 'order_detaills.product_id=product.id')->asArray()
                             ->all();


        return $this->render('confirmation',['model'=> $model,'OrderDetaills'=>$OrderDetaills]);

    }

	public function actionShipping(){		
         $this->prepare();
         return $this->render('shipping');

    }
    public function actionAddShipping(){	
        $res = array('status'=>false,'message'=>'Please select shipping method.','next_button'=>"");
        if (!empty($_POST['shipping_method'])) {
            $res = Yii::$app->cart->AddShippingToCart(array('shipping_id'=>$_POST['shipping_method']));    	
            $res['status']                 =    true;
            $res['_cart_sidebar_html']     =    $this->renderAjax('//cart/_cart_sidebar');
            $res['next_button']            =    Html::a('NEXT',['/cart/payment'],['class' => 'checkout-btn btn-continue']);                      
        }		
        echo json_encode($res);die;

    }
    public function actionExistCustomer(){
        $res['status']            = false;
        $res = Yii::$app->cart->CustomerInCart(); 
        if($res['customer_id'] == 0){
            $model  = Customer::find()->where(['email'=>$_POST['email']])->one();
            if(!empty($model)){
                $res['customer_form']     =    $this->renderAjax('//cart/_customer_form',['model'=>$model]);
                $res['status']            = true;
            }
        }
        echo json_encode($res);
    }
    public function actionCustomer()
    {
        $res = Yii::$app->cart->CustomerInCart(); 
        if($res['status']==true && !empty($res['customer_id']) && $res['customer_id'] > 0){
            $model  = Customer::find()->where(['id'=>$res['customer_id']])->one();
        }else{
            $model = new Customer();   
        } 
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $CustomerData   = array('customer_id'=>$model->id);
            $res = Yii::$app->cart->AddCustomerToCart($CustomerData);
            if($res['status']== true){
                return $this->redirect(['cart/shipping']);
            }else{
                Yii::$app->session->setFlash('customer_failed', $res['message']);
            }           
        }

         
        return $this->render('customer', [
            'model' => $model,
        ]);
    }

}

?>