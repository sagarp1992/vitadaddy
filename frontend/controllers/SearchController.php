<?php

namespace frontend\controllers;
use \common\models\Product;
use \common\models\Category;
use \common\models\ProductCategory;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\helpers\html;
use Yii;

class SearchController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;   
    public $layout = "main";
	public function actionIndex($str){
        $Items = array();
        $Product =Yii::$app->db->createCommand("SELECT name,slug,'product' as type,id as parent FROM product WHERE MATCH(name) AGAINST(:str) 
        UNION (SELECT a.name,a.slug,'category' as type,b.name as parent FROM category AS a, category AS b WHERE a.parent_id=b.id and MATCH(a.name) AGAINST(:str) )
        ORDER BY `type` ASC")
                    ->bindValue(':str',$str)
                    ->queryAll();
        foreach($Product as $ele){
            if($ele['type']=="product"){
                $item['name'] =  $ele['name'];
            }else{                
                  $item['name'] =  $ele['name'].'('.$ele['parent'].')';
            }
            $item['url']  =  $ele['type']=="product"? Url::to(['product/view', 'slug' =>  $ele['slug']]):Url::to(['product/category', 'slug' =>  $ele['slug']]);
            array_push($Items,$item);
        }
        echo json_encode($Items);
    }
    public function actionLoadCategory(){
        $str          = !empty($_POST['search'])?$_POST['search']:"";    
        $ProductCategory = Product::find()->select(['count("map_product_category.id") as category_count','category.name','category.id as category_id'])
        ->leftJoin('map_product_category','product.id=map_product_category.product_id')
        ->leftJoin('category','map_product_category.category_id=category.id')
        ->where(['like', 'product.name', $str])->groupBy(['map_product_category.category_id'])
        ->orderBy('category.name ASC')->asArray()->all();  
         $html = "";
         if(!empty($ProductCategory)){
            $html.=" <h4 class='filter-title'>Category</h4>";
            $html.='<ul  class="bredcumb-ul">';
            foreach($ProductCategory as $ele){
                if($ele['name']=="") continue;
                $html.='<li>'.Html::a($ele['name'].'('.$ele['category_count'].')',['/search/full-search','category_id'=>$ele['category_id']]).'</li>';
            }
            $html.=' </ul>';
         }
         $res['html'] = $html;
         echo json_encode($res);die;
    }
    public function actionLoadBrand(){
        $str          = !empty($_POST['search'])?$_POST['search']:"";    
        $ProductBrandCount = Product::find()->select(['count("id") as brand_count','brand'])->where(['like', 'name', $str])->groupBy(['brand'])->orderBy('brand_count DESC')->limit(7)->asArray()->all();  
        $html = "";
        if(!empty($ProductBrandCount)){
           $html.=" <h4 class='filter-title'>Top Brand</h4>";
           $html.='<ul  class="bredcumb-ul">';
           foreach($ProductBrandCount as $ele){
               if($ele['brand']=="") continue;
               $html.='<li>'.Html::a($ele['brand'].'('.$ele['brand_count'].')',['/search/full-search','brand'=>$ele['brand']]).'</li>';
           }
           $html.=' </ul>';
        }
        $res['html'] = $html;
        echo json_encode($res);die;
    }
    public function actionLoadProducts(){
        $limit        = !empty($_POST['limit'])?$_POST['limit']:0;
        $str          = !empty($_POST['search'])?$_POST['search']:"";    
        
        $str          = !empty($_POST['search'])?$_POST['search']:"";  

        $TotalResult = Product::find()->where(['like', 'name', $str])->count();        
        $Product = Product::find()->where(['like', 'name', $str])->asArray()->all();  
      
        $start 				= $limit+1;
        $end 				= $limit+30 >= $TotalResult?$TotalResult: $limit+30;
        $res['TotalResult'] = !empty($TotalResult)?$TotalResult:0;
        $res['summary']     = 'Showing '.($start).' - '.($end).' Of '.$TotalResult;
        $res['html']        =  $this->renderAjax('_product_search',[
                                    'Products'   =>  $Product,
                            ]);                              
		if($limit+30 >= $TotalResult){
			$res['status']      = false;
		}else{
			$res['status']      = true;
		}		
		echo json_encode($res);die;
	}
    public function actionFullSearch(){
        $this->layout="main";
        $str            =  !empty($_GET['search'])?$_GET['search']:"";
        $category_id    =  !empty($_GET['category_id'])?$_GET['category_id']:"";
        $TotalResult = Product::find()->where(['like', 'name', $str])->count();        
        if(!empty($str) && $TotalResult>0){
            return $this->render('fullsearch',[
                'word'=>$str 
            ]);
        }else{
            return $this->render('_empty_search',[
                'word'=>$str 
            ]);
        }
       
    }
}