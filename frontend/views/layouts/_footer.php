<?php
use yii\helpers\Html;
?>
<footer>
            <div class="container">
                <div class="row">
                    <div class=" col-sm-12 foottitle">
                        <h4>INSPIRE, NOURISH, THRIVE EVERY DAY.</h4>
                    </div>

                    <div class=" col-sm-3 col-md-3 col-lg-3 footcon">
                        <h2>
                            <span>HELP & </span> MY ACCOUNT</h2>
                        <ul>
                            <li>
                            <?= Html::a('MY ACCOUNT', array('#')); ?> 
                            </li>
                            <li>
                            <?= Html::a('HELP', array('#')); ?> 
                            </li>
                            <li>
                            <?= Html::a('MY HEALTHY AWARDS', array('#')); ?> 
                            </li>
                            <li>
                            <?= Html::a('ORDER TRACKING & HISTORY', array('#')); ?> 
                            </li>
                            <li>
                            <?= Html::a('QUICK REORDER', array('#')); ?> 
                            </li>
                            <li>
                            <?= Html::a('ORDER BY ITEM NUMBER', array('#')); ?> 
                            </li>
                            <li>
                            <?= Html::a('FAQs', array('#')); ?> 
                            </li>
                        </ul>
                    </div>

                    <div class=" col-sm-3 col-md-3 col-lg-3 footcon">
                        <h2>
                            <span>VALUE & </span> SAVINGS</h2>

                        <ul>
                            <li>
                                <?= Html::a('VITAMIN SHOPPE BRAND', array('#')); ?> 
                            </li>
                            <li>
                                  <?= Html::a('FREE SHIPPING INFORMATION', array('#')); ?> 
                            </li>
                            <li>
                                <?= Html::a('SIGN & SAVE', array('#')); ?> 
                            </li>
                            <li>
                                <?= Html::a('SALE PRODUCTS', array('#')); ?> 
                            </li>
                            <li>
                                <?= Html::a('HEALTHY AWARDS (NEW)', array('#')); ?> 
                            </li>
                        </ul>
                    </div>

                    <div class=" col-sm-3 col-md-3 col-lg-3 footcon">
                        <h2>
                            <span>POLICIES & </span> SERVICES</h2>

                        <ul>
                            <li>
                                <?= Html::a('PRIVACY POLICY', array('#')); ?> 
                            </li>
                            <li>
                                <?= Html::a('SHIPPING RATES & POLICIES', array('#')); ?> 
                            </li>
                            <li>
                                <?= Html::a('RETURNS POLICY', array('#')); ?> 
                            </li>
                            <li>
                                <?= Html::a('TERMS OF USE (UPDATED 10/9/17)', array('#')); ?> 
                            </li>
                            <li>
                                <?= Html::a('AFFILIATE PROGRAM', array('#')); ?> 
                            </li>
                            <li>
                                <?= Html::a('GIFT CARDS', array('#')); ?> 
                            </li>
                        </ul>
                    </div>

                    <div class=" col-sm-3 col-md-3 col-lg-3 footcon">
                        <h2>
                            <span>CORPORATE </span> INFORMATION</h2>
                        <ul>
                            <li>
                                 <?= Html::a('ABOUT THE VITAMIN SHOPPE', array('#')); ?> 
                            </li>
                            <li>
                                 <?= Html::a('CONTACT US', array('#')); ?> 
                            </li>
                            <li>
                                 <?= Html::a('STORE LOCATOR', array('#')); ?> 
                            </li>
                            <li>
                                <?= Html::a('INVESTOR RELATIONS', array('#')); ?> 
                            </li>
                            <li>
                                <?= Html::a('CAREERS', array('#')); ?> 
                            </li>
                            <li>
                                <?= Html::a('SITE MAP', array('#')); ?> 
                            </li>
                            <li>  
                                <?= Html::a('NEW SUPPLIERS', array('#')); ?>                               
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footbottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul>
                                <li>
                                    <span>HONORS & </span> AWARDS:</li>
                                <li>
                                    <?= Html::a(Html::img(Yii::$app->request->baseUrl.'/img/footer-award-image1.jpg'), array('#')); ?> 
                                </li>
                                <li>
                                    <?= Html::a(Html::img(Yii::$app->request->baseUrl.'/img/19205_coe.jpg'), array('#')); ?> 
                                </li>
                                <li>
                                    <?= Html::a(Html::img(Yii::$app->request->baseUrl.'/img/footer-award-image3.jpg'), array('#')); ?> 
                                </li>
                                <li>
                                    <?= Html::a(Html::img(Yii::$app->request->baseUrl.'/img/footer-award-image4.jpg'), array('#')); ?> 
                                </li>
                                <li>
                                    <?= Html::a(Html::img(Yii::$app->request->baseUrl.'/img/1705-vitamin-angels-site-logo.png'), array('#')); ?>                                   
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
    </footer>