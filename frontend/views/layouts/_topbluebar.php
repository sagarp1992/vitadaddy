<?php
use yii\helpers\Html;
use yii\widgets\Menu;
use \common\models\Category;
$Item   = createItem(null);
function createItem($parent_id){
    $Category=Category::find()->where(['parent_id'=>$parent_id])->all();
    $Itesm  =array();
    foreach($Category as $f=>$each){
        $Itesm[$f]['label']          =  $each['name'];
        $Itesm[$f]['url']            =  array('/product/category','slug'=>$each['slug']);
        $Itesm[$f]['items']          =  createItem($each['id']);       
    }   
     return $Itesm;
}

?>

<section class="topblueBar">

            <div class="container">

                <div class="row">

                    <div class="mobilemenu" style="display:none;">

                        <a href="#menu" class="menuicon">

                            <span>

                                <img src="<?= Yii::$app->request->baseUrl.'/';?>img/menu-item.png">

                                <img src="<?= Yii::$app->request->baseUrl.'/';?>img/close.png" style="display:none" class="closeicon">

                            </span>

                        </a>
                        <nav id="menu">

							<?php
                            echo Menu::widget([
								'items' => $Item
							]);
							?>
                        </nav>

                    </div>

                    <div class="col-sm-4 col-md-4 col-lg-4 logo">
                       	<?= Html::a('<img src="'.Yii::$app->request->baseUrl.'/img/Logo.png" alt="Logo">',['site/index']);?>
                    </div>
                    <div class="col-sm-8 col-md-8 col-lg-8 blueBar-Right">

                        <ul>

                            <li class="signin">

                                <a href="javascript:toggleDiv('myContent');">Sign In/Register</a>

                            </li>

                            <li class="reorder">

                                <a href="#">Reorder</a>

                            </li>

                            <li class="cartlink" id="header-cart">
                                    <?= $this->render('//cart/_header_cart');?>
                            </li>

                        </ul>

                    </div>



                </div>

            </div>

</section>

