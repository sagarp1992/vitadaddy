<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use \common\models\Category;
$MainMenuList = Category::find()->where(['parent_id'=>null])->orderBy('position ASC')->asArray()->all();
if(!empty($MainMenuList)){
    foreach($MainMenuList as $main){
        $menuItems[] = ['label' =>$main['name'], 'url' =>'javascript:void(0);','options'=> ['data-id'=>'main-'.$main['id']]];
    } 
    $MainMenu   =  Nav::widget([
        'options' => ['class' => 'nav_menu_list','id'=>''],
        'items' => $menuItems,
    ]);
?>
<section class="mainNavBar">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-8 navarea">
                 <!-- #####################  Main Menu ################ -->
                    <?= $MainMenu;?>   
                <!-- #####################  Main Menu ################ -->                    
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 searcharea">
                <div class="searchbox">
                     <?= Html::beginForm(['search/full-search'], 'GET', ['id' => 'search-form']) ?>
                             <?= Html::input('text', 'search',!empty($_GET['search'])?$_GET['search']:"", ['class' =>'searchfield','id'=>'basics','placeholder'=>"What can we help you find today?"]); ?>                           
                             <?= Html::submitButton('SEARCH', ['class' => 'searchbtn']) ?>
                    <?= Html::endForm() ?>
                </div>
            </div>
        </div>
    </div>
    <div class="header-menu-top">
        <?php 
            foreach($MainMenuList as $ele) {   
                $SubMenuList    =   Category::find()->where(['parent_id'=>$ele['id']])->orderBy('position ASC')->asArray()->all();  
                if(!empty($SubMenuList)){                                        
            ?>
                <div class="tog-menu-header hide" id="main-<?php echo $ele['id'];?>">
                    <div class="bg-menuwhite">                           
                        <div class="row">
                            <div class="col-md-2 col-sm-2 p-0">
                                <div class="sum-menu-style clearfix">
                                     <!-- #####################  Sub Menu ################ -->
                                    <?php 
                                                                                        
                                        $submenuItems    = [];
                                        foreach($SubMenuList as $k=> $sub) { 
                                                $ac= ($k==0)?"active":"";                                                                                                 
                                                $submenuItems[] = ['label' =>$sub['name'], 'url'=>['/product/category','slug'=>$sub['slug']],'options'=> ['data-index'=>'sub-'.$sub['id'],'class'=>$ac]];
                                        }
                                        $Submenu   =  Nav::widget([
                                                'options' => ['class' => 'hover_show_list','id'=>'sub-menu'],
                                                'items' => $submenuItems,
                                              //  'firstItemCssClass'=>'active'
                                        ]);  
                                        echo $Submenu;                                              
                                    ?>
                                     <!-- ##################### Sub Menu ################ -->
                                </div>
                            </div>
                              <!-- #####################  Sub Menu hover ################ -->
                            <div class="col-md-10 col-sm-10 tab_hover_main">
                                <?php foreach($SubMenuList as $sub) {   
                                        $ChildMenuList    =   Category::find()->where(['parent_id'=>$sub['id']])->orderBy('position ASC')->asArray()->all();  
                                        if(!empty($ChildMenuList)){  
                                ?>
                                                <div class="menu-header-right hover_show_div" data-id="sub-<?php echo $sub['id'];?>">
                                                    <div class="menu-title">
                                                            <h3><?php echo $sub['name'];?><span></span> </h3>
                                                    </div>
                                                    <div class="menu-li-side clearfix">
                                                        <!-- #####################  Child Menu ################ -->
                                                        <?php foreach($ChildMenuList as $child){ 
                                                            $TailChildMenuList    =   Category::find()->where(['parent_id'=>$child['id']])->orderBy('position ASC')->asArray()->all();  
                                                        ?>
                                                        <div class="menu-category">
                                                                <?php
                                                                    echo Html::a('<span>'.$child['name'].' <span class="right-arrowSpan"></span></span>', ['/product/category','slug'=>$child['slug']]);
                                                                ?>
                                                                <?php                                                                                         
                                                                        $tailChildItems    = [];
                                                                        foreach($TailChildMenuList as $k=> $tailChild) { 
                                                                                $ac= ($k==0)?"active":"";                                                                                                 
                                                                                $tailChildItems[] = ['label' =>$tailChild['name'],'url'=>['/product/category','slug'=>$tailChild['slug']]];
                                                                        }
                                                                        $tailChildMenu  =  Nav::widget([
                                                                                'options' => [],
                                                                                'items' => $tailChildItems,
                                                                                //  'firstItemCssClass'=>'active'
                                                                        ]);  
                                                                        echo $tailChildMenu;                                              
                                                                ?>
                                                            </div> 
                                                        <?php } ?>
                                                        <!-- #####################  Child Menu ################ -->

                                                    </div>
                                                </div> 
                                    <?php } ?> 
                                <?php } ?>                                                                  
                            </div>
                              <!-- #####################  Sub Menu  hover ################ -->
                        </div>
                    </div>
                </div> 
                <?php 
                } 
                } 
            ?>
    </div>
</section>
<?php  } ?>