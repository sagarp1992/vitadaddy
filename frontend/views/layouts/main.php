<?php



/* @var $this \yii\web\View */

/* @var $content string */



use yii\helpers\Html;

use yii\bootstrap\Nav;

use yii\bootstrap\NavBar;

use yii\widgets\Breadcrumbs;

use frontend\assets\AppAsset;

use common\widgets\Alert;

AppAsset::register($this);

?>

<?php $this->beginPage() ?>

<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">

<head>

    <meta charset="<?= Yii::$app->charset ?>">

    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?= Html::csrfMetaTags() ?>

    <title><?= Html::encode($this->title) ?></title>

    <link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::$app->request->baseUrl.'/';?>img/16x16.png">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" type="text/css">

    <script> 

             var baseurl = "<?= Yii::$app->getUrlManager()->getBaseUrl();?>";

    </script>

    <?php $this->head() ?>

</head>

<body>

<?php $this->beginBody() ?>

<div id="page">

    <?= $this->render('_topsection'); ?>

    <?= $this->render('_topbluebar'); ?>

    <?= $this->render('_dropsignin'); ?>

    <?= $this->render('_mainnavbar'); ?>

         <?= $content; ?>

    <?= $this->render('_footer') ?>

</div>

<?php $this->endBody() ?>

</body>

</html>

<?php $this->endPage() ?>

