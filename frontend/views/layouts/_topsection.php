<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use \common\models\Flatext;
$Flattext   =   ArrayHelper::map(Flatext::find()->where(1)->asArray()->all(),'name','text');
?>
<section class="topSection">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-sm-4 col-md-4 topLt">
                        <h3><?= $Flattext['top_first_header'];?></h3>
                    </div>
                    <div class="col-lg-4 col-sm-12 col-md-6 topMid">
                        <h3>
                        <?= $Flattext['top_second_header'];?></h3>
                    </div>
                    <div class="col-lg-4 col-sm-6 col-md-6 topRt">
                        <ul>
                            <li>
                                <a href="#">Live Chat</a>
                                <div class="dropbg">
                                    <h2>Product Questions?</h2>

                                    <a href="#">Chat with
                                        <br> Health Enthusiast</a>
                                </div>
                            </li>
                            <li>
                                <a href="#">Help</a>
                            </li>
                            <li>
                                <a href="#">Store Locator</a>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
</section>
