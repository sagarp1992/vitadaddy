<section class="dropsignin" id="myContent" style=" display:none">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="logoin">
                            <h3>Sign In</h3>
                            <div class="signinform">
                                <input name="" type="text" placeholder=" | Enter your email address">
                                <input name="" type="text" placeholder=" | Password">
                                <div class="clearfix"></div>
                                <input name="" type="button" value="Sign In" class="loginbtn">
                                <a href="#">FORGOT PASSWORD</a>
                            </div>
                        </div>
                        <div class="logoin">
                            <h3>Register an account</h3>
                            <p>Check out faster, track purchases, save your preferences, and get personalized recommendations</p>
                            <div class="signinform">
                                <input name="" type="text" placeholder=" | Enter your email address">
                                <div class="clearfix"></div>
                                <input name="" type="button" value="Register" class="loginbtn">
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-8 col-sm-8">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 srCon">
                                <h3>Healthy Awards</h3>
                                <p>It’s free to join. Redeem your points for free merchandise. Earn 1 point for every dollar
                                    you spend – the more you buy, the more you save.</p>

                                <ul>
                                    <li>$5 in awards for every 100 points</li>
                                    <li>Advanced sales, coupons, and special promotions</li>
                                    <li>Exclusive, members-only, valuable coupons</li>
                                    <li>Offers: invitations to special in-store events – health screenings, celebrity appearances,
                                        seminars and more...</li>
                                </ul>

                                <p class="margin-top">
                                    <img src="<?= Yii::$app->request->baseUrl.'/';?>img/awardimg.png" width="124" height="80" class="awardimg">Ready to earn points with every purchase?
                                    <br>
                                    <strong>Register for an account and start redeeming!</strong>
                                </p>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 srCon">
                                <h3>Quick Reordering</h3>
                                <p>Reordering the products you need has never been easier! As a Vitamin Shoppe member, reorder
                                    quickly and easy from any device.</p>

                                <h3>Auto Delivery</h3>
                                <p>Auto Delivery is the most convenient way to enjoy your favorite vitamin Shoppe products.</p>

                                <ul>
                                    <li>We send you your chosen items based upon a frequency that you set</li>
                                    <li>You can make changes online whenever you need</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>