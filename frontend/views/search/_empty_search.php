<?php
    $this->title = $word.'- Vitadaddy';
?>
<section class="categoryMainContent">
  	<div class="container">
        <div class="row">
                <h3>We're sorry, but no results were found for "<?=$word?>".</h3>
        </div>
        <div class="row">
        <div class="col-md-12">
            <h6 class="text-mute clearfix"> Here are a few tips to help you find what you're looking for:         
                <ul class="clearfix">
                    <li> Check your spelling and search again  </li>
                    <li> Try another search term  </li>
                    <li> Browse our specialty shops </li>
                </ul>
            </h6>
            </div>
        </div>
   </div>
</section>