<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
if(!empty($Products)){
	foreach($Products as $model){
?>


    <div class="cat-product-con" id="<?=$model['sku'];?>">

        <div class="catImg">

            <?= Html::a(Html::img($model['image']), array('/product/view','slug'=>$model['slug'])); ?>

        </div>

        <h1>
            <?= Html::a(Html::encode($model['name']),array('/product/view','slug'=>$model['slug'])); ?>
        </h1>

        <h5 class="c-price">Rs
            <?= Html::encode($model['retail_price']);?>
        </h5>

        <h5 class="sale-price">Sale Rs
            <?= Html::encode($model['price']);?>
        </h5>

        <?= Html::a('Add to Cart', array('/cart/add-to-cart','sku'=>$model['sku'],'_pjax'=>'#p0'),['class'=>"addtocart",'data-sku'=>$model['sku']]);?>
            <div class="spin-Div hide">
                <i class="fa fa-spinner fa-spin" style="font-size:24px"></i>
            </div>
    </div>
<?php } } ?>