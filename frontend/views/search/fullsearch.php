<?php

use yii\helpers\Html;

use yii\helpers\ArrayHelper;

use yii\bootstrap\Nav;

use yii\widgets\ListView;

use \common\models\Category;

use \common\models\ProductCategory;

use yii\data\ActiveDataProvider;

use yii\widgets\Pjax;
$this->title = $word.'- Vitadaddy';

?>
<section class="categoryMainContent">
  	<div class="container">
      <div class="row m-20">
    	<div class="col-lg-12 pagetitle">
            <h2>Search <b><?= $word;?></b><small id="summary-text" class="pull-right"></small></h2>               
        </div>
    </div>
      <div class="row">    	
      	<div class="col-lg-3 col-sm-3 col-md-3 leftside">          
        
          <div id="filter-by-category">
              
          </div>
          <div id="filter-by-brand">
               
         </div>          
        </div>
        <div class="col-lg-9 col-sm-9 col-md-9 rightside">
        	<div  id="category-product-list">
            </div>
            <div id="category-product-list-loader" class="col-md-12 text-center">
                <i class="fa fa-spin fa-circle-o-notch fa-2x text-mute"></i>
                <p class="text-mute">Loading more Products...</p>
            </div>
        </div>       
      </div>   
   </div>
</section>  
<?php 
$this->registerJs("

var loadCategory = function(limit){   
    call({ url: '/search/load-category', params: {'search':'".$word."'}, type: 'POST' }, function(resp) {      
              $('#filter-by-category').html(resp.html); 
    });
}
loadCategory();

var loadBrand = function(limit){   
    call({ url: '/search/load-brand', params: {'search':'".$word."'}, type: 'POST' }, function(resp) {  
           $('#filter-by-brand').html(resp.html); 
    });
}
loadBrand();


var page = 0; var fl= false;
var loadProducts = function(limit){
		console.log(limit)
		fl= true;
	    $('#category-product-list-loader').show();
        call({ url: '/search/load-products', params: { 'limit':limit,'search':'".$word."'}, type: 'POST' }, function(resp) {      
                    $('#category-product-list-loader').hide();
					$('#category-product-list').append(resp.html); 
				    $('#summary-text').html(resp.summary); 
					if(resp.status == true){	
						fl = false;
					}else{
						fl= true;
					}

        });
}
loadProducts(page);
$(window).scroll(function () {
	if ($(window).scrollTop() > ($(document).height() - $(window).height()-$('footer').height()) && fl==false) {	
		console.log('page:'+page)
		page = page+30;
		loadProducts(page);
	}
});
");

?>
