<?php

use yii\helpers\Html;
?>

  <section>
    <div class="container">
      <div class="shoping-cart-main">
        <div class="tital-btn clearfix">
          <div class="title-cart">
            <h1>Order received</h1>
            <small class="thank-text">Thank you. your order has been received</small>
          </div>
        </div>
        <div class="cart-product-div clearfix">
          <div class="col-md-12">
            <div class="row">
              <div class="order-main">
                <div class="order-bg">
                  <div class="order-Div">
                    <small>order number :</small> <span><?= Html::encode($model['order_no']);?></span>
                  </div>
                  <div class="order-Div">
                    <small>date :</small> <span><?= Html::encode(date('F d,Y',$model['order_date']));?></span>
                  </div>
                  <div class="order-Div">
                    <small>total :</small> <span>$ <?= Html::encode($model['order_total']);?></span>
                  </div>
                  <div class="order-Div">
                    <small>payment method :</small> <span><?= Html::encode($model['type']);?></span>
                  </div>
                </div>
                <div class="clearfix">
                </div>
                <div class="title-cart">
                  <h1>Order Details</h1>
                </div>
                <div class="clearfix">
                </div>
                <div class="order-bg p-0 table-dataBG">
                  <div class="displayTable">
                  
                    <div class="displayRow">
                      <div class="displaycell">
                        Product
                      </div>                    
                      <div class="displaycell">
                        Qty
                      </div>
                      <div class="displaycell">
                        Sub Total
                      </div>
                    </div>
                    
                    
                    
                    
                    	 <?php
					 	 if(!empty($OrderDetaills)){

									foreach($OrderDetaills as $ele){

										?>
                                        <div class="displayRow">
                                              <div class="displaycell">
                                                <?= Html::encode($ele['name']);?><br>
                                                <small>Rs <?= Html::encode(number_format($ele['price']));?></small>
                                              </div>                                            
                                              <div class="displaycell">
                                                 <?= Html::encode($ele['qty']);?>
                                              </div>
                                              <div class="displaycell">
                                                Rs <?=number_format($ele['qty'] * $ele['price']);?>
                                              </div>
                                        </div>
									
									<?php

									}

								}

                    ?>
                    
                    
                   
                   
                   
                    <div class="displayRow">
                    	 <div class="displaycell"></div>
                      <div class="displaycell">
                        Subtotal
                      </div>
                      
                      <div class="displaycell">
                      Rs <?= Html::encode(number_format($model['order_total']));?>
                      </div>
                    </div> 
                    <div class="displayRow">
                    	 <div class="displaycell"></div>
                      <div class="displaycell">
                        Shpping
                      </div>
                      
                      <div class="displaycell">
                      Rs <?= Html::encode(number_format($model['shipping_charge']));?>
                      </div>
                    </div> 
                    <div class="displayRow">
                    	 <div class="displaycell"></div>
                      <div class="displaycell">
                        Tax
                      </div>
                      
                      <div class="displaycell">
                      Rs <?= Html::encode(number_format($model['tax_charge']));?>
                      </div>
                    </div>                  
                    <div class="displayRow">
                    <div class="displaycell"></div>
                      <div class="displaycell BR-none">
                        total
                      </div>
                      <div class="displaycell BR-none">
                        Rs <?= Html::encode(number_format($model['final_total']));?>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
