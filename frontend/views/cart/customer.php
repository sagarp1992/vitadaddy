<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<section>
    <div class="container">
      <div class="shoping-cart-main">
        <div class="tital-btn clearfix">
          <div class="title-cart">
            <h1>Shipping Address</h1>
          </div>
          <div class="cart-check-btn add-summary">
            <h3>Order Summary</h3>
          </div>
        </div>
        <div class="cart-product-div clearfix">
          <div class="col-md-12">
            <div class="row">
              <div class="bg-product-wht form-BG">
                <div class="row">
                  <div class="col-md-6">
                    <div class="Shiping-form" id="customer-form">                          
                          <?= $this->render( '_customer_form',['model'=>$model]); ?>
                    </div>
                  </div>
                  <div class="col-md-offset-3 col-md-3 col-sm-offset-3">
                       <?= $this->render( '_cart_sidebar'); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
  $this->registerJs("
     $(document).on('blur','#customer-email',function () {      
      call({ url: '/cart/exist-customer', params: { 'email':$(this).val()}, type: 'POST' }, function(resp) { 
        if (resp.status == true) {
           $('#customer-form').html(resp.customer_form);           
        }

    });

     });
    " );?>
