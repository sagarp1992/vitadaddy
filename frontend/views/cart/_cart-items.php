<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
$CartTotal = Yii::$app->cart->CartTotal();
?>
<div class="shoping-cart-main">
    <?php
        $CartList = Yii::$app->cart->CartList();      
        if(!empty($CartList)){

    ?>
        <div class="tital-btn clearfix">
            <div class="title-cart">
                <h1>Shopping cart</h1>
            </div>
            <div class="cart-check-btn">
            <?= Html::a('Check out now', ['/cart/customer'],['class' => 'checkout-btn']) ?>
            </div>
        </div>
        <div class="cart-product-div clearifx">
          <div class="col-md-12">      
            <div class="row">
              <div id="view_cart_holder_html" class="row">
                    <div class="bg-product-wht">
                    <?php
                        $retail=0;
                        foreach($CartList as $ele){
                          $retail = $retail+($ele['retail_price'] * $ele['qty']);
                    ?>
                    <div class="product-box-cart">
                    <div class="row">
                        <div class="col-md-9">
                          <div class="product-cart-img clearfix">
                            <div class="img-cart">
                              <?= Html::a(Html::img($ele['image']),array('product/view','slug'=>$ele['slug']),['target'=>'_blank','class'=>'text-decoration-none']); ?>
                              <div class="remove-product">
                                  <?= Html::a('<span><i class="glyphicon glyphicon-remove"></i></span> Remove','javascript:void(0);',['class'=>'remove-from-cart','sku'=>$ele['sku']]); ?>    
                              </div>
                            </div>
                            <div class="cart-img-data">
                                <?= Html::a('<span class="vitmsapn">'.$ele['brand'].'</span>
                              <h4 class="cart-title-img">'.$ele['name'].'</h4>
                              <span class="vitmsapn-item"> Item #: '.$ele['sku'].'</span>',array('product/view','slug'=>$ele['slug']),['target'=>'_blank','class'=>'text-decoration-none']); ?>
                              
                            </div>
                          </div>
                        </div>
                        <div class="col-md-3">
                          <div class="gray-bg-incras">
                            <div class="incres-price-light">
                              <span>Rs <?= $ele['price'];?></span>
                            </div>
                            <div class="incres-div">
                              <div class="input-group">
                                <div class="inputdiv">
                                    <?= Html::input('text',   'qty['.$ele['sku'].']',$ele['qty'], ['class' => "form-control cart-qty","min"=>"1","max"=>"100",'data-sku' =>$ele['sku']]);?>
                                </div>
                                <div class="inputdiv-btn">
                                  <span class="input-group-btn">
                                  <button type="button" class="btn btn-number" data-type="plus" data-field='qty[<?=$ele['sku'];?>]' > <span class="glyphicon glyphicon-plus"></span> </button>
                                  </span> <span class="input-group-btn">
                                  <button type="button" class="btn btn-number"  data-type="minus"  data-field='qty[<?=$ele['sku'];?>]' > <span class="glyphicon glyphicon-minus"></span> </button>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="incres-price-dark">
                            <span>Rs <?= $ele['price'] * $ele['qty'];?></span>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                      <?php }  ?> 
                  </div>
                  <div class="bg-product-wht-total">
                    <div class="price-box-podcut clearifx">
                      <div class="onewht-total clearifx">
                        <span class="left-sec-product"> subtotal </span> <span class="right-sec-product"> Rs <?=  $CartTotal;?> </span>
                      </div>
                      <div class="onewht-total clearifx">
                        <span class="left-sec-product"> Estimated shipping </span> <span class="right-sec-product">Free</span>
                      </div>
                      <div class="onewht-total clearifx">
                        <span class="left-sec-product"> tax varies by address </span> <span class="right-sec-product">Included IGST</span>
                      </div>
                      <?php
                          $saved = $retail-$CartTotal;
                          if($saved>0){
                      ?>
                      <div class="onewht-total clearifx">
                        <span class="left-sec-product color-red"> You saved </span> <span class="right-sec-product color-red">Rs <?= Html::encode(number_format($saved));?></span>
                      </div>
                      <?php  } ?>
                      <div class="onewht-total sub-blue clearifx">
                        <span class="left-sec-product"> <span class="icon-abou"><i class="glyphicon glyphicon-question-sign"></i></span> Estimated order total </span> <span class="right-sec-product"> $ <?=  Yii::$app->cart->CartTotal();?></span>
                      </div>
                    </div>
                  </div>
                  </div>
                <div class="cart-check-btn sec-check-btn">
                        <?= Html::a('Check out now', ['/cart/customer'],['class' => 'checkout-btn']) ?>
                </div>            
            </div>
          </div>
        </div>
   <?php  } else{?>
        <div class="row">
                <div class="col-md-12 cart-is-empty">
                      <i class="fa fa-shopping-cart"></i>
                      <h2 class="text-center text-mute"> Your Cart Is Empty</h2> 
                                      
                      <?= Html::a('Continue Shopping', ['/site/index'],['class' => 'checkout-btn m-t-20']) ?> 
                 </div>
         </div>
   <?php } ?>
</div>