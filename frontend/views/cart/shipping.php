<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
use \common\models\Customer;
use \common\models\ShippingService;
$Customer = Yii::$app->cart->CustomerInCart();
?>

  <section>
    <div class="container">
      <div class="shoping-cart-main">
        <div class="tital-btn clearfix">
          <div class="title-cart">
            <h1>Shipping Details</h1>
          </div>
          <div class="cart-check-btn add-summary">
            <h3>Order Summary</h3>
          </div>
        </div>
        <div class="cart-product-div clearfix">
          <div class="col-md-12">
            <div class="row">
              <div class="bg-product-wht form-BG">
                <div class="row">
                  <div class="col-md-9">
                    <div class="Shiping-form">
                      <div class="row">
                        <div class="col-md-8">
                          <?php
                            $ShippingService = ShippingService :: find()->where(['status'=>1])->all();
                             if(!empty($ShippingService)){
                               
                          ?>
                          
                           <h2>Available shipping methods</h2>

                            <?php foreach($ShippingService as $ele){ 
                                  $res      = Yii::$app->cart->shippingCal($ele['id']); 
                                  $checked  = $Customer['shipping_id']==$ele['id']?'checked':"";                            
                              ?>
                                <label class="check-cate-box clearfix" for="<?php echo $ele['id'];?>">
                                  <div class="prof-checker radioDiv">
                                    <input class="radio-input" <?php echo $checked;?>  type="radio" name="shipping_method" value="<?php echo $ele['id'];?>"  id="<?php echo $ele['id'];?>" >
                                  </div>
                                  <div class="data-radiosec clearfix">
                                    <div class="img-radio">
                                        <?=Html::img(Yii::$app->request->baseUrl.'/'.$ele['image']);?>
                                    </div>
                                    <div class="data-text">
                                          <span class="radio-Title"><?php echo $ele['name'];?> - <small><?php echo $ele['price_text'];?></small></span>
                                          <span class="radio-subtitle">expirated delivery : <?php echo $res['day'];?></span>                                           
                                    </div>
                                  </div>
                                </label>
                            <?php } ?>    

                            <div class="btn-confurm m-t-20" id="next-btn">                                
                                            <?php echo $checked=="checked"?  Html::a('NEXT',['/cart/payment'],['class' => 'checkout-btn btn-continue']):"";   ?>
                            </div>


                          <?php } ?>
                        </div>
                        <div class="col-md-4">
                          <h2>Select Shipping Address</h2>
                          <div class="add-box-shiping">
                          	<?php 
								if(!empty($Customer['status'])){
									$Customer = Customer::find()->where(['id'=>$Customer['customer_id']])->asArray()->one();
									echo '<p class="name-add"><span>'.Html::encode($Customer['fullname']).'</span> '.Html::encode($Customer['adress']).','.Html::encode($Customer['city']).','.Html::encode($Customer['state']).','.Html::encode($Customer['zipcode']).'</p>';
								}else{
									echo '<p class="name-add">'.$Customer['message'].'</p>';
								}
							?>
                             <div class="btn-confurm btn-delt">         
                               <?= Html::a('change Address',['/cart/customer'],['class' => 'checkout-btn btn-continue']) ?>                                                                                     
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-3" id="_cart_sidebar_html">
                       <?= $this->render( '_cart_sidebar'); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <?php
$this->registerJs(
"
  $(document).on('change','input[type=radio][name=shipping_method]',function() {
   
     call({ url: '/cart/add-shipping', params: { 'shipping_method':$(this).val()}, type: 'POST' }, function(resp) {    
        if(resp.status == true){
          $('#_cart_sidebar_html').html(resp._cart_sidebar_html);
          $('#next-btn').html(resp.next_button);
        }else{        
        }
     });
  });
"


)
  ?>
