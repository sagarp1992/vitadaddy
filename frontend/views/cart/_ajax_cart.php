<?php

use yii\helpers\Html;
?>
<div class="click-productVBox">
  <div class="close-cart"></div>
  <div class="productVBox-BG">
    <div class="VBox-BG-title">
      <h3>Added to cart</h3>
    </div>
    <div class="VBox-BG-prodct">
      <div class="row">
        <div class="col-md-8">
          <div class="VBox-BG-product">
            <?= Html::a(Html::img($Product->image), array('/product/view','slug'=>$Product->slug)); ?>
           </div>
          <div class="BG-product-text"> <span class="name-product-text"><?=$Product->name;?></span> <span class="name-product-qty">Quantity:<?= Yii::$app->cart->productQty($Product->sku);?></span> </div>
        </div>
        <div class="col-md-4">
          <div class="BG-product-price"><span class="product-price">Rs <?=$Product->price;?></span></div>
        </div>
        <div class="total-BG-product clearfix"> <span class="pull-left"><?= Yii::$app->cart->itemCount();?> item in cart</span> 
            <span class="pull-right">Subtotal Rs <?= Yii::$app->cart->CartTotal();?></span> 
        </div>
        <div class="btn-BG-product clearfix"> 
          <?= Html::a('View cart',['/cart/view-cart'],['class'=>'product-view-btn']);?>
          <?= Html::a('Check out',['/customer/create'],['class'=>'product-checkout-btn']);?>
         </div>
      </div>
    </div>
  </div>
</div>
