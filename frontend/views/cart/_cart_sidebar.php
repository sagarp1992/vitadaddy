<?php
$CustomerInCart = Yii::$app->cart->CustomerInCart();
?>
<div class="gray-bg-incras">
                      <div class="ul-div">
                        <ul class="order-ul clearfix">
                          <li>
                            <span class="pull-left">Subtotal</span> <span class="pull-right">Rs <?= Yii::$app->cart->CartTotal();?></span>
                          </li>
                          <li>
                            <span class="pull-left">Shipping</span> <span class="pull-right">Rs <?= number_format(Yii::$app->cart->shippingCal($CustomerInCart['shipping_id'],$WantTotatal=1));?></span>
                          </li>
                          <li>
                            <span class="pull-left">Tax</span> <span class="pull-right">$ 0.00</span>
                          </li>
                          <li class="reward-li">
                            <span class="pull-left">Reward</span> <span class="pull-right">($ 0.00)</span>
                          </li>
                        </ul>
                        <ul class="order-ul br-none clearfix">
                          <li>
                            <span class="pull-left">Order Total</span> <span class="pull-right">Rs <?= number_format(Yii::$app->cart->CartTotal() + Yii::$app->cart->shippingCal($CustomerInCart['shipping_id'],$WantTotatal=1));?></span>
                          </li>
                        </ul>
                        <p class="will-order-text">You Will be able to review and modify your order on the review page</p>
        </div>
</div>