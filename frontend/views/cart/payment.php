<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model common\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

  <section>
    <div class="container">
      <div class="shoping-cart-main">
        <div class="tital-btn clearfix">
          <div class="title-cart">
            <h1>Shipping Address</h1>
          </div>
          <div class="cart-check-btn add-summary">
            <h3>Order Summary</h3>
          </div>
        </div>
        <div class="cart-product-div clearfix">
          <div class="col-md-12">
            <div class="row">
              <div class="bg-product-wht form-BG">
                <div class="row">
                  <div class="col-md-9">
                    <div class="tab-all-data clearfix">
                      <ul class="nav nav-pills nav-stacked col-md-4">
                        <li class="active">
                          <a href="#credit_card" data-toggle="pill">Credit card <span><i class="glyphicon glyphicon-chevron-right"></i></span></a>
                        </li>
                        <li>
                          <a href="#debit_card" data-toggle="pill">debit card <span><i class="glyphicon glyphicon-chevron-right"></i></span></a>
                        </li>
                        <li>
                          <a href="#internet_banking" data-toggle="pill">internet banking <span><i class="glyphicon glyphicon-chevron-right"></i></span></a>
                        </li>
                        <li>
                          <a href="#cash_on_delivery" data-toggle="pill">cash on delivery <span><i class="glyphicon glyphicon-chevron-right"></i></span></a>
                        </li>
                      </ul>
                      <div class="tab-content tab-data-right col-md-8">
                        <div class="tab-pane active" id="credit_card">
                          <div class="clearfix">
                              <h4>We accept</h4>
                              <div class="card-divImg">
                                  <?=Html::img(Yii::$app->request->baseUrl.'/img/credit-card-icons-png.png');?>
                              </div>
                             <?php                           
                                $form = ActiveForm::begin([
                                  'enableAjaxValidation' => true,
                                'action'=>['/cart/payment','payment_by'=>'Credit Card'],
                                'options' => [
                                  'class' => 'card-form'
                                ]]); ?> 
                                  <div class="card-box-div clearfix">                              
                                      <div class="group-div">
                                        <?= $form->field($model, 'card_number')->textInput(['class' => 'input-form-shipping','placeholder'=>'Card Number'])->label(false); ?>
                                        <span class="icon-card"> <?=Html::img(Yii::$app->request->baseUrl.'/img/Credit-Card-2-icon.png');?></span>
                                      </div>
                                      <div class="date-div clearfix">
                                        <div class="dateInput">
                                          <div class="group-div clearfix selecrtR">
                                              <?= $form->field($model, 'card_month')->dropDownList($model->month_list,['class' => 'input-form-shipping','prompt'=>'MM'])->label(false); ?>
                                        </div>
                                          <p class="slas-line">/</p>
                                          <div class="group-div clearfix selecrtR">
                                              <?= $form->field($model, 'card_year')->dropDownList($model->year_list(),['class' => 'input-form-shipping','prompt'=>'YYYY'])->label(false); ?>
                                          </div>
                                          <p class="slas-ex">Expiry date</p>
                                        </div>
                                        <div class="cvv-Div">
                                          <div class="group-div clearfix selecrtR">
                                            <?= $form->field($model, 'card_cvv_no')->textInput(['class' => 'input-form-shipping','placeholder'=>'CVV'])->label(false); ?>
                                            <div class="card-divImg">
                                            <?=Html::img(Yii::$app->request->baseUrl.'/img/credit-card-cvv.png');?>
                                              
                                            </div>
                                          </div>
                                        </div>
                                      </div>                             
                                      <div class="group-div">
                                          <?= $form->field($model, 'card_holder_name')->textInput(['class' => 'input-form-shipping','placeholder'=>'Name on Card'])->label(false); ?>
                                      </div>               
                                  </div>
                                  <div class="btn-form card-btn clearfix">
                                    <div class="btn-confurm">
                                      <?= $form->field($model, 'type')->hiddenInput(['value'=>'Credit Card'])->label(false); ?>
                                      <?= Html::submitButton('Proceed to payment', ['class' => 'checkout-btn btn-continue']) ?>
                                    </div>
                                  </div>
                             <?php ActiveForm::end(); ?>
                            <div class="sure-pay">
                              <span class="pay-img"> <?=Html::img(Yii::$app->request->baseUrl.'/img/icon-lock-blue-big.png');?></span> <span class="bold-text-pay">100% secure payment</span> <span class="light-text-pay">We assure safe $ secure transaction</span>
                            </div>
                                </div>
                        </div>
                        <div class="tab-pane" id="debit_card">
                        <div class="clearfix">
                            <h4>We accept</h4>
                            <div class="card-divImg">
                              <?=Html::img(Yii::$app->request->baseUrl.'/img/credit-card-icons-png.png');?>
                            </div>
                            <?php                        
                            $form = ActiveForm::begin([
                            'action'=>['/cart/payment','payment_by'=>'Debit Card'],
                            'options' => [
                              'class' => 'card-form'
                            ]]); ?> 
                              <div class="card-box-div clearfix">
                                
                                  <div class="group-div">
                                    <?= $form->field($model, 'card_number')->textInput(['class' => 'input-form-shipping','placeholder'=>'Card Number'])->label(false); ?>
                                    <span class="icon-card">    <?=Html::img(Yii::$app->request->baseUrl.'/img/Credit-Card-2-icon.png');?></span>
                                  </div>
                                  <div class="date-div clearfix">
                                    <div class="dateInput">
                                      <div class="group-div clearfix selecrtR">
                                          <?= $form->field($model, 'card_month')->dropDownList($model->month_list,['class' => 'input-form-shipping','prompt'=>'MM'])->label(false); ?>
                                    </div>
                                      <p class="slas-line">/</p>
                                      <div class="group-div clearfix selecrtR">
                                          <?= $form->field($model, 'card_year')->dropDownList($model->year_list(),['class' => 'input-form-shipping','prompt'=>'YYYY'])->label(false); ?>
                                      </div>
                                      <p class="slas-ex">Expiry date</p>
                                    </div>
                                    <div class="cvv-Div">
                                      <div class="group-div clearfix selecrtR">
                                        <?= $form->field($model, 'card_cvv_no')->textInput(['class' => 'input-form-shipping','placeholder'=>'CVV'])->label(false); ?>
                                        <div class="card-divImg">
                                        <?=Html::img(Yii::$app->request->baseUrl.'/img/credit-card-cvv.png');?>
                                        </div>
                                      </div>
                                    </div>
                                  </div>                             
                                  <div class="group-div">
                                      <?= $form->field($model, 'card_holder_name')->textInput(['class' => 'input-form-shipping','placeholder'=>'Name on Card'])->label(false); ?>
                                  </div>               
                              </div>
                              <div class="btn-form card-btn clearfix">
                                <div class="btn-confurm">
                                  <?= $form->field($model, 'type')->hiddenInput(['value'=>'Debit Card'])->label(false); ?>
                                  <?= Html::submitButton('Proceed to payment', ['class' => 'checkout-btn btn-continue']) ?>
                                </div>
                              </div>
                            <?php ActiveForm::end(); ?>
                            <div class="sure-pay">
                              <span class="pay-img"> <?=Html::img(Yii::$app->request->baseUrl.'/img/icon-lock-blue-big.png');?></span> <span class="bold-text-pay">100% secure payment</span> <span class="light-text-pay">We assure safe $ secure transaction</span>
                            </div>
                         </div>
                            </div>
                        <div class="tab-pane" id="internet_banking">                        
                                <div class="holder">
                                      <span class="icon-holder"><i class="fa fa-warning fa-3x text-mute"></i></span>
                                      <p class="text-mute">Not available this moment</p>
                                </div>
                        </div>
                        <div class="tab-pane" id="cash_on_delivery">
                                <div class="holder">
                                      <span class="icon-holder"><i class="fa fa-warning fa-3x text-mute"></i></span>
                                      <p class="text-mute">One or more Sale Product from your cart is not eligible for Cash on Delivery</p>
                                </div>
                        </div>
                      </div>
                      <!-- tab content -->
                    </div>
                  </div>
                  <div class="col-md-3">
                      <?= $this->render( '_cart_sidebar'); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
