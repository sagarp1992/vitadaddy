<?php  
use yii\helpers\Html;                                 
$CartList = Yii::$app->cart->CartList();
$CartCount  =   Yii::$app->cart->itemCount();
?>
<a href="javascript:toggleDiv('myContent1');">
    <span class="count"><?=  Yii::$app->cart->itemCount();?></span>
    <i></i>
</a>
<section class="dropCart" id="myContent1" style=" display:none">    
    <p><?= $CartCount>0 ? $CartCount." items in your cart":"There are no item in cart.";?> </p>
    <hr>
    <?php if(!empty($CartList)){?>		
        <div class="clearfix"></div> 
        <div class="cart-maindiv">
                <ul class="cart-ul">  
                    <?php foreach($CartList as $ele){?>    
                        <li>
                            <div class="li-product clearfix">
                                <div class="img-cart-div">
                                        <?=Html::img($ele['image']);?>
                                </div>
                                <div class="data-cart-div">
                                        <span class="product-title"><?= $ele['brand'];?></span>
                                        <span class="product-name"><?= $ele['name'];?></span>
                                    <span class="product-qty">Qty : <?= $ele['qty'];?>  | Price : Rs <?= $ele['price'];?></span>
                                </div>
                            </div>
                        </li>
                    <?php } ?>
                </ul>
        </div> 
        <div class="clearfix"></div>  
        <div class="subtotal">
                <h3>Subtotal Rs <?=  Yii::$app->cart->CartTotal();?></h3>
        </div>
        <div class="btn-checkout">
                <?= Html::a('view cart & check out', array('cart/view-cart'),['class'=>'btn-site check-out']); ?>  
        </div>
    <?php } ?>
</section>								