<?php
/* @var $this yii\web\View */
?>

  <section>
    <div class="container">
      <div class="shoping-cart-main">
        <div class="tital-btn clearfix">
          <div class="title-cart">
            <h1>Shipping Address</h1>
          </div>
          <div class="cart-check-btn add-summary">
            <h3>Order Summary</h3>
          </div>
        </div>
        <div class="cart-product-div clearfix">
          <div class="col-md-12">
            <div class="row">
              <div class="bg-product-wht form-BG">
                <div class="row">
                  <div class="col-md-6">
                    <div class="Shiping-form">
                      <h2>add new shipping Address</h2>
                      <form class="form-shiping">
                        <div class="group-div">
                          <input type="text" name="Email" placeholder="Email" class="input-form-shipping">
                        </div>
                        <div class="group-div">
                          <input type="text" name="Fname" placeholder="Full Name" class="input-form-shipping">
                        </div>
                        <div class="group-div">
                          <textarea name="comment" form="usrform" class="input-form-shipping add-text-area"  rows="5">Address </textarea>
                        </div>
                        <div class="group-div">
                          <input type="text" name="Ncity" placeholder="City" class="input-form-shipping">
                        </div>
                        <div class="group-div clearfix selecrtR">
                          <select class="input-form-shipping">
                            <option value="">State Region</option>
                            <option value="Usa">Usa</option>
                            <option value="caneda">caneda</option>
                          </select>
                        </div>
                        <div class="group-div zip-code clearfix">
                          <input type="text" name="Zcode" placeholder="Zip/ postal code" class="input-form-shipping">
                        </div>
                        <div class="group-div">
                          <input type="number" name="num" placeholder="Phone Number" class="input-form-shipping" >
                        </div>
                        <div class="btn-form">
                          <div class="btn-confurm">
                            <button type="button" class="checkout-btn btn-continue" >Continue</button>
                          </div>
                          <div class="btn-cancel">
                            <button type="button" class="checkout-btn btn-cancel" >Cancel</button>
                          </div>
                        </div>
                      </form>
                    </div>
                  </div>
                  <div class="col-md-offset-3 col-md-3 col-sm-offset-3">
                    <div class="gray-bg-incras">
                      <div class="ul-div">
                        <ul class="order-ul clearfix">
                          <li>
                            <span class="pull-left">Subtotal</span> <span class="pull-right">$ 15.05</span>
                          </li>
                          <li>
                            <span class="pull-left">Shipping</span> <span class="pull-right">$ 0.00</span>
                          </li>
                          <li>
                            <span class="pull-left">Tax</span> <span class="pull-right">$ 0.00</span>
                          </li>
                          <li class="reward-li">
                            <span class="pull-left">Reward</span> <span class="pull-right">($ 0.00)</span>
                          </li>
                        </ul>
                        <ul class="order-ul br-none clearfix">
                          <li>
                            <span class="pull-left">Order Total</span> <span class="pull-right">$ 15.05</span>
                          </li>
                        </ul>
                        <p class="will-order-text">You Will be able to review and modify your order on the review page</p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
