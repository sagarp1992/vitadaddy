  <?php
  /* @var $this yii\web\View */
  use yii\helpers\Html;
  ?>
 <section>
    <div class="container" id="view-cart-section">
          <?= $this->render('_cart-items');?>
    </div>
  </section>
  <?php
  $this->registerJs(
    "
     $(document).on('change','.cart-qty',function () {      
          var minValue = parseInt($(this).attr('min'));
          var maxValue = parseInt($(this).attr('max'));
          var sku      = $(this).attr('data-sku')
          if (!minValue) minValue = 1;
          if (!maxValue) maxValue = 50;
          var valueCurrent = parseInt($(this).val());
          
          var name = $(this).attr('name');
          if (valueCurrent >= minValue) {
              $('.btn-number[data-type=\'minus\'][data-field=\'name\']').removeAttr('disabled')
          } else {
              $(this).val($(this).data('oldValue'));
          }
          if (valueCurrent <= maxValue) {
              $('.btn-number[data-type=\'plus\'][data-field=\'name\']').removeAttr('disabled')
          } else {
              $(this).val($(this).data('oldValue'));
          } 
          if($(this).val()>0){
              call({ url: '/cart/add-to-cart', params: { 'sku': sku,'qty':0,'total_qty':$(this).val()}, type: 'GET' }, function(resp) {      

                  if (resp.status == true) {
                     $('#header-cart').html(resp.header_cart_html);
                     $('#view-cart-section').html(resp.view_cart_holder_html);
                     
                  }

              });
          }else{
            $(this).val(1); 
          }

      });
      $(document).on('keydown','.cart-qty',function (e) {
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 190]) !== -1 ||       
            (e.keyCode == 65 && e.ctrlKey === true) ||
            (e.keyCode >= 35 && e.keyCode <= 39)) {
            return;
        }
        if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode >105)) {
            e.preventDefault();
        }
      });"
  );
  ?>
  