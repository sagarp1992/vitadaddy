<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<section>
    <div class="container">
      <div class="shoping-cart-main">
        <div class="tital-btn clearfix">
          <div class="title-cart">
            <h1>Shipping Address</h1>
          </div>
          <div class="cart-check-btn add-summary">
            <h3>Order Summary</h3>
          </div>
        </div>
        <div class="cart-product-div clearfix">
          <div class="col-md-12">
            <div class="row">
              <div class="bg-product-wht form-BG">
                <div class="row">
                  <div class="col-md-6">
                    <div class="Shiping-form">
                         <h2>add new shipping Address</h2>    
    <?php $form = ActiveForm::begin(['class'=>'form-shiping','enableAjaxValidation' => true]); ?>
    <div class="row">
                         <div class="col-md-12">                            
                            <?php if (Yii::$app->session->hasFlash('customer_failed')): ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <h4><i class="icon fa fa-times"></i>Error!</h4>
                                    <?= Yii::$app->session->getFlash('customer_failed') ?>
                                </div>
                            <?php endif; ?>
                        </div>
     </div>
    <div class="group-div">
        <?= $form->field($model, 'email')->textInput(['class' => 'input-form-shipping','placeholder'=>'Email'])->label(false); ?>
    </div>
    <div class="group-div">
            <?= $form->field($model, 'fullname')->textInput(['class' => 'input-form-shipping','placeholder'=>'Full Name'])->label(false); ?>
    </div>
    <div class="group-div">
         <?= $form->field($model, 'adress')->textArea(['class' => 'input-form-shipping add-text-area','placeholder'=>'Address'])->label(false); ?>
    </div>
    <div class="group-div">
         <?= $form->field($model, 'city')->textInput(['class' => 'input-form-shipping','placeholder'=>'City'])->label(false); ?>
    </div>
    <div class="group-div clearfix selecrtR">
         <?= $form->field($model, 'state')->dropDownList($model->state_list,['class' => 'input-form-shipping','placeholder'=>'State','prompt'=>'Select your state'])->label(false); ?>
    </div>
    <div class="group-div zip-code clearfix">
         <?= $form->field($model, 'zipcode')->textInput(['class' => 'input-form-shipping','placeholder'=>'Zipcode'])->label(false); ?>
    </div>
    <div class="group-div">
           <?= $form->field($model, 'phone')->textInput(['class' => 'input-form-shipping','placeholder'=>'Phone'])->label(false); ?>
    </div>
    <div class="btn-form">
        <div class="btn-confurm">
                <?= Html::submitButton('Continue', ['class' => 'checkout-btn btn-continue']) ?>
        </div>
        <div class="btn-cancel">
                 <button type="reset" class="checkout-btn btn-cancel" >Cancel</button>
        </div>
   </div>

    <?php ActiveForm::end(); ?>
    </div>
                  </div>
                  <div class="col-md-offset-3 col-md-3 col-sm-offset-3">
                       <?= $this->render( '/cart/_cart_sidebar'); ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
