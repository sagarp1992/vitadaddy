<?php

/* @var $this yii\web\View */
use \common\models\Slider;
$this->title = 'Home';
$Slider =   Slider::find()->where(1)->all();
if(!empty($Slider)){
	
?>


    <section class="sliderArea">
        <div class="container">
            <div class="row">
                <div class=" col-lg-12">
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                            <?php 
                            $i=1;
                            foreach($Slider as $ele){?>
                            <div class="item <?= $i==1?'active' : " ";?>">
                                <img src="<?= Yii::$app->request->baseUrl.'/';?>img/<?php echo $ele['background_image'];?>" alt="product-item">
                            </div>
                            <?php $i++;} ?>
                        </div>
                        <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                            <span class="glyphicon glyphicon-chevron-left"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="right carousel-control" href="#myCarousel" data-slide="next">
                            <span class="glyphicon glyphicon-chevron-right"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
    <section class="productsection utility-m-b-30">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 utility-pAdd">
                    <div class="tagsection tagsection-bg-wht">
                        <h3>OUR EASY RETURNS POLICY MEANS NEVER HAVING TO SAY YOU'RE STUCK WITH IT. SHOP WITH CONFIDENCE!</h3>
                    </div>
                    <div class="product-corosol">

                        <section class="autoplay">
                            <div>
                                <a href="#" class="product-itemSlid">
                                <img src="https://www.vitacost.com/Images/Products/500/NOW-Foods/NOW-Foods-Sports-Carbo-Gain-733739020192.jpg" alt="">
                                    <h5> $ 229.99 </h5>
                                    <span>FLAT TWO WEEK CLEANSE</span>
                                </a>

                            </div>
                            <div>
                                <a href="#" class="product-itemSlid">
                                    <img src="/developer/img/2126415_set.jpg">
                                    <h5> $ 229.99 </h5>
                                    <span>FLAT TWO WEEK CLEANSE</span>
                                </a>

                            </div>
                            <div>
                                <a href="#" class="product-itemSlid">
                                <img src="https://www.vitacost.com/Images/Products/500/Iron-Tek/Iron-Tek-Glutamine-Pure-Amino-Acid-Powder-666999131004.jpg" alt="">
                                    <h5> $ 229.99 </h5>
                                    <span>FLAT TWO WEEK CLEANSE</span>
                                </a>

                            </div>
                            <div>
                                <a href="#" class="product-itemSlid">
                                    <img src="/developer/img/2135911_set.jpg">
                                    <h5> $ 229.99 </h5>
                                    <span>FLAT TWO WEEK CLEANSE</span>
                                </a>

                            </div>
                            <div>
                                <a href="#" class="product-itemSlid">
                                    <img src="/developer/img/2126415_set.jpg">
                                    <h5> $ 229.99 </h5>
                                    <span>FLAT TWO WEEK CLEANSE</span>
                                </a>

                            </div>
                            <div>
                                <a href="#" class="product-itemSlid">
                                    <img src="/developer/img/2136083_set.jpg">
                                    <h5> $ 229.99 </h5>
                                    <span>FLAT TWO WEEK CLEANSE</span>
                                </a>

                            </div>
                            <div>
                                <a href="#" class="product-itemSlid">
                                <img src="https://www.vitacost.com/Images/Products/500/ARO-Vitacost/ARO-Vitacost-Black-Series-Glutamine-RAW-844197018628.jpg" alt="">
                                    <h5> $ 229.99 </h5>
                                    <span>FLAT TWO WEEK CLEANSE</span>
                                </a>

                            </div>
                            <div>
                                <a href="#" class="product-itemSlid">
                                    <img src="/developer/img/2126415_set.jpg">
                                    <h5> $ 229.99 </h5>
                                    <span>FLAT TWO WEEK CLEANSE</span>
                                </a>

                            </div>
                            <div>
                                <a href="#" class="product-itemSlid">
                                    <img src="/developer/img/2136083_set.jpg">
                                    <h5> $ 229.99 </h5>
                                    <span>FLAT TWO WEEK CLEANSE</span>
                                </a>

                            </div>
                            <div>
                                <a href="#" class="product-itemSlid">
                                    <img src="/developer/img/2135911_set.jpg">
                                    <h5> $ 229.99 </h5>
                                    <span>FLAT TWO WEEK CLEANSE</span>
                                </a>

                            </div>

                        </section>
                    </div>
                </div>

                <div class="col-lg-12 utility-pAdd">
                    <div class="productslider productslider-wht">
                        <div class="producttitlearea producttitlearea-wht">
                            <h2>SHOP WITH CONFIDENCE!</h2>
                        </div>
                        <div class="product-corosol  shop-with-Pro1">

                            <section class="autoplay">
                                <div>
                                    <a href="#" class="product-itemSlid">
                                    <img src="https://www.vitacost.com/Images/Products/500/NOW-Foods/NOW-Foods-Sports-Carbo-Gain-733739020239.jpg" alt="">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                        <img src="/developer/img/2126415_set.jpg">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                    <img src="https://www.vitacost.com/Images/Products/500/ARO-Vitacost/ARO-Vitacost-Black-Series-Glutamine-Raw-Unflavored-844197018406.jpg" alt="">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                        <img src="/developer/img/2135911_set.jpg">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                        <img src="/developer/img/2126415_set.jpg">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                    <img src="https://www.vitacost.com/Images/Products/500/Vitacost/Vitacost-Synergy-Tonalin-XS-CLA-780-mg-CLA-835003001705.jpg" alt="">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                        <img src="/developer/img/2135911_set.jpg">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                        <img src="/developer/img/2126415_set.jpg">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                    <img src="https://www.vitacost.com/Images/Products/500/All-American-EFX/All-American-EFX-Karbolyn-Neutral-Flavor-737190002032.jpg" alt="">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                        <img src="/developer/img/2135911_set.jpg">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>

                            </section>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 utility-pAdd">
                    <div class="productslider productslider-wht">
                        <div class="producttitlearea producttitlearea-wht">
                            <h2>SHOP WITH CONFIDENCE!</h2>
                        </div>
                        <div class="product-corosol shop-with-Pro2">

                            <section class="autoplay">
                                <div>
                                    <a href="#" class="product-itemSlid">
                                    <img src="https://www.vitacost.com/Images/Products/500/Natures-Baby/Natures-Baby-Organics-Conditioner-and-Detangler-Lavender-Chamomile-183060000071.jpg" alt="">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                        <img src="/developer/img/2126415_set.jpg">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                        <img src="/developer/img/2136083_set.jpg">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                        <img src="/developer/img/2135911_set.jpg">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                    <img src="https://www.vitacost.com/Images/Products/500/Daily-Concepts/Daily-Concepts-Your-Babys-Konjac-Sponge-741021004553.jpg" alt="">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                    <img src="https://www.vitacost.com/Images/Products/500/California-Baby/California-Baby-Calendula-Cream-792692334555.jpg" alt="">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                        <img src="/developer/img/2135911_set.jpg">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                        <img src="/developer/img/2126415_set.jpg">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                    <img src="https://www.vitacost.com/Images/Products/500/California-Baby/California-Baby-Calendula-Cream-792692334555.jpg" alt="">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>
                                <div>
                                    <a href="#" class="product-itemSlid">
                                        <img src="/developer/img/2135911_set.jpg">
                                        <h5> $ 229.99 </h5>
                                        <span>FLAT TWO WEEK CLEANSE</span>
                                    </a>

                                </div>

                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <?php
   $this->registerJs("
				
					$('.autoplay').slick({
					  slidesToShow: 7,
					  slidesToScroll: 1,
					  autoplay: true,
					  autoplaySpeed: 2000,
					  
					  responsive: [
                        {
							breakpoint: 1199,
							settings: {
								slidesToShow: 5,
								slidesToScroll: 1,
							}
						},
						{
							breakpoint: 1024,
							settings: {
								slidesToShow: 4,
								slidesToScroll: 1,
							}
						},
						{
							breakpoint: 767,
							settings: {
								slidesToShow: 3,
								slidesToScroll: 1
							}
						},
						{
							breakpoint: 480,
							settings: {
								slidesToShow: 2,
								slidesToScroll: 1
							}
						}
					  
					]
					  
		});
				
												
	
			"
);
  ?>