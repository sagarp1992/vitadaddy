
<?php 
use yii\bootstrap\Nav;
use \common\models\Category;
$menuItems   =Yii::$app->general->breadCumb($Category['id']);
$MainMenu   =  Nav::widget([
    'options' => ['class' => 'bredcumb-ul','id'=>''],
    'items' => array_reverse($menuItems),
]);
?>
<section class="breadcrumbArea productpagebreadcrumb">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <ul>
                           <?php echo $MainMenu ;?>
                        </ul>

                    </div>
                </div>
            </div>
</section>