<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\Pjax;
use \common\models\Product;
?>

<?= $this->render('_bredcumb',['Product'=>$Product]); 
$this->title = $Product['meta_title'];
Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => $Product['meta_description']
]); 
Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => $Product['meta_keyword']
]); 

$productCombination = array($Product['slug']=>$Product['name'])+ArrayHelper::map($ProductCombination, 'slug', 'name');
$combination    =   array();
foreach($productCombination as $k=>$p){
    $combination[Url::toRoute(['product/view','slug'=>$k])]  =  $p ;
}
?>
<?php Pjax::begin() ?>
 <section>
            <div class="container">
                <div class="product-main-div">
                    <div class="row">
                         <div class="col-md-12">
                            <?php if (Yii::$app->session->hasFlash('cart_success')): ?>
                            <div class="alert alert-success alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                <h4><i class="icon fa fa-check"></i>Added!</h4>
                                <?= Yii::$app->session->getFlash('cart_success') ?>
                            </div>
                            <?php endif; ?>
                            <?php if (Yii::$app->session->hasFlash('cart_error')): ?>
                                <div class="alert alert-danger alert-dismissable">
                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                    <h4><i class="icon fa fa-times"></i>Error!</h4>
                                    <?= Yii::$app->session->getFlash('cart_error') ?>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="product-slider">
                                <img src="<?php echo str_replace('150',500,$Product['image']);?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="product-detail">
                                <div class="prodct-tilte">
                                    <small><?= !empty($Product['brand'])?$Product['brand']:"";?></small>
                                    <h2><?= !empty($Product['name'])?$Product['name']:"";?></h2>
                                </div>
                                <div class="product-price">
                                    <span class="price-product">Rs <?= !empty($Product['price'])?$Product['price']:"";?>
                                        <span>price</span>
                                    </span>
                                    <span class=" text-mute">Retail Price <span class="line-through">
                                        Rs <?= !empty($Product['retail_price'])?$Product['retail_price']:"";?></span>
                                        </span>
                                    
                                </div>
                                <div class="use-code">
                                    <span>use code saveoptia and pay only ₹49.99</span>
                                </div>
                                <div class="product-select">
                                <div class="row">
                                    <?php 
                                     $currentSlug =   Url::toRoute(['product/view','slug'=>$Product['slug']]);
                                    if(!empty($Product['size'])){
                                         
                                          $SizeOfComb= array();
                                          $combinationSize = Product::find()->where(
                                              ['AND',
                                                ['=','combination_id',$Product['combination_id']],
                                                ['=','flavour',$Product['flavour']],
                                             ])->asArray()->all();
                                             if(!empty($combinationSize)){
                                                    foreach($combinationSize as $ele){
                                                            $slug   = Url::toRoute(['product/view','slug'=>$ele['slug']]);
                                                            $SizeOfComb[$slug] = $ele['size'];
                                                    } 
                                             }

                                        ?>
                                        <div class="col-md-6">
                                            <label>Size</label>
                                            <?= Html::dropDownList('list',$currentSlug,$SizeOfComb,
                                            ['class'=>'form-control','id'=>'sel1']); ?>          
                                        </div>
                                    <?php } ?>
                                    <?php if(!empty($Product['flavour'])){ 
                                           $FlavourOfComb= array();
                                           $combinationFlavour = Product::find()->where(
                                            ['AND',
                                              ['=','combination_id',$Product['combination_id']],
                                              ['!=','flavour',''],
                                              ['=','size', $Product['size']],
                                           ])->asArray()->all();
                                            if(!empty($combinationFlavour)){
                                                foreach($combinationFlavour as $ele){
                                                        $slug   = Url::toRoute(['product/view','slug'=>$ele['slug']]);
                                                        $FlavourOfComb[$slug] = $ele['flavour'];
                                                } 
                                            }
                                        ?>
                                        <div class="col-md-6">
                                                <label>Flavor</label>
                                            <?= Html::dropDownList('list',$currentSlug,$FlavourOfComb,
                                            ['class'=>'form-control','id'=>'sel1']) ?>          
                                        </div> 
                                    <?php } ?>                         
                                  </div>    
                                   
                                </div>
                                <div class="prodct-cart-btn" id="<?=$Product['sku'];?>">
                                  <?= Html::beginForm(['/cart/add-to-cart'], 'get',['class'=>'add-to-cart-form','data-sku'=>$Product['sku']]) ?>
                                    <div class="product-cart-incres">
                                        <div class="input-group">
                                            <?= Html::input('text',   'qty','1', ['class' => "form-control input-number","min"=>"1","max"=>"100"]);?>
                                            <?= Html::input('hidden', 'sku', $Product['sku']);?>
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-success btn-number" data-type="plus" data-field="qty">
                                                    <span class="glyphicon glyphicon-plus"></span>
                                                </button>
                                            </span>
                                            <span class="input-group-btn">
                                                <button type="button" class="btn btn-danger btn-number" data-type="minus" data-field="qty">
                                                    <span class="glyphicon glyphicon-minus"></span>
                                                </button>
                                            </span>

                                        </div>
                                    </div>
                                    <div class="prodcut-btnCart">
                                          <?= Html::submitButton('ADD TO CART', ['class' => 'atc-btn-center']) ?>
                                    </div>
                                    <?= Html::endForm() ?>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>



        </section>
        <?php if(!empty($ProductRelated)){?>
        <section class="categoryMainContent">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="vieweditems">
                            <h2>Customers who viewed this product also viewed</h2>
                        </div>
                    </div>
                    <div class="col-lg-12 similaritemsList">
                        <ul>
                            <?php foreach($ProductRelated as $ele){?>
                                <li>
                                    <div class="cat-product-con">
                                        <div class="catImg">
                                              <?= Html::a(Html::img(Yii::$app->request->baseUrl.'/img/'.$ele['image']), array('/product/view','slug'=>$ele['slug'])); ?> 
                                        </div>
                                        <h1>
                                        <?= Html::a($ele['name'], array('#')); ?>                                             
                                        </h1>
                                        <h5>$ <?php echo $ele['price'];?></h5>
                                        <a href="#" class="addtocart">Add to Cart</a>

                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>
        <section class="productDetailsCon">
            <div class="container">
                <div class="row">
                <div class="pro_dec_title"><h3>Product
                                <span>Info</span>
                                <small>(<b>SKU</b>- <?= !empty($Product['sku'])?$Product['sku']:"";?>)</small>
                            </h3></div>
                    <?= !empty($Product['description'])?$Product['description']:"";?>
                </div>
            </div>
        </section>
    <?php
       $this->registerJs(
        "$(document).on('change','#sel1',function() { 
                var v = $(this).val();
                window.location.href = v;
        });"
    );
    ?>
    <?php Pjax::end() ?>