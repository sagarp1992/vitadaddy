<?php 

use yii\bootstrap\Nav;

use \common\models\ProductCategory;

use \common\models\Category;

$Category       = ProductCategory::find()->where(['product_id'=>$Product['id']])->one();

$menuItems[]   = ['label' =>$Product['name'], 'url' =>'#','options'=> []];

$menuItems      = $menuItems+Yii::$app->general->breadCumb($Category['category_id']);



$MainMenu   =  Nav::widget([

    'options' => ['class' => 'bredcumb-ul','id'=>''],

    'items' => array_reverse($menuItems),

]);

?>

