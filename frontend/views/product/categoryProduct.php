<?php

use yii\helpers\Html;

use yii\helpers\ArrayHelper;

use yii\bootstrap\Nav;

use yii\widgets\ListView;

use \common\models\Category;

use \common\models\ProductCategory;

use yii\data\ActiveDataProvider;

use yii\widgets\Pjax;
$this->title = $Category['meta_title'];
Yii::$app->view->registerMetaTag([
    'name' => 'description',
    'content' => $Category['meta_description']
]); 
Yii::$app->view->registerMetaTag([
    'name' => 'keywords',
    'content' => $Category['meta_keyword']
]); 

$menuItems     = Yii::$app->general->getChildCategory($Category['id']);
$CategoryIds   = ArrayHelper::map(Yii::$app->general->recursiveCategory($Category['id']),'id','id');
$CategoryIds   =  array($Category['id'])+$CategoryIds;
$CategoryIds   = !empty($CategoryIds)?implode(",",$CategoryIds) :array();

?>

<?= $this->render('_bredcumb-category',['Category'=>$Category]); ?>

<section class="categoryMainContent">
  	<div class="container">
      <div class="row">
    	<div class="col-lg-12 pagetitle">
            <h2><?php echo $Category['name'];?> <small id="summary-text" class="pull-right"></small></h2>
                <?php if (Yii::$app->session->hasFlash('cart_success')): ?>
                    <div class="alert alert-success alert-dismissable">
                         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                         <h4><i class="icon fa fa-check"></i>Added!</h4>
                          <?= Yii::$app->session->getFlash('cart_success') ?>
                    </div>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('cart_error')): ?>
                    <div class="alert alert-danger alert-dismissable">
                         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                         <h4><i class="icon fa fa-times"></i>Error!</h4>
                          <?= Yii::$app->session->getFlash('cart_error') ?>
                    </div>
                <?php endif; ?>
        </div>
      	<div class="col-lg-3 col-sm-3 col-md-3 leftside">
            <h4 class='filter-title'>Category</h4>
            <?= Nav::widget([
                             'options' => ['class' => 'bredcumb-ul','id'=>''],
                             'items' => $menuItems,
            ]);?>
        </div>
        <div class="col-lg-9 col-sm-9 col-md-9 rightside">
        	<div  id="category-product-list">
            </div>
            <div id="category-product-list-loader" class="col-md-12 text-center">
                <i class="fa fa-spin fa-circle-o-notch fa-2x text-mute"></i>
                <p class="text-mute">Loading more Products...</p>
            </div>
        </div>       
      </div>   
   </div>
</section>  
<?php 
$this->registerJs("
var page = 0; var fl= false;
var loadProducts = function(limit){
		console.log(limit)
		fl= true;
	    $('#category-product-list-loader').show();
        call({ url: '/product/load-products', params: { 'limit':limit,'category_id':'".$Category['id']."'}, type: 'POST' }, function(resp) {      
                    $('#category-product-list-loader').hide();
					$('#category-product-list').append(resp.html); 
				    $('#summary-text').html(resp.summary); 
					if(resp.status == true){	
						fl = false;
					}else{
						fl= true;
					}

        });
}
loadProducts(page);
$(window).scroll(function () {
	if ($(window).scrollTop() > ($(document).height() - $(window).height()-$('footer').height()) && fl==false) {	
		console.log('page:'+page)
		page = page+30;
		loadProducts(page);
	}
});
");

?>