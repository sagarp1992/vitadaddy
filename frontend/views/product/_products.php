<?php

use yii\helpers\Html;

use yii\helpers\HtmlPurifier;

use yii\widgets\Pjax;
if(!empty($Products)){
	foreach($Products as $model){
?>


    <div class="cat-product-con" id="<?=$model->product->sku;?>">

        <div class="catImg">

            <?= Html::a(Html::img(str_replace("500",150,$model->product->image)), array('/product/view','slug'=>$model->product->slug),['target'=>'_blank']); ?>

        </div>

        <h1>
            <?= Html::a(Html::encode($model->product->name),array('/product/view','slug'=>$model->product->slug),['target'=>'_blank']); ?>
        </h1>

        <h5 class="c-price">Rs
            <?= Html::encode($model->product->retail_price);?>
        </h5>

        <h5 class="sale-price">Sale Rs
            <?= Html::encode($model->product->price);?>
        </h5>

        <?= Html::a('Add to Cart', array('/cart/add-to-cart','sku'=>$model->product->sku,'_pjax'=>'#p0'),['class'=>"addtocart",'data-sku'=>$model->product->sku]);?>
            <div class="spin-Div hide">
                <i class="fa fa-spinner fa-spin" style="font-size:24px"></i>
            </div>
    </div>
<?php } } ?>