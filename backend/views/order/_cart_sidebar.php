<?php
use yii\helpers\Html;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
?>
<div class="block pull-r-l box-border">
                                <div class="block-header bg-body-light">
                                    <h3 class="block-title text-primary-dark font-w600">
                                    Cart
                                    </h3>
                                </div>
                                <div class="block-content">
                                    <ul class="list list-activity">
                                        <?php $CartList = Yii::$app->cart->cartList();?>
                                        <?php if(!empty($CartList)){?>
                                            <?php foreach($CartList as $Item){?>
                                                <li>                                                
                                                    <?= Html::a('<i class="si si-close text-danger"></i>',['order/remove-cart','sku'=>$Item['sku']]);?>
                                                    <div class="font-w600"><?= Html::encode($Item['name']);?>
                                                    <div class="font-size-xs text-muted">SKU : <?= Html::encode($Item['sku']);?></div>
                                                    <div class="row">
                                                        <div class="col-md-5">                                                          
                                                            <a href="javascript:void(0)">Price  : INR <?= Html::encode($Item['price']);?></a>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="col-md-4"><label>Qty</label></div> 
                                                            <div class="col-md-8"><input type="number" data-sku="<?= Html::encode($Item['sku']);?>" class="form-control cart-qty" value="<?= Html::encode($Item['qty']);?>"></div>
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </li>
                                            <?php } ?>
                                        <?php }else{ ?>
                                            <li>
                                                <div class="font-w600 text-muted text-center m-20">Your cart is empty</div>    
                                            </li>
                                        <?php } ?>
                                    </ul>
                                    <?php if(!empty($CartList)){?>
                                    <div class="block pull-r-l">
                                        <div class="block-content block-content-full block-content-sm bg-body-light">
                                            <div class="row">
                                                <div class="col-6 text-center">
                                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Total Qty</div>
                                                    <div class="font-size-h4"><?php echo Yii::$app->cart->itemCount();?></div>
                                                </div>
                                                <div class="col-6 text-center">
                                                    <div class="font-size-sm font-w600 text-uppercase text-muted">Total</div>
                                                    <div class="font-size-h4">$ <?php echo Yii::$app->cart->cartTotal();?></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <?php if(Yii::$app->controller->action->id=="create"||
                                        Yii::$app->controller->action->id=="add-to-cart"){?>
                                            <div class="col-6">
                                                <?= Html::a('Next','address',array('class'=>'btn btn-block btn-alt-primary'));?>                                      
                                            </div>
                                            <?php } ?>
                                            <div class="col-6 ">
                                            <?=Html::a('Cancel Cart',array('order/cancel-cart'),['class'=>'btn btn-block btn-alt-danger']);?>
                                            </div>
                                        
                                    </div>
                                    <?php } ?>
                                </div>
                                
    </div>  
     <?php
  $this->registerJs(
    "
     $(document).on('change','.cart-qty',function(){
        var sku = $(this).attr('data-sku');
        if($(this).val()>0){
            call({ url: '/order/add-to-cart', params: { 'sku': sku,'qty':0,'total_qty':$(this).val(),'isJson':true}, type: 'GET' }, function(resp) {      

                if (resp.status == true) {
                    $('.cart-section').html(resp.html);
                }

            });
        }else{
             $(this).val(1); 
        }
     });"
  );
  ?>