<?php

use yii\helpers\Html;

use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\bootstrap\Nav;
use yii\widgets\ListView;
use \common\models\Category;
use app\models\AdminProduct;
use \common\models\ProductCategory;
use yii\data\ActiveDataProvider;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin() ?>
<h2 class="content-heading">Add Product in Cart </h2>
<div class="col-md-12">
<?php if (Yii::$app->session->hasFlash('cart_success')): ?>
                    <div class="alert alert-success alert-dismissable">
                         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                         <h4><i class="icon fa fa-check"></i>Added! <?= Yii::$app->session->getFlash('cart_success') ?></h4>
                          
                    </div>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('cart_error')): ?>
                    <div class="alert alert-danger alert-dismissable">
                         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                         <h4><i class="icon fa fa-times"></i>Error! <?= Yii::$app->session->getFlash('cart_error') ?></h4>
                          
                    </div>
 <?php endif; ?>
 <?php if (Yii::$app->session->hasFlash('info')): ?>
                    <div class="alert alert-info alert-dismissable">
                         <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                         <h4><i class="icon fa fa-smail"></i>:) <?= Yii::$app->session->getFlash('info') ?></h4>                          
                    </div>
                <?php endif; ?>
 
</div>
<div class="col-md-12">
        <?php $form = ActiveForm::begin([
                'method' => 'get',
                'options' => ['class' => 'col-xs-8'],
                'fieldConfig' => [
                    'template' => '<div class="form-group"><div class="input-group">{input} <div class="input-group-btn">
                    <button type="submit" class="btn btn-secondary">
                        <i class="fa fa-search"></i>
                    </button>
                </div>
            </div>
        </div>',
                ],
            ]); ?>            

        <?= $form->field($searchModel, 'name')->textInput(['placeholder' => 'Search by Product Name or SKU']) ?>
            <?php ActiveForm::end(); ?>
</div>
<?php
    echo ListView::widget([
        'dataProvider' => $dataProvider,
        'options' => [
            'tag' => 'div',
            'class' => 'col-lg-9  col-sm-9 col-md-9',
            'id' => 'product-list',
        ],
        'pager' => [
            'class'             => 'yii\widgets\LinkPager',
            'options' =>[ 
            'class'  => 'pagination m0'
            ],
            'linkOptions'=> [
                'class'=>'',
            ]
        ],
        'layout' => 
                    "<div class='row'>
                            <div class='col-lg-12 col-sm-12 col-md-12 clearfix'>
                                {items}
                            </div> 
                            <div class='col-lg-12 col-sm-12 col-md-12 clearfix'>
                               
                                    <div class='col-sm-12 col-md-5'>
                                            <div class='pull-left'>
                                                {summary}
                                            </div>
                                    </div>
                                    <div class='col-sm-12 col-md-7'>
                                        <div class='pull-right'>
                                                {pager}
                                        </div>
                                    </div>
                            </div>
                    </div> ",
        'itemView' => '_products_items',
    ]);
?>         
<?php Pjax::end();?>
<div class="col-md-3 cart-section">
    <?=$this->render('_cart_sidebar');?>                           
 </div>