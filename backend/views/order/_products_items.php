<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\widgets\Pjax;
?>
<div class="col-md-3 col-xl-3">
                        <div class="block text-center box-border" title="<?= $model->name; ?>">
                            <div class="block-conten0t product-img-box">
                                    <?= Html::img(str_replace("500","100",$model->image),['style'=>'max-width: 100%;max-height: 100%;']); ?> 
                            </div>
                            <div class="block-content block-content-full">
                                <div class=" font-w600 mb-0 truncate" > <?= $model->name; ?> </div>
                                <div class=" font-w600">INR <?= $model->price; ?></div>
                                <div class=" text-muted">SKU <?= $model->sku; ?></div>
                            </div>
                            <div class="block-content block-content-full bg-body-light">
                             <?= Html::a('Add to Cart', array('/order/add-to-cart','sku'=>$model->sku),['class'=>"btn btn-primary"]); ?> 
                            </div>
                        </div>
</div>