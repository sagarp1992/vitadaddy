<?php

use yii\helpers\Html;
?>
<div class="block">
                        
                        <div class="block-content">
                            <!-- Invoice Info -->
                            <div class="row my-20">
                                <!-- Company Info -->
                                <div class="col-6">
                                    <p class="h3">Order #<?= Html::encode($model->order_no);?></p>
                                    <address>
                                    <?= Html::encode(date('F d,Y',$model->order_date));?><br>
                                    Payment method : check payment
                                    </address>
                                </div>
                                <!-- END Company Info -->

                                <!-- Client Info -->
                                <div class="col-6 text-right">
                                    <p class="h3">Client</p>
                                    <address>
                                        <?= Html::encode($Customer->fullname);?><br>
                                        <?= Html::encode($Customer->adress);?><br>
                                        <?= Html::encode($Customer->city);?>,<?= Html::encode($Customer->state);?><br>                                       
                                        Phone : <?= Html::encode($Customer->phone);?>
                                        Email :<?= Html::encode($Customer->email);?>
                                    </address>
                                </div>
                                <!-- END Client Info -->
                            </div>
                            <!-- END Invoice Info -->

                            <!-- Table -->
                            <div class="table-responsive push">
                            <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 60px;"></th>
                                            <th>Product</th>
                                            <th class="text-center" style="width: 90px;">Qnt</th>
                                            <th class="text-right" style="width: 120px;">Unit</th>
                                            <th class="text-right" style="width: 120px;">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                            
                                            if(!empty($OrderDetaills)){
                                            foreach($OrderDetaills as $ele){
                                                ?>
                                            <tr>
                                                <td class="text-center"><?= Html::img(str_replace("500","100",Html::encode($ele['image'])),['style'=>'max-width: 100%;max-height: 100%;']); ?> </td>
                                                <td><p class="font-w600 mb-5"><?= Html::encode($ele['name']);?></p><div class="text-muted">SKU : <?= Html::encode($ele['sku']);?></div></td>                                                
                                                <td class="text-center"><?=$ele['qty'];?></td>
                                                <td class="text-right">$ <?=$ele['price'];?></td>
                                                <td class="text-right">$ <?=$ele['qty'] * $ele['price'];?></td>
                                            </tr>
                                            <?php
                                            }
                                        }
                                            ?>
                                        <tr>
                                            <td colspan="4" class="font-w600 text-right">Subtotal</td>
                                            <td class="text-right">INR <?= Html::encode($model->order_total);?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="font-w600 text-right"> Rate</td>
                                            <td class="text-right">0.0%</td>
                                        </tr>
                                        <tr class="table-warning">
                                            <td colspan="4" class="font-w700 text-uppercase text-right">Total</td>
                                            <td class="font-w700 text-right">INR <?= Html::encode($model->order_total);?></td>
                                        </tr>
                                    </tbody>
</table>
                            </div>
                        </div>
                    </div>
