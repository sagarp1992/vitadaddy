
  <?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

    <h2 class="content-heading d-print-none">
                       <?= Html::a('Go back',['order/create'],['class'=>'btn btn-sm btn-rounded btn-success float-right']);?>
                                  Shipping / Billing
    </h2>
    <div class="block">
        <div class="col-md-9">
            <?php $form = ActiveForm::begin([

                'fieldConfig' => [
                    'template' => '<div class="form-group row"> 
                                        <label class="col-lg-3 col-form-label" for="example-hf-email">{label}</label>
                                        <div class="col-lg-7">
                                            {input}{hint}{error}
                                        </div>
                                </div>',
                
                ],
            ]); ?>
            <div class="row">
                <div class="col-md-12">                            
                    <?php if (Yii::$app->session->hasFlash('customer_failed')): ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <h4><i class="icon fa fa-times"></i>Error!</h4>
                            <?= Yii::$app->session->getFlash('customer_failed') ?>
                        </div>
                    <?php endif; ?>
                </div>
            </div>  
            <?= $form->field($model, 'email')->textInput(['class' => 'form-control','placeholder'=>'Email']); ?>
            <?= $form->field($model, 'fullname')->textInput(['class' => 'form-control','placeholder'=>'Full Name'])?>
            <?= $form->field($model, 'adress')->textArea(['class' => 'form-control add-text-area','placeholder'=>'Address'])->label('Address'); ?>
            <?= $form->field($model, 'city')->textInput(['class' => 'form-control','placeholder'=>'City'])->label(false); ?> 
            <?= $form->field($model, 'state')->dropDownList($model->state_list,['class' => 'form-control','placeholder'=>'State','prompt'=>'Select your state']) ?>
            <?= $form->field($model, 'zipcode')->textInput(['class' => 'form-control','placeholder'=>'Zipcode'])?>
            <?= $form->field($model, 'phone')->textInput(['class' => 'form-control','placeholder'=>'Phone']) ?>
            <div class="btn-form">
                <div class="btn-confurm">
                        <?= Html::submitButton('Continue', ['class' => 'btn btn-success']) ?>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-3 cart-section">
            <?=$this->render('_cart_sidebar');?>                           
        </div>
    </div>