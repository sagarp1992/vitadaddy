<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
?>
<h2 class="content-heading d-print-none">
<?= Html::a('Go back',['order/address'],['class'=>'btn btn-sm btn-rounded btn-success float-right']);?>
                        Shipping Method
</h2>
<div class="block">
        <div class="col-md-9">
      
        <table class="table table-striped table-borderless table-hover table-vcenter">
                                    <thead class="thead-default">
                                        <tr>
                                            <th style="width: 50%;">Shipping Mthod</th>
                                            <th class="d-none d-lg-table-cell text-center" style="width: 15%;">Fees</th>
                                            <th class="d-none d-lg-table-cell text-center" style="width: 15%;"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <h4 class="h5 mt-15 mb-5">
                                                    <a href="javascript:void(0)">By FedEx</a>
                                                </h4>                                                
                                            </td>
                                            <td class="font-size-xl text-center font-w600">$ 21,987</td>
                                            <td class="d-none d-lg-table-cell text-center">
                                             <?= Html::a('Select', array('/order/shipping','shipping_method'=>1),['class'=>"btn btn-primary"]); ?> 
                                            </td>
                                        </tr>   
                                        <tr>
                                            <td>
                                                <h4 class="h5 mt-15 mb-5">
                                                    <a href="javascript:void(0)">By USPS</a>
                                                </h4>                                                
                                            </td>
                                            <td class="font-size-xl text-center font-w600">$ 21,987</td>
                                            <td class="d-none d-lg-table-cell text-center">
                                             <?= Html::a('Select', array('/order/shipping','shipping_method'=>2),['class'=>"btn btn-primary"]); ?> 
                                            </td>
                                        </tr>   
                                    </tbody>
                                </table>
      </div>
      <div class="col-md-3 cart-section">
            <?=$this->render('_cart_sidebar');?>                           
      </div>
</div>
