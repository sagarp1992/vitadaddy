<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use kartik\daterange\DateRangePicker;
use \common\models\Customer;
/* @var $this yii\web\View */
/* @var $searchModel common\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Orders');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin(); ?>
<h2 class="content-heading d-print-none">
                        <?= Html::a(Yii::t('app', 'Create Order'), ['create'], ['class' => 'btn btn-sm  btn-success float-right']) ?>
                        <?= Html::encode($this->title) ?>
</h2>
<?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'order_no', 
            [
                'attribute' => 'status',
                'format' => 'raw',   
                'filter'=>array('New Order'=>'New Order','Pending'=>'Pending','Cancelled'=>'Cancelled','Completed'=>'Completed')   ,              
                'value' => function ($data) {
                   if($data->status == "New Order"){
                        return '<label class="label label-info">New Order</label>';
                   }else if($data->status == "Pending"){
                        return '<label class="label label-warning">Pending</label>';
                   }else if($data->status == "Cancelled"){
                        return '<label class="label label-danger">Cancelled</label>'; 
                   }else if($data->status == "Completed"){
                        return '<label class="label label-success">Completed</label>';
                   }
                },
            ], 
            [
                'attribute' => 'order_total',
                'format' => 'html',                 
                'value' => function ($data) {
                    return '$ '.$data->order_total;
                },
            ],
            [
                'attribute' => 'custome_id',
                'attribute' => 'Billing Name',
                'filter' => false,                 
                'value' => function ($data) {
                    $Customer   =   Customer::find()->where(['id'=>$data->custome_id])->one();
                    if(!empty($Customer)){
                        return $Customer->fullname;
                    }
                },
            ],
            [
                'attribute' => 'order_date', 
                'format' => 'datetime',
                'filter' => DateRangePicker::widget([
                    'model' => $searchModel,
                    'attribute' => 'order_date',
                    'convertFormat' => true,
                    'pluginOptions' => [
                        'timePicker'=>true,
                        'timePickerIncrement'=>30,
                        'locale'=>[
                            'format'=>'Y-m-d h:i A'
                        ]
                    ],
                ])
            ],
            ['class' => 'yii\grid\ActionColumn','template'=>'{view} {delete}'],
        ],
]); ?>
<?php Pjax::end(); ?>