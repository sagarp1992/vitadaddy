<?php
/* @var $this yii\web\View */
use yii\helpers\Html;
?>
<h2 class="content-heading">Cart Details <?= Html::a('Checkout','address',array('class'=>'btn btn-success pull-right'));?></h2>

<table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th class="text-center" style="width: 60px;"></th>
                                            <th>Product</th>
                                            <th class="text-center" style="width: 90px;">Qnt</th>
                                            <th class="text-right" style="width: 120px;">Unit</th>
                                            <th class="text-right" style="width: 120px;">Amount</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                            $CartList = Yii::$app->cart->CartList();
                                            if(!empty($CartList)){
                                            foreach($CartList as $ele){
                                                ?>
                                            <tr>
                                                <td class="text-center"><?= Html::img(str_replace("500","100",$ele['image']),['style'=>'max-width: 100%;max-height: 100%;']); ?> </td>
                                                <td><p class="font-w600 mb-5"><?=$ele['name'];?></p><div class="text-muted">SKU : <?=$ele['sku'];?></div></td>                                                
                                                <td class="text-center"><input class="form-control" type ="number" value ="<?=$ele['qty'];?>" min></td>
                                                <td class="text-right">$ <?=$ele['price'];?></td>
                                                <td class="text-right">$ <?=$ele['qty'] * $ele['price'];?></td>
                                            </tr>
                                            <?php
                                            }
                                        }
                                            ?>
                                        <tr>
                                            <td colspan="4" class="font-w600 text-right">Subtotal</td>
                                            <td class="text-right">INR <?=Yii::$app->cart->CartTotal();?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" class="font-w600 text-right"> Rate</td>
                                            <td class="text-right">0.0%</td>
                                        </tr>
                                        <tr class="table-warning">
                                            <td colspan="4" class="font-w700 text-uppercase text-right">Total</td>
                                            <td class="font-w700 text-right"> INR <?=Yii::$app->cart->CartTotal();?></td>
                                        </tr>
                                    </tbody>
</table>

