<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use \common\models\Customer;

/* @var $this yii\web\View */
/* @var $model common\models\Order */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Orders'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$sql = "SELECT product.*,order_detaills.*  FROM order_detaills LEFT JOIN product on order_detaills.product_id = product.id where order_detaills.order_id = ".$model->id;
$OrderItems =  Yii::$app->db->createCommand($sql)->queryAll();

$Customer   =   Customer::find()->where(['id'=>$model->custome_id])->asArray()->one();
?>
<div class="content">
                   
                    <h2 class="content-heading">Order Details</h2>
                    <div class="row row-deck gutters-tiny">
                        <div class="col-md-6">
                            <div class="block block-rounded">
                               
                                <div class="block-content">
                                    <div class="font-size-lg text-black mb-5">Order No  - <?php echo $model->order_no?></div>
                                    <?php echo date('F d Y H:i:A',$model->order_date);?><br>
                                    <?= Html::beginForm(['order/update', 'id' => $model->id], 'post') ?>
                                        <div class="row mt-30">    
                                            <div class="col-md-6 ">
                                                    <label>Order Status</label>
                                                    <?= Html::dropDownList('Order[status]', $model->status,Yii::$app->cart->orderStatus(),['class'=>'form-control']); ?>  
                                            </div>
                                            <div class="col-md-6">     
                                                     <label>Order Notes</label> 
                                                    <?= Html::textarea('Order[comment]', $model->comment, ['class' => 'form-control']);?>
                                            </div>
                                            <div class="col-md-6">     
                                                    <?= Html::submitButton('Update', ['class' => 'btn btn-sm btn-success']) ?>
                                            </div>
                                        </div>
                                    <?= Html::endForm() ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="block block-rounded">
                                <div class="block-header block-header-default">
                                    <h3 class="block-title">Billing Address</h3>
                                </div>
                                <div class="block-content">
                                    <div class="font-size-lg text-black mb-5"><?php echo $Customer['fullname'];?></div>
                                    <address>
                                       <?php echo $Customer['adress'];?><br>
                                       <?php echo $Customer['state'];?>
                                       <?php echo $Customer['city'];?>-<?php echo $Customer['zipcode'];?><br>
                                       <br><br>
                                        <i class="fa fa-phone mr-5"></i> <?php echo $Customer['phone'];?><br>
                                        <i class="fa fa-envelope-o mr-5"></i> <a href="javascript:void(0)"><?php echo $Customer['email'];?></a>
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row row-deck gutters-tiny">
                        <div class="col-md-6">
                            <div class="block block-rounded">                               
                                <div class="block-content">
                                    <div class="font-size-lg text-black mb-5">Shipping Method</div>
                                    <b>FedEx</b><br>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        <div class="block block-rounded">                               
                            <div class="block-content">
                                <div class="font-size-lg text-black mb-5">Payment Method</div>
                                 <b>Cash on Delevery</b><br>
                            </div>
                        </div>
                    </div>
                    </div>
                    <!-- END Addresses -->

                    <!-- Products -->
                    <h2 class="content-heading">Ordered Items</h2>
                    <div class="block block-rounded">
                        <div class="block-content">
                            <div class="table-responsive">
                                <table class="table table-borderless table-striped">
                                    <thead>
                                        <tr>
                                            <th style="width: 100px;">SKU</th>
                                            <th>Product</th>
                                            <th class="text-center">QTY</th>
                                            <th class="text-center" style="width: 10%;">UNIT</th>
                                            <th class="text-center" style="width: 10%;">PRICE</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($OrderItems as $Items){ ?>
                                        <tr>
                                            <td>
                                                <?= Html::a($Items['sku'],['product/view','id'=>$Items['product_id'],'class'=>'font-w600']);?>
                                            </td>
                                            <td>
                                               <?= Html::a($Items['name'],['product/view','id'=>$Items['product_id']]);?>
                                            </td>
                                            <td class="text-center font-w600"><?= $Items['qty'];?></td>
                                            <td class="text-center">INR <?= $Items['price'];?></td>                                           
                                            <td class="text-center">INR <?= $Items['price'] * $Items['qty'];?></td>
                                        </tr>
                                        <?php } ?>
                                        <tr class="table-success">
                                            <td colspan="4" class="text-right font-w600 text-uppercase">Total :</td>
                                            <td class="text-center font-w600">$ <?php echo $model->order_total;?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- END Products -->
                </div>