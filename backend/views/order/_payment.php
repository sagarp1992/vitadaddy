<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Customer */
/* @var $form yii\widgets\ActiveForm */
?>

<h2 class="content-heading d-print-none">
<?= Html::a('Go back',['order/shipping'],['class'=>'btn btn-sm btn-rounded btn-success float-right']);?>
Payment Method
</h2>
<div class="block">
<div class="col-md-9">
    <ul class="nav nav-tabs nav-tabs-alt js-tabs-enabled" data-toggle="tabs" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link active" href="#credit-card" aria-expanded="true">Credit Card</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#debit-card" aria-expanded="false">DebitCard</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#internet-banking">Internet Banking</a>
                                        </li>
      </ul>
      <div class="block-content tab-content">
          <div class="tab-pane active" id="credit-card" role="tabpanel" aria-expanded="true">
              <?php                           
                $form = ActiveForm::begin([
                  'fieldConfig' => [
                    'template' => '<div class="form-group row"> 
                                        <label class="col-lg-3 col-form-label" for="example-hf-email">{label}</label>
                                        <div class="col-lg-7">
                                            {input}{hint}{error}
                                        </div>
                                </div>',
                
                ],
                  'options' => [
                      'class' => 'col-md-6'
                ]]); ?> 
                      <?= $form->field($model, 'card_number')->textInput(['class' => 'form-control','placeholder'=>'Card Number']); ?>
                      <?= $form->field($model, 'card_month')->dropDownList($model->month_list,['class' => 'form-control','prompt'=>'MM']) ?>
                      <?= $form->field($model, 'card_year')->dropDownList($model->year_list(),['class' => 'form-control','prompt'=>'YYYY']); ?>
                      <?= $form->field($model, 'card_cvv_no')->textInput(['class' => 'form-control','placeholder'=>'CVV']); ?>
                      <?= $form->field($model, 'card_holder_name')->textInput(['class' => 'form-control','placeholder'=>'Name on Card']); ?>
                      <?= $form->field($model, 'card_name')->textInput(['class' => 'form-control','placeholder'=>'Enter Card Name']); ?>                                
                      <div class="btn-confurm">
                          <?= Html::submitButton('Proceed to payment', ['class' => 'btn  btn-primary ']) ?>
                      </div>
                <?php ActiveForm::end(); ?>
          </div>
          <div class="tab-pane" id="debit-card" role="tabpanel" aria-expanded="false">
              
          </div>
          <div class="tab-pane" id="internet-banking" role="tabpanel">
            
          </div>
      </div>
   </div>
   <div class="col-md-3 cart-section">
            <?=$this->render('_cart_sidebar');?>                           
        </div>
    </div>