<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $searchModel common\models\SliderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Sliders');
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin(); ?>
<h2 class="content-heading d-print-none">
Manage Slider
 </h2>
    <div class="block">
      
        <div class="block-content block-content-full">
        <div class="form-group row">
                                            <div class="col-12">

                                            <?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data','class'=>'form-s']]); ?> 
                                              
                                                   <?= $form->field($model, 'background_image')->fileInput(['accept' => 'image/*','class'=>'']) ?>                                                
                                                
                                                    <div class="form-group" >
                                                        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
                                                    </div>
                                                    <?php ActiveForm::end(); ?>
                                            </div>
                                        </div>
        </div>
    </div>

    

  

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            [
                'attribute' => 'background_image',
                'format' => 'raw',    
                'filter'=>false,
                'value' => function ($data) {
                    return '<div class="img-banner">'.Html::img(Yii::$app->params['host'].'/img/'.$data['background_image'],
                        ['width' => '','height' => '']).'</div>';
                },
            ],
            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{delete}'
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>