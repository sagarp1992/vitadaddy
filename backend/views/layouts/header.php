  <!-- Header -->
  <?php
  use yii\helpers\Html;
  ?>
  <header id="page-header">
                <div class="content-header">
                    <div class="content-header-section">                      
                        <button type="button" class="btn btn-circle btn-dual-secondary" data-toggle="layout" data-action="sidebar_toggle">
                            <i class="fa fa-navicon"></i>
                        </button>
                    </div>
                    <div class="content-header-section">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-rounded btn-dual-secondary" id="page-header-user-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <?= Html::encode(Yii::$app->user->identity->username);?><i class="fa fa-angle-down ml-5"></i>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right min-width-150" aria-labelledby="page-header-user-dropdown">
                                <?=Html::a('<i class="si si-user mr-5"></i> Profile',['user/profile'],['class'=>'dropdown-item'])?></li>                                                          
                                <div class="dropdown-divider"></div>
                                <?=Html::a(' <i class="si si-logout mr-5"></i> Sign Out',['site/logout'],['class'=>'dropdown-item'])?></li>      
                               
                        </div>
                    </div>
                </div>
            </div>
        </header>
