<?php
/* @var $this \yii\web\View */
/* @var $content string */
use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use common\widgets\Alert;
AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Vitadaddy Admin Panel - <?= Html::encode($this->title) ?></title>
    <link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::$app->request->baseUrl.'/';?>app-assets/img/16x16.png">
    <?php $this->head() ?>
    <script> 
             var baseurl = "<?= Yii::$app->getUrlManager()->getBaseUrl();?>";
    </script>
</head>
<body>
<?php $this->beginBody() ?>
    <body data-col="2-columns" class=" 2-columns "> 
        <div id="page-container" class="sidebar-o side-scroll page-header-modern main-content-boxed sidebar-inverse">
            <?php include('leftbar.php');?>
            <?php include('header.php');?>
            <main id="main-container">
                <!-- Page Content -->
                <div class="content">
                    <?= $content; ?>
                </div>
            </main>
            <?php include('footer.php');?>

        </div>
    </body>
<?php $this->endBody() ?>

</html>
<?php $this->endPage() ?>
