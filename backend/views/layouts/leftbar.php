<?php
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\helpers\Html;
$Controller = Yii::$app->controller->id;
$Action     = Yii::$app->controller->action->id;
$OrderCreate ="";$OrderList="";$OpenOrder="";$Dashboard = "";
if($Controller == "order"){
    $OpenOrder   = "open";
    if($Action == "index"){
        $OrderList ="active";
    }
    if($Action == "create"){
        $OrderCreate ="active";
    }
}
$OpenProduct = "";$ProductCreate="";$ProductList= "";
if($Controller == "product"){
    $OpenProduct  = "open";
    if($Action == "index"){
        $ProductList ="active";
    }
    if($Action == "create" || $Action == "update"){
        $ProductCreate ="active";
    }
}
$OpenCms    ="";$flatext ="";$slider="";
if($Controller == "slider" || $Controller == "flatext"){
    $OpenCms  = "open";
    if($Controller == "slider"){
        $slider ="active";
    }
    if($Controller == "flatext"){
        $flatext ="active";
    }
}
$coupon ="";$OpenSystem="";
if($Controller == "coupon"){
    $OpenSystem  = "open";
    if($Controller == "coupon"){
        $coupon ="active";
    }
}
$profile ="";$OpenProfile="";$change_password="";
if($Controller == "user"){
    $OpenProfile  = "open";
    if($Action == "change-password"){
        $change_password ="active";
    }
    if($Action == "profile"){
        $profile ="active";
    }
}
if($Controller == "dashboard"){
    $Dashboard  =   "active";
} 
?>

          	
	  <nav id="sidebar">
                <!-- Sidebar Scroll Container -->
                <div id="sidebar-scroll">
                    <!-- Sidebar Content -->
                    <div class="sidebar-content">
                        <!-- Side Header -->
                        <div class="content-header content-header-fullrow px-15">                          
                            <div class="content-header-section text-center align-parent sidebar-mini-hidden">                               
                                <div class="content-header-item">
                                     <?= Html::a(Html::img(Yii::$app->request->baseUrl.'/app-assets/img/Logo.png',['class'=>'sidebar-logo']),['/dashboard/index']);?>
                                </div>
                                <!-- END Logo -->
                            </div>
                            <!-- END Normal Mode -->
                        </div>
                        <div class="content-side content-side-full">
                        <ul id='main-menu-navigation' class = 'nav-main'>
                                       <li class="<?= $Dashboard;?>">
                                             <?=Html::a('<i class="si si-cup"></i><span class="sidebar-mini-hide">Dashboard</span>',['dashboard/index'],['class'=>$Dashboard])?>
                                        </li>
                                        <li class="<?= $OpenOrder;?>">
                                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-basket"></i>
                                                <span class="sidebar-mini-hide">Orders</span>
                                            </a>
                                            <ul>
                                                 <li><?=Html::a('Order List',['order/index'],['class'=>$OrderList])?></li>
                                                 <li><?=Html::a('Create Order',['order/create'],['class'=>$OrderCreate])?></li>
                                            </ul>
                                        </li>                                       
                                        <li class="<?= $OpenProduct;?>">
                                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-social-dropbox"></i>
                                                <span class="sidebar-mini-hide">Products</span>
                                            </a>
                                            <ul>
                                                 <li><?=Html::a('Product List',['product/index'],['class'=>$ProductList])?></li>
                                                 <li><?=Html::a('Create Product',['product/create'],['class'=>$ProductCreate])?></li>
                                            </ul>
                                        </li>
                                        <li class="<?= $OpenCms;?>">
                                            <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-settings"></i><span class="sidebar-mini-hide">CMS Setting</span></a>
                                            <ul>
                                                 <li><?=Html::a('Home Page Slider',['slider/index'],['class'=>$slider])?></li>
                                                 <li><?=Html::a('Header Updates',['flatext/index'],['class'=>$flatext])?></li>
                                            </ul>
                                        </li>
                                        <li class="<?= $OpenSystem;?>">
                                           <a class="nav-submenu" data-toggle="nav-submenu" href="#"><i class="si si-settings"></i><span class="sidebar-mini-hide">System Setting</span></a>
                                            <ul>
                                                 <li><?=Html::a('Coupon generator',['coupon/index'],['class'=>$coupon])?></li>
                                            </ul>
                                        </li>
                        </ul>
						
                        </div>
                    </div>
                </div>
            </nav>
