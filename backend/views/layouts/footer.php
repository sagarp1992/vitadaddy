<footer id="page-footer" class="opacity-0">
                <div class="content py-20 font-size-xs clearfix">
                    <div class="float-right">
                       Vitadadyy<i class="fa fa-heart text-pulse"></i>
                    </div>
                    <div class="float-left">
                        <a class="font-w600" href="https://goo.gl/po9Usv" target="_blank">Vitadadyy</a> &copy; <span class="js-year-copy"><?php echo date('Y'); ?></span>
                    </div>
                </div>
            </footer>
