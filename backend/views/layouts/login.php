<?php
use backend\assets\AppAsset;
use yii\helpers\Html;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title>Vitadaddy Admin | <?= Html::encode($this->title) ?></title>
    <link rel="icon" type="image/png" sizes="16x16" href="<?= Yii::$app->request->baseUrl.'/';?>app-assets/img/16x16.png">
    <?php $this->head() ?>
</head>
<body data-col="1-column" class=" 1-column  blank-page blank-page">
<?php $this->beginBody() ?>

<div id="page-container" class="main-content-boxed">
            <!-- Main Container -->
            <main id="main-container">
                <!-- Page Content -->
                <div class="sidebar-inverse bg-pattern" style="background-color: #444040;">
                    <div class="row mx-0 justify-content-center">
                        <div class="hero-static col-lg-6 col-xl-4">
                            <div class="content content-full overflow-hidden">
                                <!-- Header -->
                                <div class="py-30 text-center">
                                    <?= Html::img(Yii::$app->request->baseUrl.'/app-assets/img/logo.png');?>
                                </div>
                                <?= $content ?>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
