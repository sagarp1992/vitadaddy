<?php
use miloschuman\highcharts\Highcharts;
?>
<h2 class="content-heading d-print-none">
Dashboard
 </h2>
<div class="row invisible mb-20" data-toggle="appear">                        
        <div class="col-6 col-xl-3">
            <a class="block block-rounded block-bordered block-link-shadow" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-right mt-15 d-none d-sm-block">
                        <i class="si si-bag fa-2x text-primary-light"></i>
                    </div>
                    <div class="font-size-h3 font-w600 text-primary" id="TotalSales">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Total Sales</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-3">
            <a class="block block-rounded block-bordered block-link-shadow" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-right mt-15 d-none d-sm-block">
                        <i class="si si-cup fa-2x text-earth-light"></i>
                    </div>
                    <div class="font-size-h3 font-w600 text-earth" id="TodaySales">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Today Sales</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-3">
            <a class="block block-rounded block-bordered block-link-shadow" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-right mt-15 d-none d-sm-block">
                        <i class="si si-basket fa-2x text-elegance-light"></i>
                    </div>
                    <div class="font-size-h3 font-w600 text-elegance" id="TotalOrder">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Total Order</div>
                </div>
            </a>
        </div>
        <div class="col-6 col-xl-3">
            <a class="block block-rounded block-bordered block-link-shadow" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-right mt-15 d-none d-sm-block">
                        <i class="si si-users fa-2x text-pulse"></i>
                    </div>
                    <div class="font-size-h3 font-w600 text-pulse"id="TotalCustomer">0</div>
                    <div class="font-size-sm font-w600 text-uppercase text-muted">Total Customer</div>
                </div>
            </a>
        </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default border-b">
                <h3 class="block-title">Sale Report By Day</h3>               
            </div>
            <div class="block-content block block-rounded" >
                <?php
                    echo Highcharts::widget([
                        'options' => [
                            'chart'=> [
                                'type'=> 'column'
                            ],
                        'title' => ['text' => 'Statistics of This Month'],
                        'xAxis' => [
                            'categories' => $Dates
                        ],
                        'yAxis' => [
                            'title' => ['text' => 'Sales Amount (In $)']
                        ],
                        'tooltip'=>[
                            'pointFormat'=> '$ {point.y:.1f}',
                          
                        ],
                        'series' => [
                                ['name' => 'Sales Amount', 'data' =>$Sales]
                        ]
                        ]
                    ]);
                ?>
            </div>
        </div>
    </div>   
</div>
<div class="row invisible mt-20" data-toggle="appear">
    <div class="col-md-6">
        <div class="block block-rounded block-bordered">
            <div class="block-header block-header-default border-b">
                <h3 class="block-title">Latest Orders</h3>               
            </div>
            <div class="block-content block block-rounded" id="LatestOrder">
                
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="block block-rounded">
            <div class="block-header block-header-default border-b">
                <h3 class="block-title">Top Products</h3>                
            </div>
            <div class="block-content block-bordered" id="TopProduct">
              
            </div>
        </div>
    </div>
</div>
<?php
    $this->registerJs('
        TotalSales();
        TodaySales();
        TotalOrder();
        TotalCustomer();
        LatestOrder();
        TopProduct();
        
        '
        
    );
?>
