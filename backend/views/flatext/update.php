<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Flatext */

$this->title = Yii::t('app', 'Update : {nameAttribute}', [
    'nameAttribute' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Flatexts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('app', 'Update');
?>
<h2 class="content-heading d-print-none">
Manage Site Offer Text
 </h2>
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>