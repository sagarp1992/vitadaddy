<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Flatext */

$this->title = Yii::t('app', 'Create Flatext');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Flatexts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<?= $this->render('_form', [
    'model' => $model,
]) ?>

