<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
/* @var $this yii\web\View */
/* @var $model common\models\Flatext */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="flatext-form">

    <?php $form = ActiveForm::begin(); ?>

    <?php 
    if(!$model->name){
             $form->field($model, 'name')->textInput(['maxlength' => true]) ;
    } ?>
    <?=  $form->field($model, 'text')->widget(CKEditor::className(), [
                'options' => ['rows' => 2],
                'preset' => 'advanced'
            ]); ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
