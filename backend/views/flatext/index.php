<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\FlatextSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Flatexts');
$this->params['breadcrumbs'][] = $this->title;
?>
<h2 class="content-heading d-print-none">
Manage Site Offer Text
 </h2>
<?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'name',
            [
                'attribute' => 'text',
                'format' => 'html',    
                'filter'=>false,
                'value' => function ($data) {
                    return $data['text'];
                },
            ],
            [
                'attribute' => 'created_at',
                'format' => 'html',    
                'filter'=>false,
                'value' => function ($data) {
                    return date('Y-m-d',$data['created_at']);
                },
            ],

            ['class' => 'yii\grid\ActionColumn','template'=>'{update}'],
            
        ],
    ]); ?>
<?php Pjax::end(); ?>
