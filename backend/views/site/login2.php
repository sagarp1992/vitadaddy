<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(['id' => 'login-form','options'=>['class'=>'js-validation-signin px-20']]); ?>
                                    
                                        <div class="col-12">
                                            <div class=" floating">
                                            <?= $form->field($model, 'username')->textInput(['autofocus' => true
										        ,'class'=>"form-control"]);?>                                                
                                            </div>
                                        </div>
                                    
                                 
                                        <div class="col-12">
                                            <div class=" floating">
                                                <?= $form->field($model, 'password')->passwordInput([
                                                    'class'=>"form-control"
                                                    ]);
                                                ?>
                                            </div>
                                    </div>     
                                    <div class="col-12 row">      
                                    <div class="col-md-6">                                              
                                        <?= Html::submitButton('<i class="si si-login mr-10"></i> Sign In',
                                         ['class' => 'btn btn-sm btn-hero btn-alt-primary', 
										 'name' => 'login-button']);
                                         ?>
                                    </div>
                                    <div class="col-md-6"> 
                                     <?= Html::a(' Forgot Password ?',['site/request-password-reset'],['class'=>'text-right link-effect text-muted mr-10 mb-5 d-inline-block'])?>
                                            
                                    </div>
                                    </div>
 <?php ActiveForm::end(); ?>


