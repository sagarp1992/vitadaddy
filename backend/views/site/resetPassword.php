<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ResetPasswordForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Reset password';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(['options' => ['class'=>'js-validation-signin']]); ?>
                                    <div class="block block-themed block-rounded block-shadow">
                                        <div class="block-header bg-gd-dusk">
                                            <h3 class="block-title">Change Password</h3>                                            
                                        </div>
                                        <div class="block-content">
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label for="login-username">Enter New Password</label>
                                                    <?= $form->field($model, 'password')->passwordInput(['autofocus' => true])->label(false);?>
                                                </div>
                                            </div>                                           
                                            <div class="form-group row mb-0">
                                                <div class="col-sm-6 d-sm-flex align-items-center push">                                                 
                                                   <?= Html::a('Sign In', ['site/index'],['class'=>'link-effect text-muted mr-10 mb-5 d-inline-block']) ?>.
                                                </div>
                                                <div class="col-sm-6 text-sm-right push">
                                                    <button type="submit" class="btn btn-alt-primary">
                                                        <i class="si si-login mr-10"></i> Change Password
                                                    </button>
                                                </div>
                                            </div>
                                        </div>                                       
                                    </div>
<?php ActiveForm::end(); ?>
