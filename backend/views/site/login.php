<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $form = ActiveForm::begin(['options' => ['class'=>'js-validation-signin']]); ?>
                                    <div class="block block-themed block-rounded block-shadow">
                                        <div class="block-header bg-gd-dusk">
                                            <h3 class="block-title">Please Sign In</h3>                                            
                                        </div>
                                        <div class="block-content">
                                        <?php if (Yii::$app->session->hasFlash('success')): ?>
                                                <div class="alert alert-info alert-dismissable">
                                                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                                    <p> <?= Yii::$app->session->getFlash('success') ?></p>                          
                                                </div>
                                            <?php endif; ?>
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label for="login-username">Username</label>
                                                    <?= $form->field($model, 'username')->textInput(['autofocus' => true])->label(false); ?>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <div class="col-12">
                                                    <label for="login-password">Password</label>
                                                    <?= $form->field($model, 'password')->passwordInput()->label(false); ?>
                                                </div>
                                            </div>
                                            <div class="form-group row mb-0">
                                                <div class="col-sm-6 d-sm-flex align-items-center push">                                                 
                                                   <?= Html::a('Forgot Password ?', ['site/request-password-reset'],['class'=>'link-effect text-muted mr-10 mb-5 d-inline-block']) ?>.
                                                </div>
                                                <div class="col-sm-6 text-sm-right push">
                                                    <button type="submit" class="btn btn-alt-primary">
                                                        <i class="si si-login mr-10"></i> Sign In
                                                    </button>
                                                </div>
                                            </div>
                                        </div>                                       
                                    </div>
<?php ActiveForm::end(); ?>
