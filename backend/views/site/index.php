<h2 class="content-heading">Static Header - Modern</h2>
                    <p>This is the default Header style, transparent and a bit closer to main content (screen width greater than 991px).</p>

                    <!-- Dummy content -->
                    <h2 class="content-heading">Dummy Content <small>To preview Header behaviour on scroll</small></h2>
                    <div class="block">
                        <div class="block-content">
                            <p class="text-center py-100">...</p>
                        </div>
                    </div>
                    <div class="block">
                        <div class="block-content">
                            <p class="text-center py-100">...</p>
                        </div>
                    </div>
                    <div class="block">
                        <div class="block-content">
                            <p class="text-center py-100">...</p>
                        </div>
                    </div>
                    <div class="block">
                        <div class="block-content">
                            <p class="text-center py-100">...</p>
                        </div>
                    </div>