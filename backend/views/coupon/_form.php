<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Coupon */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="coupon-form">

    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
         <div class="col-md-3">
            <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'type')->dropDownList([ 'Percentage' => 'Off Percentage', 'Amount' => 'Off Amount', ]) ?>
        </div>
        <div class="col-md-3">
              <?= $form->field($model, 'value')->textInput() ?>
        </div>
        <div class="col-md-3">
             <?= $form->field($model, 'status')->dropDownList([ 'UnUsed' => 'UnUsed', 'Used' => 'Used', ]) ?>
        </div>
    </div>


    <div class="form-group">
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
