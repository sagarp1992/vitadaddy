<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\CouponSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Coupons');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="coupon-index">

<h2 class="content-heading d-print-none">
        Generate a Coupon
 </h2>
    <?php Pjax::begin(); ?>
    <?php echo $this->render('_form', [
            'model' => $model,
        ]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],  
            'code',
            [
                'attribute' => 'type',
                'format' => 'html', 
                'filter'=>array('Amount'=>'Amount','Percentage'=>'Percentage')   ,
                'value' => function ($data) {
                    return $data->type;
                },
            ],   
            [
                'attribute' => 'value',
                'format' => 'html',    
                'value' => function ($data) {
                    return $data['type']=="Percentage"?$data['value'].'% Off ':"INR ".$data['value'].' Off';
                },
            ], 
            [
                'attribute' => 'created_at',
                'format' => 'html', 
                'filter'=>false,
                'value' => function ($data) {
                    return $data->created_at;
                },
            ],  
            [
                'attribute' => 'status',
                'format' => 'html', 
                'filter'=>array('Used'=>'Used','UnUsed'=>'UnUsed')   ,
                'value' => function ($data) {
                    return $data->status;
                },
            ],   
            ['class' => 'yii\grid\ActionColumn','template'=>'{update} {delete}'],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
