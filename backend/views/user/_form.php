<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
$model->password_hash="";
?>
     
    

    <div class="row">
        <div class="col-md-6">
                 <h2 class="content-heading">Edit My Profile</h2>
                <?php if (Yii::$app->session->hasFlash('profile_success')): ?>
                    <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Saved!</h4>
                    <?= Yii::$app->session->getFlash('profile_success') ?>
                    </div>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('profile_failed')): ?>
                    <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Oops!</h4>
                    <?= Yii::$app->session->getFlash('profile_failed') ?>
                    </div>
                <?php endif; ?>
                <?php $form = ActiveForm::begin(); ?>
                    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Update Profile'), ['class' => 'btn btn-success']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-6">
                <h2 class="content-heading">Change Password</h2>
                <?php if (Yii::$app->session->hasFlash('password_success')): ?>
                    <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Saved!</h4>
                    <?= Yii::$app->session->getFlash('password_success') ?>
                    </div>
                <?php endif; ?>
                <?php if (Yii::$app->session->hasFlash('password_failed')): ?>
                    <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Oops!</h4>
                    <?= Yii::$app->session->getFlash('password_failed') ?>
                    </div>
                <?php endif; ?>
                <?php
                $form = ActiveForm::begin(['action'=>['/user/change-password']]); ?>
                     <?= $form->field($PasswordForm, 'password')->passwordInput(['maxlength' => true])->label('Password'); ?>
                     <?= $form->field($PasswordForm, 'password_repeate')->passwordInput(['maxlength' => true])->label('Confirm Password'); ?>
                     <div class="form-group">
                        <?= Html::submitButton(Yii::t('app', 'Change Password'), ['class' => 'btn btn-success']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
        </div>
</div>

