<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Users');
$this->params['breadcrumbs'][] = $this->title;
?>
<section id="extended">
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-header">
                    	  <h4 class="card-title"><?php echo $this->title;?></h4>
                </div>
                <div class="card-body">
                    <div class="card-block">
						   <?php Pjax::begin(); ?>   
                                    <?= GridView::widget([
                                        'dataProvider' => $dataProvider,
                                        'filterModel' => $searchModel,
										'filterSelector' => '#' . Html::getInputId($searchModel, 'pagesize'),
									
                                        'columns' => [
                                            ['class' => 'yii\grid\SerialColumn'],
											 ['class' => 'yii\grid\CheckboxColumn'],
                                            'username',
                                            'email:email',
                                            'status',
                                            [
												'class' => 'yii\grid\ActionColumn',
												'template' => '{view}{update}{delete}',
												 'headerOptions'=>['style'=>'width:10%']
											],
                                        ],
                                    ]); ?>
                            <?php Pjax::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

