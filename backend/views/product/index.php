<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel common\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Products');
$this->params['breadcrumbs'][] = $this->title;
?>
    <h2 class="content-heading d-print-none">
                        <?= Html::a(Yii::t('app', 'Create Product'), ['create'], ['class' => 'btn btn-sm  btn-success float-right']) ?>
                        <?= Html::encode($this->title) ?>
    </h2>
    <?php Pjax::begin(); ?>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'image',
                'format' => 'html',    
                'filter'=>false,
                'value' => function ($data) {
                    return Html::img($data['image'],
                        ['width' => '50px','height' => '50px']);
                },
            ],
            'sku',
            'name', 
            [
                'attribute' => 'price',
                'format' => 'html',    
                'value' => function ($data) {
                    return '$ '.$data['price'];
                },
            ],       
            'brand',
            'flavour',
            'size',
            //'combination_id',
            //'meta_title:ntext',
            //'meta_description:ntext',
            //'meta_keyword:ntext',
            //'import_url:ntext',
            //'real_retail_price',
            //'real_sale_price',
            //'is_active',
            'created_at:datetime',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn',
                'headerOptions'=>['style'=>'width:10%'],
                'buttons'=>[
                    'view' => function ($url, $model) {     
                    return Html::a('<i class="fa fa-search"></i>',Yii::$app->urlManagerFrontEnd->createUrl(['product/view','slug'=>$model->slug]), [
                            'title' => Yii::t('yii', 'View'),'class'=>'btn btn-sm btn-info',
                            'target'=>'_blank'
                    ]);                                

                    }
                ]
            ],
        ],
    ]); ?>
    <?php Pjax::end(); ?>