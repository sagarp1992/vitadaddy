<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dosamigos\ckeditor\CKEditor;
use \common\models\Category;
$Item   = createItem(1,true,$ProductCategory);
function createItem($parent_id,$folder,$ProductCategory){
    $Category=Category::find()->where(['parent_id'=>$parent_id])->all();
    $Itesm  =array();
    foreach($Category as $f=>$each){
        $hasChild   =   Category::find()->where(['parent_id'=>$each['id']])->all();
        $Itesm[$f]['title']          =  $each['name'].' |'.$each['id'];
        $Itesm[$f]['key']            =  $each['id'];
        $Itesm[$f]['folder']         =  !empty($hasChild)?true:false;
        $Itesm[$f]['children']       =  createItem($each['id'],true,$ProductCategory);
        $Itesm[$f]['selected']       =  in_array($each['id'],$ProductCategory)?true:false;
        $Itesm[$f]['expanded']     =    true;
    }   
     return $Itesm;
}
/* @var $this yii\web\View */
/* @var $model common\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="product-form">

    <?php $form = ActiveForm::begin(['id'=>'product-form']); ?>
    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'sku')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?=  $form->field($model, 'description')->widget(CKEditor::className(), [
                'options' => ['rows' => 6],
                'preset' => 'advanced'
            ]); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">          
                <?= $form->field($model, 'price')->textInput() ?>         
        </div>
        <div class="col-md-4">          
                <?= $form->field($model, 'retail_price')->textInput() ?>         
        </div>
        <div class="col-md-4">          
                <?= $form->field($model, 'brand')->textInput() ?>         
        </div>
       
    </div> 
    <div class="row">
        <div class="col-md-4">          
            <?= $form->field($model, 'size')->textInput() ?>         
        </div>
        <div class="col-md-4">          
            <?= $form->field($model, 'flavour')->textInput() ?>         
        </div>
        <div class="col-md-4">          
                <?= $form->field($model, 'slug')->textInput() ?>         
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">          
            <?= $form->field($model, 'image')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">          
                 <?= $form->field($model, 'meta_title')->textarea(['rows' => 6]) ?>     
        </div>
        <div class="col-md-6">          
                 <?= $form->field($model, 'meta_keyword')->textarea(['rows' => 6]) ?>
        </div>       
    </div>
    <div class="row">
        <div class="col-md-12">          
                <?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?> 
        </div>
    </div>
    <div class="row">
        <div class="col-md-12"> 
        <label class="control-label" for="adminproduct-sku">Select Category </label>
        <div class="tree-view"> 
        <?php echo yii2mod\tree\Tree::widget([
                    'items' => $Item,
                    'clientOptions' => [
                        'autoCollapse' => true,
                         'autoScroll'=>true,
                        'clickFolderMode' => 3,
                        //'minExpandLevel'=>4,
                        'checkbox'=> true,
                        'quicksearch'=>true,
                        'select' => new \yii\web\JsExpression('
                            function(node, data) {                                   
                                                    
                            }
                        '),
                    ],
                ]); ?>
        </div>
     </div>
    </div>
    <div class="row">
     <div class="col-md-12"> 
        <?= Html::submitButton(Yii::t('app', 'Save'), ['class' => 'btn btn-success','style'=>'margin-top:10px']) ?>
    </div>
    </div>        
    <?php ActiveForm::end(); ?>

</div>