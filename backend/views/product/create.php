<?php

use yii\helpers\Html;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\models\Product */

$this->title = Yii::t('app', 'Create Product');
$this->params['breadcrumbs'][] = ['label' => Yii::t('app', 'Products'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php Pjax::begin(); ?>
<h2 class="content-heading"><?= Html::encode($this->title) ?></h2>

<?= $this->render('_form', [
    'model' => $model,
    'ProductCategory'=>$ProductCategory
]) ?>
<?php Pjax::end(); ?>

