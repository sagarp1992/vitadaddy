<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\AdminLoginForm;
use common\models\Category;
use common\models\Product;
use common\models\ProductCategory;
set_time_limit(0);
include_once('simple_html_dom.php');
/**
 * Site controller
 */
class ScrapperCategoryController extends Controller
{

    public function curl($url){    
        $headers = array(
            'accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'accept-encoding:gzip, deflate, br',
            'accept-language:en-US,en;q=0.9',
            'upgrade-insecure-requests:1',
            'user-agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
        );
        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "gzip, deflate, br",       // handle all encodings
            CURLOPT_USERAGENT      => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 20,      // timeout on connect
            CURLOPT_TIMEOUT        => 20,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false,    // Disabled SSL Cert checks
            CURLOPT_SSL_VERIFYHOST => false,    // Disabled SSL Cert checks
            CURLOPT_HTTPHEADER => $headers,           
        );        
        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );
        if($err){
            $content['status']=false;
            print_r($errmsg);
            print_r($header);
            // echo 'error='.$err;die;
        }
        return $content;
    }
    private function Fetchcategory($url,$CategoryId, $increment = '',$level=0){  
           
        $dash = "----". $increment;
        $content=$this->curl($url) ;
        $categoryName   = array();
        if(isset($content['status']) && $content['status'] == false){
            echo 'Error Above ------------<br>';
            return;
        }
        $dom = new \simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);
        $html=$dom->load($content, true, true);   
        if(!empty($html->find('.guided-nav__section-content',0))){
            $ul = $html->find('.guided-nav__section-content',0)->find('.guided-nav__dimensions-list',0);
            $position=1; 
           // $p[$level]      = $CategoryId;    
            foreach($ul->find('li') as $k=>$c) { 
                if(!empty($c->find('.guided-nav__list-link--brand',0)->href)){ 
                   $tName = str_replace("&nbsp;",'',strip_tags(strtok($c->find('.guided-nav__list-link--brand',0)->plaintext,'(')));
                   $tUrl = $c->find('.guided-nav__list-link--brand',0)->href.'&allCategories=true';                 
                    $thisCatId = $this->saveCategory(array('name'=>$tName,'parent_id'=>$CategoryId,'href'=>$tUrl,'position'=>$position));   
                    echo $dash.$tName.'('.$position.') ID: ' . $thisCatId . '<br>';
                    ob_flush();
                    $ChildData = $this->Fetchcategory($tUrl, $thisCatId, $increment.'----');
                    
                    $position++;
                }
            }
        }else{
                echo'-----------  End  --------<br>';
        }     
        return $categoryName;    
    }
    public function actionIndex(){
        $Category   = Yii::$app->db->createCommand('SELECT * FROM category WHERE `parent_id` IN (SELECT id FROM category WHERE `parent_id` IN 
        (SELECT id FROM `category` WHERE `parent_id` IN (SELECT id FROM `category` WHERE `parent_id` = 1))) ORDER BY `category`.`id` DESC')->queryAll();
        foreach($Category as $ele ){
               if($ele['id']<2185){
                    ob_implicit_flush(true);
                    ob_end_flush();
                    ob_start(); 
                    $url = $ele['import_url'].'&allCategories=true';
                    echo $ele['name'].'<br>';
                    $categoryName = $this->Fetchcategory($url,$ele['id']);
               }
        }
    }
    public function saveCategory($CategoryData){            
        $title  = ""; $description="";$keyword="";
        if(!empty($CategoryData['href'])){
            $CategoryData['href'] = preg_replace("/^http:/i", "https:", $CategoryData['href']);
            $content=$this->curl($CategoryData['href']) ;
            $dom = new \simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);
            $html=$dom->load($content, true, true); 
           
            ///################### Description ##############################
            if(!empty($html->find('meta[name=description]',0))){
                $desc   =   $html->find('meta[name=description]',0)->content;
                if (strpos($desc, 'Vitacost.com') !== false) {
                    $description   =        str_replace("Vitacost",' Vitadaddy',$desc);
                }else{
                    $description   =        str_replace("Vitacost",' Vitadaddy.com',$desc); 
                }  
                if (strpos($description, 'Vitacost') !== false) {
                    $description   =        str_replace("Vitacost",'Vitadaddy',$description); 
                }
            }
            ///################### Title ##############################
            if(!empty($html->find('title',0))){
                $title             =       $html->find('title',0)->plaintext;            
                if (strpos($title, 'Vitacost.com') !== false) {
                    $title    =       str_replace("Vitacost.com",'Lowest Price in India at Vitadaddy.com',$title);
                }else{
                    $title   =        str_replace("Vitacost",'Lowest Price in India at Vitadaddy.com',$title); 
                }  
                if (strpos($title, 'Vitacost') !== false) {
                    $title   =        str_replace("Vitacost",'Vitadaddy',$title); 
                }
            }
            if(!empty($html->find('meta[name=keywords]',0))){
                $keyword            =  $html->find('meta[name=keywords]',0)->content;
                if (strpos($keyword, 'Vitacost.com') !== false) {
                    $keyword    =       str_replace("Vitacost.com",'Vitadaddy',$keyword);
                }else{
                    $keyword   =        str_replace("Vitacost",'Vitadaddy',$keyword); 
                } 
                if (strpos($keyword, 'Vitacost') !== false) {
                    $keyword   =        str_replace("Vitacost",'Vitadaddy',$keyword); 
                }
                if (strpos($keyword, 'Vitadaddy') !== false) {
                }else{
                    $keyword   =  $keyword.' - Vitadaddy';
                }
            }
        }      
    
        $model                     = new Category();
        $model->name               = str_replace("&amp;",' & ',$CategoryData['name']); 
        $model->slug               = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $model->name))).'-'.$CategoryData['parent_id'];
        $model->parent_id          = $CategoryData['parent_id'];
        $model->created_at         = time();
        $model->updated_at         = time();
        $model->position           = !empty($CategoryData['position'])?$CategoryData['position']:"0";
        $model->is_active          = 1;
        $model->meta_keyword       = trim($keyword);
        $model->meta_title         = trim($title);
        $model->meta_description   = trim($description);
        $model->import_url         = !empty($CategoryData['href'])?$CategoryData['href']:"";
        $id =  $model->save()?$model->id:0;
        return $id ;
    }
}
?>