<?php
namespace backend\controllers;
use Yii;
use \common\models\Admin;
use \common\models\PasswordForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

class UserController extends Controller
{
    public function behaviors(){
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['profile', 'change-password'],
                'rules' => [               
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }
    public function actionProfile(){
        $id= Yii::$app->user->identity->id;
        $model = $this->findModel($id);
        $PasswordForm = new PasswordForm;;
        if ($model->load(Yii::$app->request->post())) {  
            $model->username = $_POST['Admin']['username'];
            $model->email    = $_POST['Admin']['email'];           
            if($model->validate() && $model->save()){
                Yii::$app->session->setFlash('profile_success', "Your details has been updated.");
           }else{
              Yii::$app->session->setFlash('profile_failed', json_encode($model->errors));
           }
        }
        return $this->render('_form', [
            'model' => $model,
            'PasswordForm'=>$PasswordForm
           
        ]);
    }
    public function actionChangePassword(){
        $id= Yii::$app->user->identity->id;
        $model = $this->findModel($id);
        $PasswordForm = new PasswordForm;
        if ($PasswordForm->load(Yii::$app->request->post())) { 
            $model->password_hash= Yii::$app->security->generatePasswordHash($PasswordForm->password); 
            if($model->validate() && $model->save()){
                Yii::$app->session->setFlash('password_success', "Your passowrd has been updated.");
            }else{
              Yii::$app->session->setFlash('password_failed', json_encode($model->errors));
            }
        }
        return $this->redirect(['profile']);
    }
    protected function findModel($id)
    {
        if (($model = Admin::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
