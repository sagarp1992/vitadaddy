<?php

namespace backend\controllers;

use Yii;
use common\models\Order;
use common\models\OrderSearch;
use common\models\OrderDetaills;

use common\models\PaymentCard;

use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\AdminProduct;
use app\models\AdminProductSearch;
use common\models\Customer;
class OrderController extends Controller
{
    
    public function behaviors(){
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','view','payment','shipping','address','add-to-cart','remove-cart','cancel-cart','confirmation','delete','update'],
                'rules' => [               
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    private function prepare(){
        if(Yii::$app->cart->cartId() == 0){
            Yii::$app->session->setFlash('info','Please add item in cart.');
            return $this->redirect('create');
        }
        return ;
    }
    public function actionIndex()
    {
        $searchModel = new OrderSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionPayment(){
        $this->prepare();
        $model = new PaymentCard();         
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $res = Yii::$app->cart->placeOrder($model->id);    
            return $this->redirect(['confirmation','order_id'=>$res['order_id']]);  
        }
        return $this->render('_payment', [
            'model' => $model,
        ]);
    }
    public function actionShipping()
    {
        $this->prepare();
        if (!empty($_GET['shipping_method'])) {
            return $this->redirect(['order/payment']);
        }
        return $this->render('_shipping');
    }
    public function actionAddress()
    {
        $this->prepare();
        $res = Yii::$app->cart->CustomerInCart(); 
        if($res['status']==true && !empty($res['customer_id']) && $res['customer_id'] > 0){
            $model = Customer::find()->where(['id'=>$res['customer_id']])->one();
        }else{
             $model = new Customer();   
        }    
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $CustomerData   = array('customer_id'=>$model->id);
            $res = Yii::$app->cart->AddCustomerToCart($CustomerData);
            if($res['status']== true){
                return $this->redirect(['order/shipping']);
            }else{
                Yii::$app->session->setFlash('customer_failed', $res['message']);
            }           
        }
        return $this->render('_address', [
            'model' => $model,
        ]);
    }
    public function actionAddToCart($sku,$qty=1,$total_qty=0,$isJson=false){       
        $CartData = array('sku'=>$sku,'qty'=>$qty,'total_qty'=>$total_qty);
        $res =  Yii::$app->cart->AddToCart($CartData);
        if(!$isJson){
            if($res['status']== true){
                Yii::$app->session->setFlash('cart_success', $res['message']);
            }else{
                Yii::$app->session->setFlash('cart_failed', $res['message']);
            }
            return $this->redirect(Yii::$app->request->referrer);
        }else{
            $res['html'] = $this->renderAjax('_cart_sidebar');
            echo json_encode($res);die;
        }
    }
    public function actionRemoveCart($sku){
        $CartData = array('sku'=>$sku);
        $res =  Yii::$app->cart->RemoveItemFromCart($CartData);        
        if($res['status']== true){
            Yii::$app->session->setFlash('view_cart_success', $res['message']);
        }else{
            Yii::$app->session->setFlash('view_cart_failed', $res['message']);
        }
        return $this->redirect(Yii::$app->request->referrer);
    }
    public function actionCreate()
    {
        $searchModel = new AdminProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('_product', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionCancelCart(){
        Yii::$app->cart->clearCart();
        return $this->redirect(['order/create']);
    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }
    }
    public function actionConfirmation($order_id)
    {
        
        $model = Order::find()->where(['order_no'=>$order_id])->one();
        $OrderDetaills  =   OrderDetaills::find()->select(['product.*','order_detaills.*'])->where(['order_id'=>$model->id])
                             ->leftJoin('product', 'order_detaills.product_id=product.id')->asArray()
                             ->all();

        $Customer       =   Customer::find()->where(['id'=>$model->custome_id])->one();
        return $this->render('_confirmation',['model'=> $model,'OrderDetaills'=>$OrderDetaills,'Customer'=>$Customer]);
    }
    public function actionDelete($id){
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }
    protected function findModel($id){
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        }
        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
