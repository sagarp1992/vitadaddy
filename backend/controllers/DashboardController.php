<?php
namespace backend\controllers;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\Order;
use common\models\OrderDetails;
use common\models\Category;
use common\models\Product;
use common\models\ProductCategory;
class DashboardController extends Controller
{
    public $enableCsrfValidation = false;
    public function behaviors(){
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','dashboard'],
                'rules' => [               
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionIndex(){

        $Sql= "SELECT DATE_FORMAT(FROM_UNIXTIME(`order_date`), '%Y-%m-%d') as Date,SUM(`order_total`) as Sales FROM `order`
         WHERE status='Completed' AND DATE_FORMAT(FROM_UNIXTIME(`order_date`), '%Y-%m-%d') >= LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY - INTERVAL 1 MONTH 
         AND DATE_FORMAT(FROM_UNIXTIME(`order_date`), '%Y-%m-%d') < LAST_DAY(CURRENT_DATE) + INTERVAL 1 DAY 
         GROUP BY DATE_FORMAT(FROM_UNIXTIME(`order_date`), '%Y-%m-%d')";
        $Data  = Yii::$app->db->createCommand($Sql)->queryAll();
        $Array = ArrayHelper::map($Data,'Date','Sales');

        $maxDays=date('t');
        for($day=1;$day<=$maxDays;$day++){
           $day =$day<10?'0'.$day:$day;
           $date    = date('Y').'-'.date('m').'-'.$day;
           $Dates[] = date('d F,Y ',strtotime($date));
           $Sales[] = !empty($Array[$date])?(float)$Array[$date]:0;
        }
        return $this->render('index',[
            'Dates'=>$Dates,
            'Sales'=>$Sales
        ]);
    }
    public function actionDashboard(){          
        switch ($_POST['action']) {
            case "TotalSales":
                $res =  $this->TotalSale();
                break;
            case "TodaySales":
                $res = $this->TodaySale();
                break;
            case "TotalCustomer":
                $res = $this->TotalCustomer();
                break;
            case "TotalOrder":
                $res = $this->TotalOrder();
                break;
            case "LatestOrder":
                $res = $this->LatestOrder();
                break;
            case "TopProduct":
                $res = $this->TopProduct();
                break;
            default:
               $res['status']   =   false;
               $res['message']  =   'Action name is missing';
        }
        echo json_encode($res);die;
    }
    private function TopProduct(){
        $sql = 'SELECT COUNT(order_detaills.qty) as TotalSoldItem,product.sku,product.name,order_detaills.product_id,order_detaills.order_id, SUM((order_detaills.price*order_detaills.qty)) as SaleAmount  FROM `order_detaills` 
        LEFT JOIN product on order_detaills.product_id = product.id
        WHERE order_detaills.order_id IN (SELECT order.id FROM `order` WHERE order.status = "Completed") GROUP BY order_detaills.product_id ORDER BY SaleAmount DESC LIMIT 0,10';
        $Products = Yii::$app->db->createCommand($sql)->queryAll();

        if(!empty($Products)){  
            $html = '<table class="table table-borderless table-striped">
                <thead>
                    <tr>
                        <th>SKU</th>
                        <th>NAME</th>
                        <th>SALE</th>                                
                        <th>SOLD QTY</th>
                    </tr>
                </thead>
            <tbody>';         
            foreach($Products as $ele){               
                $html.='
                        <tr>
                            <td>
                                '.Html::a($ele['sku'],['product/update','id'=>$ele['product_id']],['target'=>'_blank']).'
                            </td>
                            <td>
                                '.Html::encode($ele['name']).'
                            </td>
                            <td>$'.$ele['SaleAmount'].'</td>                          
                            <td>'.$ele['TotalSoldItem'].'</td>    
                        </tr> ';
            }             
            $html.= ' </tbody>';
            $html.= '</table>';   
        }else{
            $html ='<div class="font-size-sm p-10 text-center text-muted">No Product Sell Yet</div>';
        }      
        $res['status']  = true;
        $res['message'] = $html;
        return $res;
    }
    private function LatestOrder(){
        $Order = Order::find()->where(1)->orderby('id DESC')->limit(10)->asArray()->all();     
        if(!empty($Order)){  
            $html = '<table class="table table-borderless table-striped">
                <thead>
                    <tr>
                        <th>Oreder No</th>
                        <th>Date</th>
                        <th>Status</th>                                
                        <th class="text-right">Amount</th>
                    </tr>
                </thead>
            <tbody>';         
            foreach($Order as $ele){
                if($ele['status']=="New Order"){
                    $orderStatus = ' <span class="badge badge-info">New Order</span>';
                }else if($ele['status']=="Pending"){
                    $orderStatus = ' <span class="badge badge-warning">Pending</span>';
                }else if($ele['status']=="Completed"){
                    $orderStatus = ' <span class="badge badge-success">Completed</span>';
                }else if($ele['status']=="Cancelled"){
                    $orderStatus = ' <span class="badge badge-danger">Cancelled</span>';
                }
                $html.='
                        <tr>
                            <td>
                                '.Html::a($ele['order_no'],['order/view','id'=>$ele['id']],['target'=>'_blank']).'
                            </td>
                            <td>
                                '.date('d F,Y H:i A',$ele['order_date']).'
                            </td>
                            <td>'.$orderStatus.'</td>                            
                            <td class="text-right">
                                <span class="text-black">$ '.$ele['order_total'].'</span>
                            </td>
                        </tr> ';
            }             
            $html.= ' </tbody>';
            $html.= '</table>';   
        }else{
            $html ='<div class="font-size-sm p-10 text-center text-muted">No Order Yet</div>';
        }      
        $res['status']  = true;
        $res['message'] = $html;
        return $res;
    }
    private function TotalSale(){
        $sql            = "SELECT SUM(`order_total`) as TotalSale FROM `order` WHERE status = 'Completed'";
        $totalSale      = Yii::$app->db->createCommand($sql)->queryOne();
        $res['status']  = true;
        $res['message'] = !empty($totalSale['TotalSale'])?$totalSale['TotalSale']:0;
        return $res;
    }
    private function TodaySale(){
        $FromDate   = strtotime("today");
        $ToDate     = $FromDate+86400;
        $Sql        = "SELECT SUM(`order_total`) as TotalSale FROM `order` WHERE status = 'Completed' AND order_date BETWEEN '".$FromDate."' AND '".$ToDate."'";
        $totalSale  = Yii::$app->db->createCommand($Sql)->queryOne();
        $res['status']  = true;
        $res['message'] = !empty($totalSale['TotalSale'])?$totalSale['TotalSale']:0;
        return $res;
    }
    private function TotalCustomer(){
        $Sql             = "select count(distinct email) as count from customer group by email";
        $customer        =  array_sum(ArrayHelper::map(Yii::$app->db->createCommand($Sql)->queryAll(),'count','count'));       
        $res['status']   = true;        
        $res['message']  = !empty($customer)?$customer:0;
        return $res;
    }
    private function TotalOrder(){
        $Order = Order::find()->where(1)->count();
        $res['status']   = true;        
        $res['message']  = $Order;
        return $res;
    }
}
?>