<?php

namespace backend\controllers;

use Yii;
use app\models\AdminProduct;
use app\models\AdminProductSearch;
use \common\models\ProductCategory;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    public function behaviors(){
        return [
            'access' => [
                'class' => \yii\filters\AccessControl::className(),
                'only' => ['index','view','create','update','delete'],
                'rules' => [               
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    public function actionIndex()
    {
        $searchModel = new AdminProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionCreate()
    {
        $model = new AdminProduct();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            if(!empty($_POST['categoty_id'])){
                foreach($_POST['categoty_id'] as $category_id){
                     $ProductCategory = new ProductCategory;
                     $ProductCategory->category_id = $category_id;
                     $ProductCategory->product_id = $model->id;
                     $ProductCategory->save(false);
                }
            }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'ProductCategory'=>array()
        ]);
    }
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $ProductCategory = ArrayHelper::map(ProductCategory::find()->where(['product_id'=>$id])->asArray()->all(),'category_id','category_id');
      
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
           if(!empty($_POST['categoty_id'])){
               Yii::$app->db->createCommand('DELETE FROM `map_product_category` WHERE `product_id`=:product_id')->bindValue(':product_id',$model->id)->execute();
               foreach($_POST['categoty_id'] as $category_id){
                    $ProductCategory = new ProductCategory;
                    $ProductCategory->category_id = $category_id;
                    $ProductCategory->product_id = $model->id;
                    $ProductCategory->save(false);
               }
           }
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update',[
            'model' => $model,
            'ProductCategory'=>$ProductCategory
        ]);
    }
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    protected function findModel($id)
    {
        if (($model = AdminProduct::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException(Yii::t('app', 'The requested page does not exist.'));
    }
}
