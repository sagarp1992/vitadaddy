<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\AdminLoginForm;
use common\models\Category;
use common\models\Product;
use common\models\ProductCategory;
set_time_limit(0);
include_once('simple_html_dom.php');
/**
 * Site controller
 */
class ScrapperController extends Controller
{


    public $position = 1;
    private function saveNewProduct(){
        
    }
    public function actionOnlyProductFetch(){

        $Products =Yii::$app->db->createCommand('SELECT *  FROM `product` WHERE `id` = 32780')->queryAll();  
        foreach($Products as $k=>$ele){ 
            
            ob_implicit_flush(true);
            ob_end_flush();
            ob_start(); 
           
            $href   = $ele['import_url'];
            //################ If Product Exist & Map Category ###############
            $ExistProduct  = Product::find()->where(['id'=>$ele['id']])->one();
            //################ If New Product ###############
            $title  = ""; $description="";$keyword="";
            $datas = array(); 
            $getHrefData = $this->curl($href);
          
            if(!isset($getHrefData['status'])){ 
                        ///################ Load Product Deatil Page Html ##################
                        $dom                = new \simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);
                        $getHrefData        = $dom->load($getHrefData, true, true);
                        ///################ Get All Attributes of Product ##################
                        $currentProdData    = $this->parseProdData($getHrefData);
                       // print_r($currentProdData);die;
                        $currentProdData['update'] =1;
                        $currentProdData['combination_id'] ="";
                        $this->saveProduct($href, $currentProdData);
                        echo 'id----'.$ele['id'].'----saved <br>';
            }else{
                echo 'id----'.$ele['id'].'----Failed <br>';
            }         
            ob_flush();
        }
            
        
    }
    public function actionFetchProduct(){
        $ProductGroupByCategory  =Yii::$app->db->createCommand('SELECT category_id FROM `product_temp` WHERE 1 GROUP BY category_id ORDER BY `product_temp`.`category_id` DESC LIMIT 50,1845')->queryAll();     
       
        foreach($ProductGroupByCategory as $category){
            if($category['category_id'] <= 904){

                // try{
                //     $curl = curl_init();
                //     curl_setopt_array($curl, array(
                //     CURLOPT_URL => "http://teamgroovy.in/mymail/index.php?no=".$category['category_id'],
                //     CURLOPT_RETURNTRANSFER => true,
                //     CURLOPT_ENCODING => "",
                //     CURLOPT_MAXREDIRS => 10,
                //     CURLOPT_TIMEOUT => 30,
                //     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                //     CURLOPT_CUSTOMREQUEST => "GET",
                //     CURLOPT_HTTPHEADER => array(
                //         "cache-control: no-cache",
                //         "postman-token: 44f8b37b-4529-aa05-f6ef-baf63ce5cbbb"
                //     ),
                //     ));
    
                //     $response = curl_exec($curl);
                //     $err = curl_error($curl);
    
                //     curl_close($curl);
                // }catch(\Exception $e){
    
                // }


                $Categoryid = $category['category_id'];
                $Products =Yii::$app->db->createCommand('SELECT * FROM `product_temp` WHERE ( is_done ="Pending" OR is_done ="" ) AND category_id = '.$Categoryid)->queryAll();     
                $position = 1;
                foreach($Products as $k=>$ele){ 
                    
                    ob_implicit_flush(true);
                    ob_end_flush();
                    ob_start(); 
                    echo 'Position===='.$position.'';              
                    $href   = $ele['url'];
                    //################ If Product Exist & Map Category ###############
                    $ExistProduct  = Product::find()->where('import_url="https://www.vitacost.com'.$href.'" OR import_url="'.$href.'"')->asArray()->one();
                
                    if(!empty($ExistProduct)){
                        $ProductId  =  $ExistProduct['id'];  
                        $ExistProductCategory  = ProductCategory::find()->where(['product_id'=>$ProductId,'category_id'=> $Categoryid])->asArray()->one();
                        if(empty( $ExistProductCategory )){
                            $ProductCategory                =   new  ProductCategory;
                            $ProductCategory->product_id    =   $ProductId;
                            $ProductCategory->category_id   =   $Categoryid;
                            $ProductCategory->position      =   $position;
                            $ProductCategory->save(false);
                        }
                        $sql = "UPDATE `product_temp` SET `is_done`='Already',order_id= '".$position."',`Response`='Product is already exist' WHERE id =".$ele['id'];;
                        Yii::$app->db->createCommand($sql)->execute();
                        echo '<pre>';
                        print_r($ele);
                        echo '<pre>';
                        $position++;
                        continue;
                    }        
                    //################ If New Product ###############
                    $title  = ""; $description="";$keyword="";
                    $datas = array(); 
                    $getHrefData = $this->curl($href);
                    if(!isset($getHrefData['status'])){ 

                                ///################ Load Product Deatil Page Html ##################
                                $dom                = new \simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);
                                $getHrefData        = $dom->load($getHrefData, true, true);

                                ///################ Get All Attributes of Product ##################
                                $currentProdData    = $this->parseProdData($getHrefData);

                                //################ For Combination Product ############################
                                $flavourProdIds = [];
                                $prodIds = [];
                                $bothAttrs = false;

                                $flavoursAdded = [];
                                $sizeAdded = [];

                                ///################ if product has size and Flavour both attributes ##################
                                if(!empty($getHrefData->find('#Size0',0)) && !empty($getHrefData->find('#Flavor1',0))){
                                    $bothAttrs = true;
                                }
                                //############## If Product Has size Attributes ###########################
                                if(!empty($getHrefData->find('#Size0',0))){
                                    $dropDown = $getHrefData->find('#Size0',0);
                                    foreach($dropDown->find('option') as $opt) {
                                        $optUrl               = $opt->value;
                                        $selected             = $opt->selected;
                                        if(!$selected) {
                                            $prodIds[]        = $optUrl;
                                        }
                                    }
                                }
                                //############## If Product Has Flavour Attributes ###########################
                                if(!empty($getHrefData->find('#Flavor1',0))){
                                    $dropDown = $getHrefData->find('#Flavor1',0);
                                    foreach($dropDown->find('option') as $opt) {
                                        $optUrl               = $opt->value;
                                        $selected             = $opt->selected;
                                        if(!$bothAttrs) {
                                            $prodIds[]        = $optUrl;
                                        } else {
                                            $flavourProdIds[] = $optUrl;
                                        }
                                    }
                                }
                                
                                //############## generate combination id  ###########################
                                $combId = '';
                                if(!empty($prodIds)) {
                                    $combId = md5(time() . rand());
                                }                          
                                $currentProdData['combination_id']  = $combId;
                                //############## Save Product ###########################
                                $prodId                             = $this->saveProduct($href, $currentProdData);
                                
                                $sql = "UPDATE `product_temp` SET `is_done`='Success',order_id = '".$position."',`Response`='Product is saved' WHERE id =".$ele['id'];;
                                Yii::$app->db->createCommand($sql)->execute();
                            

                                //############## Set Category For Product ###########################
                                $this->setProductCategory($prodId, $Categoryid, $position);                           
                                /*################# Looping through all comibination products - for combination Size ############# */
                                if(!empty($prodIds)){
                                    foreach($prodIds as $pId){
                                        /* save current fetched product data */
                                        $subhref = "https://www.vitacost.com".$pId; 
                                        $ExistProduct  = Product::find()->where('import_url="'.$pId.'" OR import_url="'.$subhref.'"')->asArray()->one();
                                        if(!empty($ExistProduct)){
                                            $ProductId  =  $ExistProduct['id'];  
                                            $ExistProductCategory  = ProductCategory::find()->where(['product_id'=>$ProductId,'category_id'=> $Categoryid])->asArray()->one();
                                            if(empty( $ExistProductCategory )){
                                                $ProductCategory                =   new  ProductCategory;
                                                $ProductCategory->product_id    =   $ProductId;
                                                $ProductCategory->category_id   =   $Categoryid;
                                                $ProductCategory->position      =   0;
                                                $ProductCategory->save(false);
                                            }
                                            continue;
                                        } 
                                        $getHrefData = $this->curl($subhref);
                                        if(!isset($getHrefData['status'])){ 
                                            $getHrefData                         =   $dom->load($getHrefData, true, true);
                                            $currentProdData                     =   $this->parseProdData($getHrefData);
                                            $currentProdData['combination_id']   =   $combId;
                                            $prodId                              =   $this->saveProduct($subhref, $currentProdData);
                                            $this->setProductCategory($prodId, $Categoryid, 0);  
                                        }
                                    }
                                }

                                /* Loopting throught all flavors */
                                if(!empty($flavourProdIds)){
                                    foreach($flavourProdIds as $pId){
                                        /* save current fetched product data */
                                        $flhref = "https://www.vitacost.com".$pId; 
                                        $ExistProduct  = Product::find()->where('import_url="'.$pId.'" OR import_url="'.$flhref.'"')->asArray()->one();
                                        if(!empty($ExistProduct)){
                                            $ProductId  =  $ExistProduct['id'];  
                                            $ExistProductCategory  = ProductCategory::find()->where(['product_id'=>$ProductId,'category_id'=> $Categoryid])->asArray()->one();
                                            if(empty( $ExistProductCategory )){
                                                $ProductCategory                =   new  ProductCategory;
                                                $ProductCategory->product_id    =   $ProductId;
                                                $ProductCategory->category_id   =   $Categoryid;
                                                $ProductCategory->position      =   0;
                                                $ProductCategory->save(false);
                                            }
                                            continue;
                                        }                                     
                                        $getHrefData = $this->curl($flhref);
                                        if(!isset($getHrefData['status'])){ 
                                            $getHrefData=$dom->load($getHrefData, true, true);
                                            if(!empty($getHrefData->find('#Size0',0))){
                                                $dropDown = $getHrefData->find('#Size0',0);
                                                if(!empty($dropDown->find('option'))){
                                                    foreach($dropDown->find('option') as $opt) {
                                                        $optUrl = $opt->value;
                                                        /* save current fetched product data */
                                                        $hrefOpt = "https://www.vitacost.com".$optUrl; 
                                                        $ExistProduct  = Product::find()->where('import_url="'.$optUrl.'" OR import_url="'.$hrefOpt.'"')->asArray()->one();
                                                        if(!empty($ExistProduct)){
                                                            $ProductId  =  $ExistProduct['id'];  
                                                            $ExistProductCategory  = ProductCategory::find()->where(['product_id'=>$ProductId,'category_id'=> $Categoryid])->asArray()->one();
                                                            if(empty( $ExistProductCategory )){
                                                                $ProductCategory                =   new  ProductCategory;
                                                                $ProductCategory->product_id    =   $ProductId;
                                                                $ProductCategory->category_id   =   $Categoryid;
                                                                $ProductCategory->position      =   0;
                                                                $ProductCategory->save(false);
                                                            }
                                                            continue;
                                                        } 
                                                        $getHrefDataOpt = $this->curl($hrefOpt);
                                                        if(!isset($getHrefDataOpt['status'])){ 
                                                            $getHrefDataOpt = $dom->load($getHrefDataOpt, true, true);
                                                            $currentProdDataOpt = $this->parseProdData($getHrefDataOpt);
                                                            $currentProdDataOpt['combination_id'] = $combId;
                                                            $prodIdOpt = $this->saveProduct($optUrl, $currentProdDataOpt);
                                                            $this->setProductCategory($prodIdOpt, $Categoryid, 0);
                                                        }
                                                    }
                                                }
                                            }
                                            $currentProdData = $this->parseProdData($getHrefData);
                                            $currentProdData['combination_id'] = $combId;
                                            $prodId = $this->saveProduct($href, $currentProdData);
                                            $this->setProductCategory($prodId, $Categoryid, 0);
                                        }
                                    }

                                }
                    }else{
                        $sql = "UPDATE `product_temp` SET `is_done`='Failed',`Response`='".json_encode($getHrefData)."' WHERE id =".$ele['id'];;
                                Yii::$app->db->createCommand($sql)->execute();
                    }
                    $position++;
                    echo '<pre>';
                    print_r($ele);
                    echo '<pre>';
                    ob_flush();
                }
            }
        }
    }
    public function actionProductUrl(){

        $Start  =    !empty($_GET['start'])?$_GET['start']:1500;
        $Limit  =    !empty($_GET['limit'])?$_GET['limit']:2000;
        $time_start = microtime(true);         

        // $Categoty =Yii::$app->db->createCommand(
        //     'SELECT * FROM category WHERE `parent_id` IN (SELECT id FROM category 
        //      WHERE `parent_id` IN (SELECT id FROM `category` WHERE `parent_id` IN (SELECT id FROM `category` WHERE `parent_id` = 1))) ORDER BY `id` DESC LIMIT '.$Start.','.$Limit)
        // ->queryAll();

      //  $Categoty =Yii::$app->db->createCommand('SELECT * FROM category WHERE id IN (SELECT category_id FROM `failed_category_url` WHERE 1 GROUP BY `category_id`)')
         $Categoty =Yii::$app->db->createCommand('SELECT * FROM category WHERE id =832')
         ->queryAll();
         print_r($Categoty);
        foreach($Categoty as $ele){ 

            $current = 0;
            $maxResults = 0;
            $isMore = true;
            $CategoryId = 829;//$ele['id']; 
            $ele['import_url']= "https://www.vitacost.com/productresults.aspx?N=1309669+4293081418"  ; 
                     do {
                        $Url        = $ele['import_url'].'&scrolling=false&No='.$current;   
                                        
                        $getProduct = $this->curl($Url);      
                               
                        if(isset($getProduct['status']) && $getProduct['status'] == false){
                            $sql = "INSERT INTO `failed_category_url`(`category_id`, `url`,`Response`) VALUES ('".$CategoryId."','".$Url."','".json_encode($getProduct)."')";
                            Yii::$app->db->createCommand($sql)->execute();
                            $current = $current+20;
                            continue;
                        }
                        if(!$getProduct){
                            $sql = "INSERT INTO `failed_category_url`(`category_id`, `url`,`Response`) VALUES ('".$CategoryId."','".$Url."','Curl  response not getting.')";
                            Yii::$app->db->createCommand($sql)->execute();
                            $current = $current+20;                 
                            continue;
                        } 
                        $html = json_decode($getProduct, true);
                        if(!empty($html['searchRecordsTotal']) && !empty($html['html'])){  
                            $maxResults = $html['searchRecordsTotal'];
                            $getProduct = $html['html'];

                            $dom = new \simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);
                            $html=$dom->load($getProduct, true, true);
                        
                            if(!empty($html->find('.productWrapper',0))){                  
                                $ul = $html->find('.productWrapper',0);
                                if(!empty($ul->find('.product-block'))){              
                                    foreach($ul->find('.product-block') as $data){                              
                                        if(!empty($data->find('.pb-blocks-wrap',0)) && !empty($data->find('.pb-blocks-wrap',0)->find('a',0))){                    
                                            $href = "https://www.vitacost.com" . $data->find('.pb-blocks-wrap',0)->find('a',0)->href;                                         
                                            $sql = "SELECT * FROM `product_temp` WHERE `category_id`= '".$CategoryId."' AND `url`='".$href."'";
                                            $alreadyInTemp = Yii::$app->db->createCommand($sql)->queryOne();
                                            if(empty($alreadyInTemp)){
                                                $sql = "INSERT INTO `product_temp`(`category_id`, `url`) VALUES ('".$CategoryId."','".$href."')";
                                                Yii::$app->db->createCommand($sql)->execute();
                                            } else{
                                                echo 'already exist <br>';
                                            }                                  
                                        }
                                    }
                                }else{
                                    $sql = "INSERT INTO `failed_category_url`(`category_id`, `url`,`Response`) VALUES ('".$CategoryId."','".$Url."',,'li with product-block class is not found')";
                                    Yii::$app->db->createCommand($sql)->execute();
                                    $current = $current+20;
                                    continue;
                                }
                            }else{                               
                                $sql = "INSERT INTO `failed_category_url`(`category_id`, `url`,`Response`) VALUES ('".$CategoryId."','".$Url."','productWrapper is not found')";
                                Yii::$app->db->createCommand()->execute();
                                $current = $current+20;
                                continue;
                            }
                        }
                        if(($current + 20) > $maxResults){
                            $isMore = false;
                        } else {
                            $current += 20;
                        }
                       
                    
                } while ( $isMore );
            $time_end = microtime(true);
            $execution_time = ($time_end - $time_start);
            echo $ele['name'].'========== <b>Total Execution Time:</b> '.$execution_time.' Seconds <br>';
            ob_flush();            
        }

    }
    public function actionProduct(){
        $Start  =    !empty($_GET['start'])?$_GET['start']:0;
        $Limit  =    !empty($_GET['limit'])?$_GET['limit']:50;
        $time_start = microtime(true);         
        $Categoty =Yii::$app->db->createCommand(
            'SELECT * FROM category WHERE `parent_id` IN (SELECT id FROM category 
             WHERE `parent_id` IN (SELECT id FROM `category` WHERE `parent_id` IN (SELECT id FROM `category` WHERE `parent_id` = 1))) ORDER BY `id` DESC LIMIT '.$Start.','.$Limit)
        ->queryAll();

        ob_implicit_flush(true);
        ob_end_flush();
        ob_start(); 

        foreach($Categoty as $ele){
            
           try{
                $curl = curl_init();
                curl_setopt_array($curl, array(
                CURLOPT_URL => "http://teamgroovy.in/mymail/index.php?no=".$ele['id'],
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "cache-control: no-cache",
                    "postman-token: 44f8b37b-4529-aa05-f6ef-baf63ce5cbbb"
                ),
                ));

                $response = curl_exec($curl);
                $err = curl_error($curl);

                curl_close($curl);
            }catch(\Exception $e){

            }
            

            $current = 0;
            $maxResults = 0;
            $isMore = true;
            $CategoryId = $ele['id'];
            if($CategoryId < 2176){
                do {
                    $Url        = $ele['import_url'].'&scrolling=false&No='.$current;
                    $getProduct = $this->curl($Url);  
                    if(!$getProduct){
                        echo 'Failed getting '. $href. ' <br>';
                        continue;
                    }  
                    if(isset($getProduct['status']) && $getProduct['status'] == false){
                        continue;
                    }

                    $html = json_decode($getProduct, true);
                    $maxResults = $html['searchRecordsTotal'];
                    $getProduct = $html['html'];

                    $this->productArray($getProduct, $CategoryId);

                    if(($current + 20) > $maxResults){
                        $isMore = false;
                    } else {
                        $current += 20;
                    }
                } while ( $isMore );
                $time_end = microtime(true);
                $execution_time = ($time_end - $time_start);

                //execution time of the script
                echo $ele['name'].'========== <b>Total Execution Time:</b> '.$execution_time.' Seconds <br>';
                ob_flush();
            }
        }
    }

    private function parseProdData($getHrefData) {
        $datas = [];
        //########### Product Description #########################
        $details1  = !empty($getHrefData->find('#productDetails',0))?$getHrefData->find('#productDetails',0):"";
        $details2  = !empty($getHrefData->find('#nutritionFacts',0))?$getHrefData->find('#nutritionFacts',0):"";;     
        $datas['detail'] = $details1.$details2;
        //########### Product Title #########################
        if(!empty($getHrefData->find('#pdTitleBlock',0)) && !empty($getHrefData->find('#pdTitleBlock',0)->find('h1',0))
         && $getHrefData->find('#pdTitleBlock',0)->find('h1',0)->plaintext){          
                 $datas['title'] =   $getHrefData->find('#pdTitleBlock',0)->find('h1',0)->plaintext;
        }else{
                 $datas['title'] =   "";
        }
        //########### Product Image Url #########################
        if(!empty($getHrefData->find('#productImage', 0)) && !empty($getHrefData->find('#productImage', 0)->find('img', 0))
         && $getHrefData->find('#productImage', 0)->find('img', 0)->src){          
                $datas['image'] = "https://www.vitacost.com".$getHrefData->find('#productImage', 0)->find('img', 0)->src;
        }else{
                $datas['image'] =  "";
        }
        //########### Product Brand #########################
        if(!empty($getHrefData->find('.pBrandNameM',0)) && !empty($getHrefData->find('.pBrandNameM',0)->plaintext)){          
                $datas['brand'] = $getHrefData->find('.pBrandNameM',0)->plaintext;
        }else{
                $datas['brand'] =  "";
        }
        //########### Product Brand #########################
        if(!empty($getHrefData->find('.link-line',0)) && !empty( $getHrefData->find('.link-line',0)->find('li',1) 
         && !empty($getHrefData->find('.link-line',0)->find('li',1)->plaintext))){          
                $datas['sku'] = preg_replace('/[^0-9]/', '', $getHrefData->find('.link-line',0)->find('li',1)->plaintext);
        }else{
                $datas['sku'] =  "";
        }   
        //########### Product real_retail_price #########################   
        if(!empty($getHrefData->find('#pdpSubPrice',0)) && !empty($getHrefData->find('#pdpSubPrice',0)->find('.prstrike', 0))){

                $datas['real_retail_price'] =  (float) preg_replace('/[^0-9.]/', '',$getHrefData->find('#pdpSubPrice',0)->find('.prstrike', 0)->plaintext);
           
        }else{
                $datas['real_retail_price'] =  "";
        }
        //########### Product real_sale_price #########################   
        if(!empty($getHrefData->find('#pdpSubPrice',0))  && !empty($getHrefData->find('#pdpSubPrice',0)->find('.pOurPrice', 0))){
                $datas['real_sale_price'] =   (float) preg_replace('/[^0-9.]/', '',$getHrefData->find('#pdpSubPrice',0)->find('.pOurPrice', 0)->plaintext);
        }else{
                $datas['real_sale_price'] =  "";
        }
        //########### Product Meta Description #########################   
        if(!empty($getHrefData->find('meta[name=twitter:description]',0)) && !empty($getHrefData->find('meta[name=twitter:description]',0)->content)){
            $description="";
            $desc   =   $getHrefData->find('meta[name=twitter:description]',0)->content;
            if (strpos($desc, 'Vitacost.com') !== false) {
                $description   =        str_replace("Vitacost",' Vitadaddy',$desc);
            }else{
                $description   =        str_replace("Vitacost",' Vitadaddy',$desc); 
            }  
                $datas['meta']['description'] =      $description." available in India at Vitadaddy.com";
        }else{
                $datas['meta']['description']  =  "";  
        }
        //################### Product Meta  Title ##############################
        if(!empty($getHrefData->find('title',0)) && !empty($getHrefData->find('title',0)->plaintext)){
            $title="";
            $title             =       $getHrefData->find('title',0)->plaintext;            
            if (strpos($title, 'Vitacost.com') !== false) {
                $title    =       str_replace("Vitacost.com",'Vitadaddy',$title);
            }else{
                $title   =        str_replace("Vitacost",'Vitadaddy',$title); 
            }
            if (strpos($title, 'lowest price in India at Vitadaddy.com') !== false) {
            }else{
                 $title = $title.' lowest price in India at Vitadaddy.com';
            }
            $datas['meta']['title'] = $title;
        }else{
            $datas['meta']['title'] = "";
        }
        //################### Product Meta  Keyword ##############################
        if(!empty($getHrefData->find('meta[name=twitter:keywords]',0)) && !empty($getHrefData->find('meta[name=twitter:keywords]',0)->content)){
            $keyword="";
            $keyword            =  $getHrefData->find('meta[name=twitter:keywords]',0)->content;
            if (strpos($keyword, 'Vitacost.com') !== false) {
                $keyword    =       str_replace("Vitacost.com",'Vitadaddy',$keyword);
            }else{
                $keyword   =        str_replace("Vitacost",'Vitadaddy',$keyword); 
            } 
            if (strpos($keyword, 'Vitacost') !== false) {
                $keyword   =        str_replace("Vitacost",'Vitadaddy',$keyword); 
            }
            if (strpos($keyword, 'Vitadaddy') !== false) {
            }else{
                $keyword   =  $keyword.' - Vitadaddy';
            }
            $datas['meta']['keyword'] = $keyword;
        }else{
            $datas['meta']['keyword'] = "";
        }
        //################### Product Size ##############################
        if(!empty($getHrefData->find('#Size0',0)) && !empty($getHrefData->find('#Size0',0)->find('option[selected=selected]', 0))
            && !empty($getHrefData->find('#Size0',0)->find('option[selected=selected]', 0)->plaintext)) {
            $datas['size'] = $getHrefData->find('#Size0',0)->find('option[selected=selected]', 0)->plaintext;
        }else{
            $datas['size']    = '';
        }
        //################### Product Flavour ##############################
        if(!empty($getHrefData->find('#Flavor1',0)) && !empty($getHrefData->find('#Flavor1',0)->find('option[selected=selected]', 0))
            && !empty($getHrefData->find('#Flavor1',0)->find('option[selected=selected]', 0)->plaintext)) {
            $datas['flavour'] = $getHrefData->find('#Flavor1',0)->find('option[selected=selected]', 0)->plaintext;
        }else{
            $datas['flavour'] = '';
        }
        return $datas;
    }

    private function saveProduct($href, $datas){

        $model = $this->checkInDB($datas['sku']);
            
        if($model->id > 0 && !isset($datas['update'])){        
            return $model;
        }
        if($model->id > 0){
            
            $name = str_replace('-','',$model->name);
            $name = str_replace('--','',$name);
            $name = str_replace('--','',$name);
            $name = str_replace('--','',$name);
            $name = str_replace('---','',$name);
            $name = str_replace('----','',$name);
            $name = str_replace('-----','',$name);
            $name = str_replace('------','',$name);
            $name = str_replace('-------','',$name);
               
        }else{
            $model                   =  new Product;            
            $model->name             =  str_replace("&amp;",' & ',$datas['title']); 
            $name = str_replace('-','',$model->name);
            $name = str_replace('--','',$name);
            $name = str_replace('--','',$name);
            $name = str_replace('--','',$name);
            $name = str_replace('---','',$name);
            $name = str_replace('----','',$name);
            $name = str_replace('-----','',$name);
            $name = str_replace('------','',$name);
            $name = str_replace('-------','',$name);
        }
        $model->slug             =  strtolower(trim(preg_replace('/[^A-Za-z0-9]+/', '-', $name)));
        $model->sku              =  !empty($datas['sku']) ? $datas['sku'] : $model->slug;
        $model->description      =  str_replace("Vitacost",'Vitadaddy',$datas['detail']); 
        $model->brand            =  str_replace("Vitacost",'Vitadaddy',$datas['brand']); 
        $model->image            =  $datas['image'];

        if(!empty($datas['real_retail_price'])){
            $price  =   $datas['real_retail_price'];
        }else if(!empty($datas['real_sale_price'])){
            $price  =   $datas['real_sale_price'];
        }else{
            $price  =   0;
        }
        $model->retail_price     =  floor($price*88);
        $model->price            =  floor($price*72);

        $model->real_sale_price     =  $datas['real_retail_price'];
        $model->real_retail_price   =  $datas['real_sale_price'];

        $model->flavour          =  $datas['flavour'];
        $model->size             =  $datas['size'];
        $model->combination_id   =  $datas['combination_id'];

        $model->created_at         = time();
        $model->updated_at         = time();

        $model->is_active          = 1;
        $model->meta_keyword       = trim($datas['meta']['keyword']);
        $model->meta_title         = trim($datas['meta']['title']);
        $model->meta_description   = trim($datas['meta']['description']);
        $model->import_url         = !empty($href) ? $href : "" ;
        print_r($model);die;
        $id =  $model->save(false)?$model->id:0;

        return $id;
    }

    private function setProductCategory($proId, $catId, $position){

        $exists = ProductCategory::find()->where(['product_id' => $proId, 'category_id' => $catId])->one();

        if(empty($exists)){
            $ProductCategory = new ProductCategory;
            $ProductCategory->product_id = $proId;
            $ProductCategory->category_id = $catId;
            $ProductCategory->position    = $position;
            $ProductCategory->created_at         = time();
            $ProductCategory->update_at         = time();
            $ProductCategory->save(false);
            return $ProductCategory->id;
        } else {
            $exists->position    = $position;
            $exists->update_at   = time();
            $exists->save(false);
            return $exists->id;
        }
    }

    private function checkInDB($sku){
        $exists = Product::find()->where(['sku' => $sku])->one();
        if(!empty($exists)) return $exists;
        else return false;
    }

    public function productArray($getProduct ,$Categoryid){
        
        
        $dom = new \simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);
        $html=$dom->load($getProduct, true, true);
       // echo $html;die;
        $li = $html->find('.productWrapper',0);
        $finalArray = array();
        if(empty($li->find('.product-block'))){
            return;
        }
        foreach($li->find('.product-block') as $k=>$data){

            
            $title  = ""; $description="";$keyword="";
            $datas = array();
            $href = "https://www.vitacost.com" . $data->find('.pb-blocks-wrap',0)->find('a',0)->href; 
           // $href = "https://www.vitacost.com/optimum-nutrition-gold-standard-100-whey-double-rich-chocolate-5-lbs-1";
            $getHrefData = $this->curl($href);
            if(!$getHrefData){
                echo 'Failed getting '. $href. ' <br>';
                continue;
            }
            // echo $getHrefData->find('#productDetails',0);die;

            if(isset($getHrefData['status']) && $getHrefData['status'] != false){
                            $dom = new \simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);

                            
                            $getHrefData = $dom->load($getHrefData, true, true);
                            $currentProdData = $this->parseProdData($getHrefData);
                            $flavourProdIds = [];
                            $prodIds = [];
                            $bothAttrs = false;

                            $flavoursAdded = [];
                            $sizeAdded = [];

                            if(!empty($getHrefData->find('#Size0',0)) && !empty($getHrefData->find('#Flavor1',0))){
                                $bothAttrs = true;
                            }
                            
                            if(!empty($getHrefData->find('#Size0',0))){
                                $dropDown = $getHrefData->find('#Size0',0);
                                foreach($dropDown->find('option') as $opt) {
                                    $optUrl = $opt->value;
                                    $selected = $opt->selected;
                                    if(!$selected) {
                                        $prodIds[] = $optUrl;
                                    }
                                }
                            }
                            
                            if(!empty($getHrefData->find('#Flavor1',0))){
                                $dropDown = $getHrefData->find('#Flavor1',0);
                                foreach($dropDown->find('option') as $opt) {
                                    $optUrl = $opt->value;
                                    $selected = $opt->selected;
                                    if(!$bothAttrs) {
                                        $prodIds[] = $optUrl;
                                    } else {
                                        $flavourProdIds[] = $optUrl;
                                    }
                                }
                            }

                            $combId = '';
                            if(!empty($prodIds)) {
                                $combId = md5(time() . rand());
                            }

                            /* save current fetched product data */
                            $currentProdData = $this->parseProdData($getHrefData);
                            $alreadyAdded = $this->checkInDB($currentProdData['sku']);
                            
                            if($alreadyAdded > 0){

                                echo 'already in db found: '. $alreadyAdded->id.'<br>';
                                ob_flush();

                                $this->setProductCategory($alreadyAdded, $Categoryid, $this->position);
                                $this->position++;
                                continue;
                            }

                            $currentProdData['combination_id'] = $combId;
                            $prodId = $this->saveProduct($href, $currentProdData);
                            
                        
                            $this->setProductCategory($prodId, $Categoryid, $this->position);
                            $this->position++;

                            
                            /* Looping through all comibination products - for combination Size */
                            foreach($prodIds as $pId){
                                
                                /* save current fetched product data */
                                $href = "https://www.vitacost.com".$pId; 
                                $getHrefData = $this->curl($href);

                                if(!$getHrefData){
                                    echo 'Failed getting '. $href. ' <br>';
                                    continue;
                                }
                                $getHrefData=$dom->load($getHrefData, true, true);
                                $currentProdData = $this->parseProdData($getHrefData);
                                $currentProdData['combination_id'] = $combId;
                                $prodId = $this->saveProduct($href, $currentProdData);
                                $this->setProductCategory($prodId, $Categoryid, 0);               

                            }

                            /* Loopting throught all flavors */
                            if(!empty($flavourProdIds)){
                                foreach($flavourProdIds as $pId){
                                    
                                    /* save current fetched product data */
                                    $href = "https://www.vitacost.com".$pId; 
                                    $getHrefData = $this->curl($href);

                                    if(!$getHrefData){
                                        echo 'Failed getting '. $href. ' <br>';
                                        continue;
                                    }

                                    $getHrefData=$dom->load($getHrefData, true, true);

                                    if(!empty($getHrefData->find('#Size0',0))){
                                        $dropDown = $getHrefData->find('#Size0',0);
                                        foreach($dropDown->find('option') as $opt) {
                                            
                                            $optUrl = $opt->value;
                                            /* save current fetched product data */
                                            $hrefOpt = "https://www.vitacost.com".$optUrl; 
                                            $getHrefDataOpt = $this->curl($hrefOpt);

                                            $getHrefDataOpt = $dom->load($getHrefDataOpt, true, true);
                                            $currentProdDataOpt = $this->parseProdData($getHrefDataOpt);
                                            $currentProdDataOpt['combination_id'] = $combId;
                                            $prodIdOpt = $this->saveProduct($optUrl, $currentProdDataOpt);
                                            $this->setProductCategory($prodIdOpt, $Categoryid, 0);
                                        }
                                    }

                                    $currentProdData = $this->parseProdData($getHrefData);
                                    $currentProdData['combination_id'] = $combId;
                                    $prodId = $this->saveProduct($href, $currentProdData);
                                    $this->setProductCategory($prodId, $Categoryid, 0);

                                }

                            }
            }
        }
    }
    public function curl($url){    
        // $url = preg_replace("/^http:/i", "https:", $url);
        $headers = array(
            'accept:text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'accept-encoding:gzip, deflate, br',
            'accept-language:en-US,en;q=0.9',
            'upgrade-insecure-requests:1',
            'user-agent:Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'
        );
        
        $options = array(
            CURLOPT_RETURNTRANSFER => true,     // return web page
            CURLOPT_HEADER         => false,    // don't return headers
            CURLOPT_FOLLOWLOCATION => true,     // follow redirects
            CURLOPT_ENCODING       => "gzip, deflate, br",       // handle all encodings
            CURLOPT_USERAGENT      => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36", // who am i
            CURLOPT_AUTOREFERER    => true,     // set referer on redirect
            CURLOPT_CONNECTTIMEOUT => 20,      // timeout on connect
            CURLOPT_TIMEOUT        => 20,      // timeout on response
            CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            CURLOPT_SSL_VERIFYPEER => false,    // Disabled SSL Cert checks
            CURLOPT_SSL_VERIFYHOST => false,    // Disabled SSL Cert checks
            CURLOPT_HTTPHEADER => $headers,
            // CURLOPT_PORT => 443
        );
        
        $ch      = curl_init( $url );
        curl_setopt_array( $ch, $options );
        $content = curl_exec( $ch );
        $err     = curl_errno( $ch );
        $errmsg  = curl_error( $ch );
        $header  = curl_getinfo( $ch );
        curl_close( $ch );
        if($err){
            $content['status']=false;
            $content['error_message']=$errmsg;
            $content['error_header']=$header;  
            print_r($errmsg);
            print_r($header);
            // echo 'error='.$err;die;
        }
        return $content;
    }
    public function actionFiltercategory(){
        $Category   = Yii::$app->db->createCommand('SELECT * FROM category WHERE `parent_id` IN (SELECT id FROM `category` WHERE `parent_id` IN (SELECT id FROM `category` WHERE `parent_id` = 1))')->queryAll();
        foreach($Category as $ele ){
                if($ele['id']<=125){
                    continue;
                }
                $url = $ele['import_url'].'&allCategories=true';

                $content=$this->curl($url) ;
                if(isset($content['status']) && $content['status'] == false){
                    continue;
                }
                $dom = new \simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);
                $html=$dom->load($content, true, true);   
                if(!empty($html->find('.guided-nav__section-content',0))){
                    $ul = $html->find('.guided-nav__section-content',0)->find('.guided-nav__dimensions-list',0);
                    $position=1;
                    foreach($ul->find('li') as $k=>$c) {                        
                        if(!empty($c->find('.guided-nav__list-link--brand',0)->href)){                          
                                $categoryName = strip_tags(strtok($c->find('.guided-nav__list-link--brand',0)->plaintext,'(')) ;
                                $categoryName = str_replace("&nbsp;",'',$categoryName); 
                                if(!empty($c->find('.guided-nav__list-link--brand',0)->href)){
                                      $a   =  $c->find('.guided-nav__list-link--brand',0)->href;
                                      $this->saveCategory(array('name'=>$categoryName,'parent_id'=>$ele['id'],'href'=>$a,'position'=>$position));    
                                      $position++;
                                }
                        }
                    }
                }            
        }
    }
    public function actionIndex(){
        $url = 'https://www.vitacost.com/';  
        $content=$this->curl($url) ;
        $dom = new \simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);
        $html=$dom->load($content, true, true);   

        $element  =     $html->find('#departmentSubNav',0)->find('li[role=menuitem]');
        $i=0;$SubPosition=1;
        foreach($element as $e) {
            if($i>1){
                $SubCategoryName = strip_tags($e->find('a',0)->plaintext);
                if (strpos($SubCategoryName, 'Vitacost Brands') === false && strpos($SubCategoryName, 'Expert')=== false) {
                     echo $SubCategoryName.'<br/>';
                     $a = "https://www.vitacost.com".$e->find('a',0)->href;
                     $SubCategoryId = $this->saveCategory(array('name'=>$SubCategoryName,'parent_id'=>1,'href'=>$a,'position'=>$SubPosition));         
                     $childUl  =     $e->find('.submenu-wrapper-items',0); 
                     $ChildPosition = 1;
                     foreach($childUl->children()   as $k=>$c) {
                        $ChildCategoryName = strip_tags($c->find('a',0)->plaintext); 
                        if (strpos($ChildCategoryName, 'Vitacost') === false  && strpos($SubCategoryName, 'Expert') === false) {
                                    echo '====== :  '.$ChildCategoryName.'<br>';
                                    $a = "https://www.vitacost.com".$c->find('a',0)->href;                                   
                                    $ChildCategoryId =  $this->saveCategory(array('name'=>$ChildCategoryName,'parent_id'=>$SubCategoryId,'href'=>$a,'position'=>$ChildPosition));    
                                    $tailchildUl  =     $c->find('ul',0);
                                    if(!empty($tailchildUl)){
                                        $TailChildPosition = 1;
                                        foreach($tailchildUl->find('li')  as $tail) {                            
                                                $TailChildCategoryName = strip_tags($tail->find('a',0)->plaintext);; 
                                                if (strpos($TailChildCategoryName, 'Vitacost') === false  && strpos($TailChildCategoryName, 'Expert') === false) {
                                                    echo '=========== :  '.$TailChildCategoryName.'<br>';
                                                    $a = "https://www.vitacost.com".$tail->find('a',0)->href; 
                                                    $this->saveCategory(array('name'=>$TailChildCategoryName,'parent_id'=>$ChildCategoryId,'href'=>$a,'position'=>$TailChildPosition));    
                                                    $TailChildPosition++;
                                                }
                                        }
                                    }
                            $ChildPosition++;
                        }   
                                          
                    }
                    $SubPosition++;  
                }
            }
            $i++;
        }
            
    }
    public function saveCategory($CategoryData){            
        $title  = ""; $description="";$keyword="";
        if(!empty($CategoryData['href'])){
            $CategoryData['href'] = preg_replace("/^http:/i", "https:", $CategoryData['href']);
            $content=$this->curl($CategoryData['href']) ;
            $dom = new \simple_html_dom(null, true, true, DEFAULT_TARGET_CHARSET, true, DEFAULT_BR_TEXT, DEFAULT_SPAN_TEXT);
            $html=$dom->load($content, true, true); 
           
            ///################### Description ##############################
            if(!empty($html->find('meta[name=description]',0))){
                $desc   =   $html->find('meta[name=description]',0)->content;
                if (strpos($desc, 'Vitacost.com') !== false) {
                    $description   =        str_replace("Vitacost",' Vitadaddy',$desc);
                }else{
                    $description   =        str_replace("Vitacost",' Vitadaddy.com',$desc); 
                }  
                if (strpos($description, 'Vitacost') !== false) {
                    $description   =        str_replace("Vitacost",'Vitadaddy',$description); 
                }
            }
            ///################### Title ##############################
            if(!empty($html->find('title',0))){
                $title             =       $html->find('title',0)->plaintext;            
                if (strpos($title, 'Vitacost.com') !== false) {
                    $title    =       str_replace("Vitacost.com",'Lowest Price in India at Vitadaddy.com',$title);
                }else{
                    $title   =        str_replace("Vitacost",'Lowest Price in India at Vitadaddy.com',$title); 
                }  
                if (strpos($title, 'Vitacost') !== false) {
                    $title   =        str_replace("Vitacost",'Vitadaddy',$title); 
                }
            }
            if(!empty($html->find('meta[name=keywords]',0))){
                $keyword            =  $html->find('meta[name=keywords]',0)->content;
                if (strpos($keyword, 'Vitacost.com') !== false) {
                    $keyword    =       str_replace("Vitacost.com",'Vitadaddy',$keyword);
                }else{
                    $keyword   =        str_replace("Vitacost",'Vitadaddy',$keyword); 
                } 
                if (strpos($keyword, 'Vitacost') !== false) {
                    $keyword   =        str_replace("Vitacost",'Vitadaddy',$keyword); 
                }
                if (strpos($keyword, 'Vitadaddy') !== false) {
                }else{
                    $keyword   =  $keyword.' - Vitadaddy';
                }
            }
        }      
      //  echo $title;die;
        echo "======title====".$title.'<br>';
        echo "======desc====".$description.'<br>' ;
        echo "======keywor====".$keyword.'<br>' ;
       // die;
        
        $model                     = new Category();
        $model->name               = str_replace("&amp;",' & ',$CategoryData['name']); 
        $model->slug               = strtolower(trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $model->name))).'-'.$CategoryData['parent_id'];
        $model->parent_id          = $CategoryData['parent_id'];
        $model->created_at         = time();
        $model->updated_at         = time();
        $model->position           = !empty($CategoryData['position'])?$CategoryData['position']:"0";
        $model->is_active          = 1;
        $model->meta_keyword       = trim($keyword);
        $model->meta_title         = trim($title);
        $model->meta_description   = trim($description);
        $model->import_url         = !empty($CategoryData['href'])?$CategoryData['href']:"";
        $id =  $model->save()?$model->id:0;
        return $id ;
    }
        
}
?>