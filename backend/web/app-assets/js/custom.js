var call = function(data, callback) {
    var callTry = function(data, callback) {
        data.params._csrf =  $('meta[name=csrf-token]').attr("content");
        var DATA = data.params;
        var ajxOpts = {
            url: baseurl + data.url,
            data: DATA,
            dataType: 'json',
            crossDomain: true,
            cache: false,
            type: (typeof data.type != 'undefined' ? data.type : 'Post'),
        };
        $.ajax(ajxOpts).done(function(res) {
            callback(res);
        }).fail(function(r) {
            callback('fail');
        });
    }
    callTry(data, callback);
}
var TotalSales = function() {
    call({ url: '/dashboard/dashboard', params: { 'action': 'TotalSales'}, type: 'POST' }, function(resp) {
         $('#TotalSales').html('$ ' + resp.message);
    });
} 
var TodaySales = function() {
    call({ url: '/dashboard/dashboard', params: { 'action': 'TodaySales'}, type: 'POST' }, function(resp) {
         $('#TodaySales').html('$ ' + resp.message);
    });
} 
var TotalCustomer = function() {
    call({ url: '/dashboard/dashboard', params: { 'action': 'TotalCustomer'}, type: 'POST' }, function(resp) {
         $('#TotalCustomer').html(resp.message);
    });
} 
var TotalOrder = function() {
    call({ url: '/dashboard/dashboard', params: { 'action': 'TotalOrder'}, type: 'POST' }, function(resp) {
         $('#TotalOrder').html(resp.message);
    });
}
var LatestOrder = function() {
    $('#LatestOrder').addClass('block-mode-loading');
    call({ url: '/dashboard/dashboard', params: { 'action': 'LatestOrder'}, type: 'POST' }, function(resp) {
         $('#LatestOrder').html(resp.message);
         $('#LatestOrder').removeClass('block-mode-loading');
    });
} 
var TopProduct = function() {
    $('#TopProduct').addClass('block-mode-loading');
    call({ url: '/dashboard/dashboard', params: { 'action': 'TopProduct'}, type: 'POST' }, function(resp) {
        $('#TopProduct').html(resp.message);
        $('#TopProduct').removeClass('block-mode-loading');
    });
} 

$(document).ready(function(){   
    $(document).on('click','.fancytree-checkbox',function(){
        var html = "";
        $('.category-ids').remove();
        $('.fancytree-selected').each(function(i){
            var a = $(this).find('.fancytree-title').text().split('|'); 
            html += '<input type ="hidden" name="categoty_id[]" value="'+a[1]+'" class="category-ids">';    
        });
        $('#product-form').prepend(html);
    });
    
});