<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
		"../backend/web/app-assets/css/codebase.min.css",
		"../backend/web/css/groovy.css",
    ];	
    public $js = [
		'../backend/web/app-assets/js/core/popper.min.js',
		'../backend/web/app-assets/js/core/bootstrap.min.js',
		'../backend/web/app-assets/js/core/jquery.slimscroll.min.js',
		'../backend/web/app-assets/js/core/jquery.scrollLock.min.js',
		'../backend/web/app-assets/js/core/jquery.appear.min.js',
		'../backend/web/app-assets/js/core/jquery.countTo.min.js',
		'../backend/web/app-assets/js/core/js.cookie.min.js',
		'../backend/web/app-assets/js/codebase.js',		
		'../backend/web/app-assets/js/plugins/chartjs/Chart.bundle.min.js',
		'../backend/web/app-assets/js/pages/be_pages_ecom_dashboard.js',
		'../backend/web/app-assets/js/custom.js',	
	];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
