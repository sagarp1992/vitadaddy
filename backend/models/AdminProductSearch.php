<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\AdminProduct;

/**
 * AdminProductSearch represents the model behind the search form of `app\models\AdminProduct`.
 */
class AdminProductSearch extends AdminProduct
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'is_active', 'created_at', 'updated_at'], 'integer'],
            [['sku', 'name', 'description', 'image', 'brand', 'slug', 'flavour', 'size', 'combination_id', 'meta_title', 'meta_description', 'meta_keyword', 'import_url', 'real_retail_price', 'real_sale_price'], 'safe'],
            [['price', 'retail_price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = AdminProduct::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'price' => $this->price,
            'retail_price' => $this->retail_price,
            'is_active' => $this->is_active,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'sku', $this->sku])
            ->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'image', $this->image])
            ->andFilterWhere(['like', 'brand', $this->brand])
            ->andFilterWhere(['like', 'slug', $this->slug])
            ->andFilterWhere(['like', 'flavour', $this->flavour])
            ->andFilterWhere(['like', 'size', $this->size])
            ->andFilterWhere(['like', 'combination_id', $this->combination_id])
            ->andFilterWhere(['like', 'meta_title', $this->meta_title])
            ->andFilterWhere(['like', 'meta_description', $this->meta_description])
            ->andFilterWhere(['like', 'meta_keyword', $this->meta_keyword])
            ->andFilterWhere(['like', 'import_url', $this->import_url])
            ->andFilterWhere(['like', 'real_retail_price', $this->real_retail_price])
            ->andFilterWhere(['like', 'real_sale_price', $this->real_sale_price]);

        return $dataProvider;
    }
}
