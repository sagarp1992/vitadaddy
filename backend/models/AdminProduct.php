<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $sku
 * @property string $name
 * @property string $description
 * @property double $price
 * @property double $retail_price
 * @property string $image
 * @property string $brand
 * @property string $slug
 * @property string $flavour
 * @property string $size
 * @property string $combination_id
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $import_url
 * @property string $real_retail_price
 * @property string $real_sale_price
 * @property int $is_active
 * @property int $created_at
 * @property int $updated_at
 *
 * @property CartContent[] $cartContents
 * @property MapProductCategory[] $mapProductCategories
 * @property ProductCombination[] $productCombinations
 * @property ProductCombination[] $productCombinations0
 * @property ProductRelated[] $productRelateds
 * @property ProductRelated[] $productRelateds0
 */
class AdminProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sku', 'name', 'description', 'price', 'image','slug'], 'required'],
            [['description', 'meta_title', 'meta_description', 'meta_keyword', 'import_url'], 'string'],
            [['price', 'retail_price'], 'number'],
            ['image','url'],
            [['is_active', 'created_at', 'updated_at'], 'integer'],
            [['sku', 'name', 'image', 'slug', 'flavour', 'size', 'combination_id'], 'string', 'max' => 255],
            [['brand'], 'string', 'max' => 70],
            [['sku','slug'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'sku' => Yii::t('app', 'Sku'),
            'name' => Yii::t('app', 'Name'),
            'description' => Yii::t('app', 'Description'),
            'price' => Yii::t('app', 'Price'),
            'retail_price' => Yii::t('app', 'Retail Price'),
            'image' => Yii::t('app', 'Image'),
            'brand' => Yii::t('app', 'Brand'),
            'slug' => Yii::t('app', 'Slug'),
            'flavour' => Yii::t('app', 'Flavour'),
            'size' => Yii::t('app', 'Size'),
            'combination_id' => Yii::t('app', 'Combination ID'),
            'meta_title' => Yii::t('app', 'Meta Title'),
            'meta_description' => Yii::t('app', 'Meta Description'),
            'meta_keyword' => Yii::t('app', 'Meta Keyword'),
            'import_url' => Yii::t('app', 'Import Url'),
            'real_retail_price' => Yii::t('app', 'Real Retail Price'),
            'real_sale_price' => Yii::t('app', 'Real Sale Price'),
            'is_active' => Yii::t('app', 'Is Active'),
            'created_at' => Yii::t('app', 'Created At'),
            'updated_at' => Yii::t('app', 'Updated At'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCartContents()
    {
        return $this->hasMany(CartContent::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapProductCategories()
    {
        return $this->hasMany(MapProductCategory::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCombinations()
    {
        return $this->hasMany(ProductCombination::className(), ['child_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCombinations0()
    {
        return $this->hasMany(ProductCombination::className(), ['parent_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductRelateds()
    {
        return $this->hasMany(ProductRelated::className(), ['child_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductRelateds0()
    {
        return $this->hasMany(ProductRelated::className(), ['parent_product_id' => 'id']);
    }
}
