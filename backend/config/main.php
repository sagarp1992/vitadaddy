<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);
return [
    'id' => 'app-backend',
	'name'=>'www.iphonedata.info - Admin Panel',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'cart' => [ 
            'class' => 'common\components\MyCart', 
        ],
		'assetManager' => [
			'bundles' => [
				'yii\bootstrap\BootstrapAsset' => [
					'css' => [],
				],
			],
		],
		'user' => [
            'identityClass' => 'common\models\Admin',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_identity-backend', // unique for backend
				'httpOnly' => true
            ]
        ],
        'session' => [
            'name' => 'PHPBACKSESSID',
            'savePath' => sys_get_temp_dir(),
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request'=>[
			'class' => 'common\components\Request',
			'web'=> '/backend/web',
			'adminUrl' => '/admin',
			'cookieValidationKey' => 'mK4wDCHYZO',
			'csrfParam' => '_csrf-frontend',
		],
		'urlManager' => [
				'enablePrettyUrl' => true,
				'showScriptName' => false,
        ],
        'urlManagerFrontEnd' => [
            'class' => 'yii\web\urlManager',
            'baseUrl' => $params['host'],
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'assetManager' => [  
            'bundles' => [
                'yii2mod\tree\TreeAsset' => [
                    'css' => [
                        'skin-win8/ui.fancytree.less',
                    ]
                ],
            ],
        ],
    ],
    'params' => $params,
];
