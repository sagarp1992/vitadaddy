<?php

namespace common\models;
use yii\web\UploadedFile;
use Yii;

/**
 * This is the model class for table "slider".
 *
 * @property int $id
 * @property string $background_image
 * @property string $text
 * @property int $is_active
 * @property int $created_at
 * @property int $updated_at
 */
class Slider extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'slider';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['background_image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],     
            [['is_active', 'created_at', 'updated_at'], 'integer'],
            [['background_image','created_at'],'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'background_image' => 'Background Image',
            'text' => 'Text',
            'is_active' => 'Is Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function upload()
    {
        if ($this->validate()) {
            $fileName = 'slider/'.time().$this->background_image->baseName . '.' . $this->background_image->extension;
            $this->background_image->saveAs('../../frontend/web/img/' . $fileName);
            return $fileName;
        } else {
            return false;
        }
    }
}
