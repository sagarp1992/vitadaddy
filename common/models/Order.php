<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property int $id
 * @property string $order_no
 * @property int $order_date
 * @property int $ship_date
 * @property string $status
 * @property string $order_total
 * @property int $custome_id
 * @property int $shipping_id
 * @property int $paymen_card_id
 * @property int $comment
 *
 * @property OrderDetaills[] $orderDetaills
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_no','custome_id'], 'required'],
            [['order_date', 'ship_date', 'custome_id', 'shipping_id', 'paymen_card_id'], 'integer'],
            [['status','comment'], 'string'],
            [['order_total'], 'number'],
            [['order_no','comment'], 'string', 'max' => 255],
            [['order_no'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_no' => 'Order No',
            'order_date' => 'Order Date',
            'ship_date' => 'Ship Date',
            'status' => 'Status',
            'order_total' => 'Order Total',
            'custome_id' => 'Custome ID',
            'shipping_id' => 'Shipping ID',
            'paymen_card_id' => 'Paymen Card ID',
            'comment'=>'Order Notes'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDetaills()
    {
        return $this->hasMany(OrderDetaills::className(), ['order_id' => 'id']);
    }
    
}
