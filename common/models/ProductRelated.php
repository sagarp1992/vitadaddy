<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product_related".
 *
 * @property int $id
 * @property int $parent_product_id
 * @property int $child_product_id
 * @property int $created_at
 *
 * @property Product $childProduct
 * @property Product $parentProduct
 */
class ProductRelated extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_related';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_product_id', 'child_product_id', 'created_at'], 'required'],
            [['parent_product_id', 'child_product_id', 'created_at'], 'integer'],
            [['child_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['child_product_id' => 'id']],
            [['parent_product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['parent_product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_product_id' => 'Parent Product ID',
            'child_product_id' => 'Child Product ID',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'child_product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParentProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'parent_product_id']);
    }
}
