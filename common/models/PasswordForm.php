<?php

namespace common\models;
use Yii;
use yii\base\Model;
class PasswordForm extends Model
{
    public $password="";
    public $password_repeate="";
    public function rules()
    {
        return [
       
            [['password','password_repeate'], 'required'],
            ['password_repeate', 'required'],
            ['password_repeate', 'compare', 'compareAttribute'=>'password', 'message'=>"Passwords don't match" ],
            
        ];
    }
}
