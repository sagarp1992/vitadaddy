<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "cart".
 *
 * @property string $cart_unique_no
 * @property int $id
 * @property int $created_at
 * @property int $customer_id
 * @property int $payment_card_id
 * @property int $shipping_id
 *
 * @property CartContent[] $cartContents
 */
class Cart extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cart';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_at', 'customer_id', 'payment_card_id', 'shipping_id'], 'integer'],
            [['cart_unique_no'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'cart_unique_no' => 'Cart Unique No',
            'id' => 'ID',
            'created_at' => 'Created At',
            'customer_id' => 'Customer ID',
            'payment_card_id' => 'Payment Card ID',
            'shipping_id' => 'Shipping ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCartContents()
    {
        return $this->hasMany(CartContent::className(), ['cart_id' => 'id']);
    }
}
