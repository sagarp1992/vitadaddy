<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Order;

/**
 * OrderSearch represents the model behind the search form of `\common\models\Order`.
 */
class OrderSearch extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'order_date', 'ship_date', 'custome_id', 'shipping_id', 'paymen_card_id'], 'integer'],
            [['order_no', 'status'], 'safe'],
            [['order_total'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
      
        $query = Order::find()->orderBy(['id' => SORT_DESC]);

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);
           

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,         
            'ship_date' => $this->ship_date,
            'order_total' => $this->order_total,
            'custome_id' => $this->custome_id,
            'shipping_id' => $this->shipping_id,
            'paymen_card_id' => $this->paymen_card_id,
        ]);

        $query->andFilterWhere(['like', 'order_no', $this->order_no])
            ->andFilterWhere(['like', 'status', $this->status]);
        if (!is_null($this->order_date) &&  strpos($this->order_date, ' - ') !== false ) {

            list($start_date, $end_date) = explode(' - ', $this->order_date);
            $query->andFilterWhere(['between', 'order_date', strtotime($start_date), strtotime($end_date)]);

        }
        return $dataProvider;
    }
}
