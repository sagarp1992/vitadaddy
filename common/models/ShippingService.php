<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "shipping_service".
 *
 * @property int $id
 * @property string $price
 * @property string $name
 * @property string $image
 * @property int $delivery_day
 * @property int $before_12
 * @property int $after_12
 * @property string $description
 */
class ShippingService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'shipping_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['delivery_day', 'before_12', 'after_12'], 'integer'],
            [['description'], 'string'],
            [['price', 'image'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 30],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price' => 'Price',
            'name' => 'Name',
            'image' => 'Image',
            'delivery_day' => 'Delivery Day',
            'before_12' => 'Before 12',
            'after_12' => 'After 12',
            'description' => 'Description',
        ];
    }
}
