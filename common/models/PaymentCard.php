<?php

namespace common\models;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "payment_card".
 *
 * @property int $id
 * @property string $type
 * @property string $card_number
 * @property string $card_month
 * @property string $card_year
 * @property string $card_cvv_no
 * @property string $card_name
 * @property int $created_at
 *
 * @property MapCardCustomerCart[] $mapCardCustomerCarts
 */
class PaymentCard extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $month_list = array(
        '01'=>'01',
        '02'=>'02',
        '03'=>'03',
        '04'=>'04',
        '05'=>'05',
        '06'=>'06',
        '07'=>'07',
        '08'=>'08',
        '09'=>'09',
        '10'=>'10',
        '11'=>'11',
        '12'=>'12'

    );
    public function year_list(){
        $Year = date('Y');
        for($i=1;$i<=26;$i++){           
            $YearList[$Year]= $Year;
            $Year++;
        }
        return $YearList;
    }
    public static function tableName()
    {
        return 'payment_card';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['card_number', 'bryglen\validators\CreditCardValidator'],
            [['card_number'], 'required'],            
            ['card_month', 'required','message'=>'Invalid'],
            ['card_year',  'required','message'=>'Invalid'],
            ['card_holder_name',  'required'],
            ['card_cvv_no', 'required','message'=>'Invalid'],
            ['card_cvv_no', 'string', 'length' => [3, 3],'message'=>'Invalid'],
            ['card_cvv_no', 'number','message'=>'Invalid'],
            [['type', 'card_number', 'card_month', 'card_year', 'card_cvv_no'], 'string'],
            [['created_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'card_number' => 'Card Number',
            'card_month' => 'Card Month',
            'card_year' => 'Card Year',
            'card_cvv_no' => 'Card Cvv No',
            'card_name' => 'Card Name',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapCardCustomerCarts()
    {
        return $this->hasMany(MapCardCustomerCart::className(), ['card_id' => 'id']);
    }
    public function beforeSave($insert) {
        if ($this->isNewRecord){
            $this->card_number  =      utf8_encode(Yii::$app->security->encryptByKey($this->card_number,':$uperm@n201569585781!34:;::'));     
            $this->card_month   =      utf8_encode(Yii::$app->security->encryptByKey($this->card_number,':$uperm@n201569585781!34:;::'));     
            $this->card_year    =      utf8_encode(Yii::$app->security->encryptByKey($this->card_number,':$uperm@n201569585781!34:;::'));     
            $this->card_cvv_no  =      utf8_encode(Yii::$app->security->encryptByKey($this->card_number,':$uperm@n201569585781!34:;::'));     
            $this->created_at   =      time();          
          // echo Yii::$app->security->decryptByKey(utf8_decode($c),':$uperm@n201569585781!34:;::');die;
        }
     
        return parent::beforeSave($insert);
    }
}
