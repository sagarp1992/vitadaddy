<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "product".
 *
 * @property int $id
 * @property string $sku
 * @property string $name
 * @property string $description
 * @property double $price
 * @property double $retail_price
 * @property string $image
 * @property string $brand
 * @property string $slug
 * @property string $flavour
 * @property string $size
 * @property string $combination_id
 * @property string $meta_title
 * @property string $meta_description
 * @property string $meta_keyword
 * @property string $import_url
 * @property int $is_active
 * @property int $created_at
 * @property int $updated_at
 *
 * @property MapProductCategory[] $mapProductCategories
 * @property ProductCombination[] $productCombinations
 * @property ProductCombination[] $productCombinations0
 * @property ProductRelated[] $productRelateds
 * @property ProductRelated[] $productRelateds0
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['sku', 'name', 'description', 'price', 'retail_price', 'image', 'brand', 'slug', 'flavour', 'size', 'meta_title', 'meta_description', 'meta_keyword', 'import_url', 'created_at', 'updated_at'], 'required'],
            [['description', 'meta_title', 'meta_description', 'meta_keyword', 'import_url'], 'string'],
            [['price', 'retail_price'], 'number'],
            [['is_active', 'created_at', 'updated_at'], 'integer'],
            [['sku', 'name', 'image', 'slug', 'flavour', 'size', 'combination_id'], 'string', 'max' => 255],
            [['brand'], 'string', 'max' => 70],
            [['sku'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sku' => 'Sku',
            'name' => 'Name',
            'description' => 'Description',
            'price' => 'Price',
            'retail_price' => 'Retail Price',
            'image' => 'Image',
            'brand' => 'Brand',
            'slug' => 'Slug',
            'flavour' => 'Flavour',
            'size' => 'Size',
            'combination_id' => 'Combination ID',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
            'meta_keyword' => 'Meta Keyword',
            'import_url' => 'Import Url',
            'is_active' => 'Is Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapProductCategories()
    {
        return $this->hasMany(MapProductCategory::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCombinations()
    {
        return $this->hasMany(ProductCombination::className(), ['child_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductCombinations0()
    {
        return $this->hasMany(ProductCombination::className(), ['parent_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductRelateds()
    {
        return $this->hasMany(ProductRelated::className(), ['child_product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductRelateds0()
    {
        return $this->hasMany(ProductRelated::className(), ['parent_product_id' => 'id']);
    }
}
