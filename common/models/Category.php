<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "category".
 *
 * @property int $id
 * @property string $name
 * @property int $parent_id
 * @property int $position
 * @property string $slug
 * @property int $is_active
 * @property int $created_at
 * @property int $updated_at
 * @property string $meta_keyword
 * @property string $meta_title
 * @property string $meta_description
 *
 * @property Category $parent
 * @property Category[] $categories
 * @property MapProductCategory[] $mapProductCategories
 */
class Category extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'position', 'slug'], 'required'],
            [['slug'],'unique'],
            [['parent_id', 'position', 'is_active', 'created_at', 'updated_at'], 'integer'],
            [['meta_keyword', 'meta_title', 'meta_description'], 'string'],
            [['name', 'slug'], 'string', 'max' => 255],
            [['parent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'parent_id' => 'Parent ID',
            'position' => 'Position',
            'slug' => 'Slug',
            'is_active' => 'Is Active',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'meta_keyword' => 'Meta Keyword',
            'meta_title' => 'Meta Title',
            'meta_description' => 'Meta Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(Category::className(), ['id' => 'parent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategories()
    {
        return $this->hasMany(Category::className(), ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMapProductCategories()
    {
        return $this->hasMany(MapProductCategory::className(), ['category_id' => 'id']);
    }
}
