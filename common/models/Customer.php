<?php

namespace common\models;

use Yii;
use borales\extensions\phoneInput\PhoneInputValidator;
/**
 * This is the model class for table "customer".
 *
 * @property int $id
 * @property string $fullname
 * @property string $email
 * @property string $adress
 * @property string $city
 * @property string $state
 * @property string $zipcode
 * @property string $phone
 * @property int $created_at
 * @property int $update_at
 */
class Customer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customer';
    }
    public $state_list  = array (
        'AP' => 'Andhra Pradesh',
        'AR' => 'Arunachal Pradesh',
        'AS' => 'Assam',
        'BR' => 'Bihar',
        'CT' => 'Chhattisgarh',
        'GA' => 'Goa',
        'GJ' => 'Gujarat',
        'HR' => 'Haryana',
        'HP' => 'Himachal Pradesh',
        'JK' => 'Jammu & Kashmir',
        'JH' => 'Jharkhand',
        'KA' => 'Karnataka',
        'KL' => 'Kerala',
        'MP' => 'Madhya Pradesh',
        'MH' => 'Maharashtra',
        'MN' => 'Manipur',
        'ML' => 'Meghalaya',
        'MZ' => 'Mizoram',
        'NL' => 'Nagaland',
        'OR' => 'Odisha',
        'PB' => 'Punjab',
        'RJ' => 'Rajasthan',
        'SK' => 'Sikkim',
        'TN' => 'Tamil Nadu',
        'TR' => 'Tripura',
        'UK' => 'Uttarakhand',
        'UP' => 'Uttar Pradesh',
        'WB' => 'West Bengal',
        'AN' => 'Andaman & Nicobar',
        'CH' => 'Chandigarh',
        'DN' => 'Dadra and Nagar Haveli',
        'DD' => 'Daman & Diu',
        'DL' => 'Delhi',
        'LD' => 'Lakshadweep',
        'PY' => 'Puducherry',
    );
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname', 'email', 'adress', 'city', 'state', 'zipcode', 'phone'], 'required'],
            ['email','email'],
            [['created_at', 'update_at'], 'integer'],
            [['fullname', 'state'], 'string', 'max' => 50],
            [['email', 'city'], 'string', 'max' => 80],
            [['adress'], 'string', 'max' => 255],
            [['zipcode'], 'string', 'max' => 6],
            ['zipcode', 'number', 'message'=>'Please enter valid zipcode'],
            ['phone', 'match', 'pattern' => '/[0-9]{10}/','message'=>'Please enter valid phone number']
             
        ];
    }

    /**
     * @inheritdoc
     */
   
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'fullname' => 'Fullname',
            'email' => 'Email',
            'adress' => 'Adress',
            'city' => 'City',
            'state' => 'State',
            'zipcode' => 'Zipcode',
            'phone' => 'Phone',
            'created_at' => 'Created At',
            'update_at' => 'Update At',
        ];
    }
}
