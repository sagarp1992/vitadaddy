<?php
namespace common\components;
use Yii;
use yii\helpers\ArrayHelper;
use yii\base\Component;
use yii\base\InvalidConfigException; 
use \common\models\Product;
use \common\models\Cart;
use \common\models\CartContent;
use \common\models\Order;
use \common\models\OrderDetaills;
use \common\models\ShippingService;
class MyCart extends Component
{
  
    public function shippingCal($shpping_id,$WantTotatal=0){
        $ShippingService = ShippingService::find()->where(['id'=>$shpping_id])->one();
        $CurrentHour = date('H');
        if(!empty($ShippingService)){
            switch ($ShippingService['delivery_status']){
                case 'Approx': 
                    $res['day']   =  date('d M Y',time()+86400*$ShippingService['delivery_day']).' - '.date('d M Y',time()+86400*$ShippingService['delivery_upto']);
                    $res['price'] =  ($this->CartTotal() * $ShippingService['price'] ) / 100;
                    $res['checked'] = "checked";
                    break;
                case 'Day' :
                    $res['day']   =  $CurrentHour < 12 ? date('d M Y',time()+86400*$ShippingService['before_12']):date('d M Y',time()+86400*$ShippingService['after_12']);
                    $res['price'] =  ($this->CartTotal() * $ShippingService['price'] ) / 100;
                    $res['checked'] = "";
                    break;     
                case 'Fixed' :
                    $res['day']   =  date('d M Y',time()+86400*$ShippingService['delivery_day']);
                    $res['price'] =  $ShippingService['price'];
                    $res['checked'] = "";
                    break;                       
                default :
                    $res['day']   = 0;
                    $res['price'] = 0;
                    $res['checked'] = "";
                    break;
            }
        }else{
            $res['day']   = 0;
            $res['price'] = 0;
            $res['checked'] = "";
        }
        return $WantTotatal==1 ? $res['price']:$res;
        
    }
    public function placeOrder($paymen_card_id){

        $cartId                 =    $this->cartId();
        $Cart                   =    Cart::find()->where(['id'=>$cartId])->one();
        $CartList               =    $this->CartList();

        $Order                  =    new Order;
        $Order->order_no        =    'VTD'.rand(1111111,9999999);
        $Order->order_date      =    time();
        $Order->status          =    "New Order";
        $Order->order_total     =    $this->CartTotal();
        $Order->paymen_card_id  =    $paymen_card_id;
        $Order->custome_id      =    !empty($Cart->customer_id)?$Cart->customer_id:0;
        $Order->shipping_id     =    !empty($Cart->shipping_id)?$Cart->shipping_id:0;
        $Order->shipping_charge =    $this->shippingCal($Cart->shipping_id,1);
        $Order->tax_charge      =    0;
        $Order->final_total     =    $Order->order_total + $Order->shipping_charge + $Order->tax_charge;
        if($Order->validate() && $Order->save()){      
            $res['order_id']        = $Order->id;  
            foreach($CartList as $Item){
                $OrderDetails               =  new OrderDetaills;
                $OrderDetails->order_id     =  $Order->id;
                $OrderDetails->product_id   =  !empty($Item['product_id'])?$Item['product_id']:0;
                $OrderDetails->price        =  !empty($Item['price'])?$Item['price']:0;
                $OrderDetails->qty          =  !empty($Item['qty'])?$Item['qty']:0;
                if($OrderDetails->validate() && $OrderDetails->save()){ 

                }else{
                    $res['status']          = false;                   
                    $res['error']['order'][$Item['sku']]  = 'Failed to place order with SKU :<b>'.$Item['sku'].'</b>';
                }
            }
            if(empty($res['status'])){
                 $res['status']          = true;
                 $res['order_id']        = $Order->order_no;
                 $this->clearCart();
            }
        }else{
            $res['status']          = false;
            $res['error']['order']  = $Order->errors;
        }
        return $res;
    }
    public function cartId(){
        if(!empty($_COOKIE['cartId'])) {            
            $cartId   = $_COOKIE['cartId'];
            $Cart     = Cart::find()->where(['cart_unique_no'=>$cartId])->one();
            return !empty($Cart->id)?$Cart->id:0;die;
        }
        return 0;
    }
    public function clearCart(){
        $cartId                =    $this->cartId();        
        $cart     =Cart::find()->where(['id'=>$cartId])->one();
        if($cartId!= 0 && !empty($cart)){
            $cart->delete();
        }
        return ;
    }
    public function itemCount(){
        $cartId = $this->cartId();
        if ($cartId > 0){ 
            $Cart   = CartContent::find()->select(['SUM(`qty`) as Total'])
                    ->InnerJoin('product', 'cart_content.product_id=product.id')         
                    ->where(['cart_content.cart_id'=>$cartId])->asArray()->one();
            return !empty($Cart['Total'])?$Cart['Total']:0;
        }
        return 0;
    }
    public function CartTotal(){
        $cartId = $this->cartId();
        if ($cartId > 0){     
            $Cart   = CartContent::find()->select(['SUM(product.price * cart_content.qty) as Total'])
                     ->InnerJoin('product', 'cart_content.product_id=product.id')         
                     ->where(['cart_content.cart_id'=>$cartId])->asArray()->one();
            return !empty($Cart['Total'])?$Cart['Total']:0;
        }
        return 0;
    }    
    public function CartList(){
        $CartItem = array();
        $cartId = $this->cartId();
        if ($cartId > 0){        
            $Cart     = CartContent::find()->select(['product.sku','product.retail_price','product.slug','product.id','product.brand','product.image','product.name','product.price','cart_content.qty'])
                            ->InnerJoin('product', 'cart_content.product_id=product.id')         
                            ->where(['cart_content.cart_id'=>$cartId])->orderBy('cart_content.id DESC')->asArray()->all();
            if(!empty($Cart)){
                foreach ($Cart as $Items) {
                    array_push($CartItem,array('retail_price'=>$Items['retail_price'],'product_id'=>$Items['id'],'image'=>$Items['image'],'sku'=>$Items['sku'],'brand'=>$Items['brand'],'name'=>$Items['name'],'qty'=>$Items['qty'],'price'=>$Items['price'],'slug'=>$Items['slug']));
                }           
            }
        }
        return $CartItem;
    }
	 public function productQty($sku=0){
        $CartItem = array();
        $cartId = $this->cartId();
        if ($cartId > 0){        
            $Cart     = CartContent::find()->select(['product.sku','product.slug','product.id','product.brand','product.image','product.name','product.price','cart_content.qty'])
                            ->InnerJoin('product', 'cart_content.product_id=product.id')         
                            ->where(['cart_content.cart_id'=>$cartId])->asArray()->all();
            if(!empty($Cart)){
                foreach ($Cart as $Items) {
                    $CartItem[$Items['sku']]  = $Items['qty'];
                }           
            }
        }		
        return !empty($CartItem[$sku])?$CartItem[$sku]:0;
    }
    public function CreateCart(){     	 
        if(!empty($_COOKIE['cartId'])) {             
            $Cart   =   Cart::find()->where(['cart_unique_no'=>$_COOKIE['cartId']])->one();  
            if(empty($Cart)){ 
                $Cart                    = new Cart;
                $Cart->cart_unique_no    = $_COOKIE['cartId'];
                $Cart->created_at        = time();
                if($Cart->validate() && $Cart->save(false)){
                    $res['status']       = true;
                    $res['cart_id']      = $Cart->id;
                }else{
                    $res['status']       = false;
                    $res['message']      = json_encode($Cart->errors);
                }               
            }else{
                $res['status']       = true;
                $res['cart_id']      = $Cart->id;
            }
        }else{  
        	    $uniqueId  = md5(time());  
                setcookie('cartId', $uniqueId,  time() + 24 * 3600 * 15, "/");  

                $Cart                    = new Cart;
                $Cart->cart_unique_no    = $uniqueId;
                $Cart->created_at        = time();
                if($Cart->validate() && $Cart->save(false)){
                    $res['status']       = true;
                    $res['cart_id']      = $Cart->id;
                }else{
                    $res['status']       = false;
                    $res['message']      = json_encode($Cart->errors);
                }
                
        }
        return $res;
    }
    public function AddToCart($CartData){
        $sku          =   !empty($CartData['sku'])?$CartData['sku']:"";
        $qty          =   !empty($CartData['qty'])?$CartData['qty']:"";
        $total_qty    =   !empty($CartData['total_qty'])?$CartData['total_qty']:0;
        
        $Product = Product::find()->where(['sku'=>$sku])->one();
        if(!empty($Product)){
            //############## Create Persistance CartId ##################
            $res                               =   $this->CreateCart();
            if($res['status']== false){
                return $res;  
            }
            $cartId                            =   $res['cart_id'];
            $alreadyInCart                     =   CartContent::find()->where(['cart_id'=>$cartId,'product_id'=>$Product->id])->one();
            $CartContent                       =   !empty($alreadyInCart)?$alreadyInCart:new CartContent;
            $CartContent->product_id           =   $Product->id;
            $CartContent->cart_id              =   $cartId;
            $CartContent->qty                  =   !empty($alreadyInCart) ? $total_qty==0 ?(int) $alreadyInCart->qty  + $qty : $total_qty : $qty;
            if(empty($alreadyInCart)){                
                 $CartContent->created_at      =   time();
            }
            if($CartContent->validate() && $CartContent->save()){
                $res['status']    =  true;
                $res['message']   = 'Your product with sku <b>SKU: '.$sku.'</b> has been added';
            }else{
                $res['status']    =  false;
                $res['message']   =  json_encode($CartContent->errors);
            }
            
        }else{
                $res['status']    =  false;
                $res['message']   =  "There are no any product with <b>SKU : '.$sku.'</b>";              
        }
        return $res;        
    }
    public function RemoveItemFromCart($CartData){
        $cartId                     =   $this->cartId();   
        $sku    =   !empty($CartData['sku'])?$CartData['sku']:"";
        $Product = Product::find()->where(['sku'=>$sku])->one();
        
        if(!empty($Product)){
            //############## Create Persistance CartId ##################
          
            $Cart   =   CartContent::find()->where(['cart_id'=>$cartId,'product_id'=>$Product->id])->one();
            if(!empty($Cart)){
                if($Cart->delete()){
                     if($this->itemCount()==0){
                            $this->clearCart();
                     }
                     $res['status']    =  true;
                     $res['message']   = '<b>: '.$Product['name'].'</b> has been removed';
                }else{
                     $res['status']    =  false;
                     $res['message']   = 'Failed to remove this item';
                }
                
            }else{
                $res['status']    =  false;
                $res['message']   =  'This item is no more in cart';;
            }
            
        }else{
                $res['status']    =  false;
                $res['message']   =  "There are no any product with <b>SKU : '.$sku.'</b>";              
        }
        return $res;        
    }
    public function AddCustomerToCart($CustomerData){
        $cartId                             =    $this->cartId();  
        if($cartId>0){    
                $Cart                       =    Cart::find()->where(['id'=>$cartId])->one();
                $Cart->customer_id          =    !empty($CustomerData['customer_id'])?$CustomerData['customer_id']:0;
                if($Cart->save()){
                            $res['status']    =  true;
                            $res['message']   = 'Assigned';
                }else{
                        $res['status']    =  false;
                        $res['message']   = 'Failed to assigned';
                }
        }else{
            $res['status']    =  false;
            $res['message']   = 'Please add item in cart.';
        }
        return $res;   
    }
	 public function AddShippingToCart($ShipingData){
        $cartId                             =    $this->cartId();  
        if($cartId>0){    
                $Cart                       =    Cart::find()->where(['id'=>$cartId])->one();
                $Cart->shipping_id          =    !empty($ShipingData['shipping_id'])?$ShipingData['shipping_id']:0;
                if($Cart->save()){
                            $res['status']    =  true;
                            $res['message']   = 'Assigned';
                }else{
                        $res['status']    =  false;
                        $res['message']   = 'Failed to assigned';
                }
        }else{
            $res['status']    =  false;
            $res['message']   = 'Please add item in cart.';
        }
        return $res;   
    }
    public function CustomerInCart(){
        $cartId                             =    $this->cartId();  
        if($cartId>0){    
            $Cart                       =    Cart::find()->where(['id'=>$cartId])->one();
            if(!empty($Cart)){
                        $res['status']    		=  true;
                        $res['customer_id']   = $Cart->customer_id;
						$res['shipping_id']   = $Cart->shipping_id;
						$res['payment_card_id']   = $Cart->payment_card_id;
            }else{
                    $res['status']    =  false;
                    $res['shipping_id']   = 0;
                    $res['customer_id']   = 0;
                    $res['payment_card_id']   = 0;
                    $res['message']   = 'not assigned in cart';
            }
        }else{
            $res['status']    =  false;
            $res['message']   = 'Please add item in cart.';
            $res['shipping_id']   = 0;
            $res['customer_id']   = 0;
            $res['payment_card_id']   = 0;
        }
        return $res;   
    }
    public function orderStatus(){
        return array('New Order'=>'New Order','Pending'=>'Pending','Cancelled'=>'Cancelled','Completed'=>'Completed');
    }
    
}
?>