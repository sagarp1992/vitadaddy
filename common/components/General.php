<?php

namespace common\components;

use Yii;

use yii\helpers\ArrayHelper;

use yii\base\Component;

use yii\base\InvalidConfigException; 

use \common\models\ProductCategory;

use \common\models\Category;

class General extends Component

{
    
	
    public function breadCumb($CategoryId){

        do{

            $Category     = Category::find()->where(['id'=>$CategoryId])->orderBy('position')->asArray()->one();    

            $menuItems[] = ['label' =>$Category['name'], 'url'=>['/product/category','slug'=>$Category['slug']],'options'=> []];

        

            $CategoryId = false;

            if($Category['parent_id'] !=null){

                $CategoryId = $Category['parent_id'];

            }    

        }while($CategoryId);
		array_pop($menuItems);      
        return $menuItems;

    }

    public function categoryProductCount($CategoryId){      

        $CategoryIds     = array($CategoryId)+ArrayHelper::map($this->recursiveCategory($CategoryId),'id','id');     

        $ProductCategory     = "0";

        if(!empty($CategoryIds)){

            $CategoryIds   =  implode(",",$CategoryIds);           

            $ProductCategory = ProductCategory::find()->joinWith('product')->where('map_product_category.category_id IN ('.$CategoryIds.')')->count();

        }

        return $ProductCategory;

    }

    public function recursiveCategory($CategoryId,$isCount=0){

        $sql          =   'SELECT * FROM category WHERE `parent_id` = '.$CategoryId.' UNION SELECT * FROM category WHERE parent_id IN (SELECT id FROM category WHERE parent_id = '.$CategoryId.')';

        $Category     =    Yii::$app->db->createCommand($sql)->queryAll();

        if($isCount==1){

            return count($Category);

        }else{

            return $Category;  

        }      

    }

    public function getChildCategory($categoryId=0,$isMenu=1){       

        $Categories    =  Category::find()->where(['parent_id'=>$categoryId])->orderBy('position')->asArray()->all();

      

        if($isMenu==0){

            return  $Categories;

        }else{

            if(!empty($Categories)){

                foreach($Categories as $Category){

                    $count   = $this->categoryProductCount($Category['id']);

                    $menuItems[] = ['label' =>$Category['name'].'('.$count.')', 'url'=>['/product/category','slug'=>$Category['slug']],'options'=> []];

                   

                }

                return $menuItems;

            }else{

                return array();

            }

        }

    } 

}

?>